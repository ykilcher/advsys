package ch.ethz.grmbl.remote;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class SerializeTest {

	static class DummyClass implements Serializable {
		private static final long serialVersionUID = -2204697117409444876L;
		String s;
		List<String> sl = new ArrayList<String>();
	}

	@Test
	public void testSerialize() throws IOException, ClassNotFoundException {
		System.out.println("ObjectStreams");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DummyClass d = new DummyClass();
		d.s = "hello";
		for (int i = 0; i < 1000; i++) {
			d.sl.add("hello" + i);
		}
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		System.out.println(System.currentTimeMillis() + " write object start");
		oos.writeObject(d);
		System.out.println(System.currentTimeMillis() + " write object done");
		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(
				baos.toByteArray()));
		System.out.println(System.currentTimeMillis() + " read object start");
		Object in = ois.readObject();
		System.out.println(System.currentTimeMillis() + " read object done");
		assertEquals("hello", ((DummyClass) d).s);
	}

	@Test
	public void testNormalStreams() throws IOException, ClassNotFoundException {
		System.out.println("StringStreams");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DummyClass d = new DummyClass();
		d.s = "hello";
		d.sl = new ArrayList<String>();
		for (int i = 0; i < 1000; i++) {
			d.sl.add("hello" + i);
		}
		PrintWriter pw = new PrintWriter(baos);
		System.out.println(System.currentTimeMillis() + " write object start");
		pw.println(d.s);
		for (String s : d.sl) {
			pw.println(s);
		}
		System.out.println(System.currentTimeMillis() + " write object done");
		BufferedReader isr = new BufferedReader(new InputStreamReader(
				new ByteArrayInputStream(baos.toByteArray())));
		DummyClass d2 = new DummyClass();
		System.out.println(System.currentTimeMillis() + " read object start");
		d2.s = isr.readLine();
		for (int i = 0; i < 1000; i++) {
			d2.sl.add(isr.readLine());
		}
		System.out.println(System.currentTimeMillis() + " read object done");
		assertEquals("hello", ((DummyClass) d).s);
	}

}
