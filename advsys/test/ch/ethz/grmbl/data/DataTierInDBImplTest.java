package ch.ethz.grmbl.data;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.UUID;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.ethz.grmbl.exception.QueueCreationException;
import ch.ethz.grmbl.exception.QueueDeletionException;
import ch.ethz.grmbl.exception.QueueEmptyException;
import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.helper.DBHandler;
import ch.ethz.grmbl.model.ClientRecord;
import ch.ethz.grmbl.model.ClientRecordImpl;
import ch.ethz.grmbl.model.MessageRecord;
import ch.ethz.grmbl.model.MessageRecordImpl;
import ch.ethz.grmbl.model.QueueReadOrder;
import ch.ethz.grmbl.model.QueueRecord;
import ch.ethz.grmbl.model.QueueRecordImpl;
import ch.ethz.grmbl.monitoring.DataMonitor;
import ch.ethz.grmbl.util.Config;
import ch.ethz.grmbl.util.Config.DataTierImplementation;

public class DataTierInDBImplTest{

	private static final UUID C1 = UUID.randomUUID();
	private static DataTierInDBImpl dt;
	//private static DataTierInDBV2Impl dt;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception{
    	Config.get().dataTierImplementation = DataTierImplementation.JDBC;
    	Config.get().dbServerName = "localhost";
    	dt = (DataTierInDBImpl) DataTierFactory.getDataTier();
    	DataMonitor.start(true);
    	//dt = new DataTierInDBV2Impl();
    }

	@Before
	public void setUp() throws Exception {
		DBHandler.resetDB();
	}
	
	private <T> boolean contains(Collection<T> col, T obj)
	{
		@SuppressWarnings("rawtypes")
		Iterator it = col.iterator();
		while(it.hasNext())
			if(obj.equals(it.next()))
				return true;
		return false;
	}
	
	private MessageRecord createMessage(String message, String context, int priority, UUID sender, UUID receiver)
	{
		MessageRecordImpl mr = new MessageRecordImpl(null);
		mr.setContext(context);
		mr.setMessage(message);
		mr.setPriority(priority);
		mr.setSenderId(sender);
		mr.setReceiverId(receiver);
		return mr;
	}

	@Test
	public void testRegisterUnregister() 
	{
		ClientRecord cr = new ClientRecordImpl(C1);
		
		cr = dt.registerClient(cr);
		assertTrue(contains(dt.getAllClients(),cr));
		dt.unregisterClient(C1);
		assertFalse(contains(dt.getAllClients(),cr));
	}
	
	/*@Test(expected=PSQLException.class)
	public void testRegisterTwice()
	{
		ClientRecord cr = new ClientRecordImpl(C1);
		
		cr = dt.registerClient(cr);
		cr = dt.registerClient(cr);
	}
	
	@Test(expected=PSQLException.class)
	public void testDeleteNonExistentClient()
	{
		dt.unregisterClient(C1);
	}*/
	
	@Test
	public void testCreateDeleteQueue() throws QueueCreationException,QueueDeletionException
	{
		QueueRecord qr = new QueueRecordImpl(UUID.randomUUID());
		
		dt.createQueue(qr);
		dt.deleteQueue(qr.getId());
	}
	
	@Test
	public void testCheckIfQueueIsEmpty() throws QueueCreationException, QueueNonExistantException
	{
		QueueRecord qr = new QueueRecordImpl(UUID.randomUUID());
		
		dt.createQueue(qr);
		assertTrue(dt.checkIfQueueEmpty(qr.getId()));
		
		Collection<UUID> qids = new ArrayList<UUID>();
		qids.add(qr.getId());

		ClientRecord cr = new ClientRecordImpl(C1);
		cr = dt.registerClient(cr);
		
		dt.sendMessageToQueues(createMessage("ABC", null, 4, C1, null), qids);
		assertFalse(dt.checkIfQueueEmpty(qr.getId()));
	}
	
	/*@Test(expected=QueueCreationException.class)
	public void testCreateQueueTwice() throws QueueCreationException
	{
		QueueRecord qr = new QueueRecordImpl(UUID.randomUUID());
		
		dt.createQueue(qr);
		dt.createQueue(qr);
	}*/
	
	@Test(expected=QueueDeletionException.class)
	public void testDeleteNonExistentQueue() throws QueueDeletionException
	{
		dt.deleteQueue(UUID.randomUUID());
	}
	
	@Test
	public void testGetAllQueues() throws QueueCreationException
	{
		QueueRecord qr1 = new QueueRecordImpl(UUID.randomUUID());
		QueueRecord qr2 = new QueueRecordImpl(UUID.randomUUID());
		QueueRecord qr3 = new QueueRecordImpl(UUID.randomUUID());
		
		ArrayList<UUID> ids = new ArrayList<UUID>();
		ids.add(qr1.getId());
		ids.add(qr2.getId());
		ids.add(qr3.getId());
		
		dt.createQueue(qr1);
		dt.createQueue(qr2);
		dt.createQueue(qr3);
		
		Collection<QueueRecord> qrs = dt.getAllQueues();
		Iterator<QueueRecord> it = qrs.iterator();
		assertTrue(qrs.size() == 3);
		while(it.hasNext())
		{
			assertTrue(contains(ids,it.next().getId()));
		}
	}
	
	@Test
	public void testSendDeleteSingleMessage() throws QueueCreationException, QueueNonExistantException
	{
		ClientRecord cr = new ClientRecordImpl(C1);
		cr = dt.registerClient(cr);
		
		QueueRecordImpl qr1 = new QueueRecordImpl(UUID.randomUUID());
		dt.createQueue(qr1);
		
		ArrayList<UUID> queues = new ArrayList<UUID>();
		queues.add(qr1.getId());
		
		MessageRecord mr = createMessage("Test", "12abCD", 4, C1, null);
		
		Collection<MessageRecord> answer = dt.sendMessageToQueues(mr, queues);
		Iterator<MessageRecord> it = answer.iterator();
		assertTrue(it.hasNext());
		MessageRecord answerMr = it.next();
		
		assertTrue(answerMr.getId() != null);
		assertTrue(answerMr.getArrivalTime() > 0);
		assertTrue(answerMr.getContext().equals(mr.getContext()));
		assertTrue(answerMr.getMessage().equals(mr.getMessage()));
		assertTrue(answerMr.getPriority() == mr.getPriority());
		assertTrue(answerMr.getQueueId().equals(qr1.getId()));
		assertTrue(answerMr.getReceiverId() == null);
		assertTrue(answerMr.getSenderId().equals(C1));
		
		dt.deleteMessageById(answerMr.getId());
		assertFalse(dt.existsMessage(answerMr.getId()));
	}
	
	@Test
	public void testSendDeleteSingleMessageToMultipleQueues() throws QueueCreationException, QueueNonExistantException
	{
		ClientRecord cr = new ClientRecordImpl(C1);
		cr = dt.registerClient(cr);
		
		QueueRecordImpl qr1 = new QueueRecordImpl(UUID.randomUUID());
		dt.createQueue(qr1);
		QueueRecordImpl qr2 = new QueueRecordImpl(UUID.randomUUID());
		dt.createQueue(qr2);
		QueueRecordImpl qr3 = new QueueRecordImpl(UUID.randomUUID());
		dt.createQueue(qr3);
		
		ArrayList<UUID> queues = new ArrayList<UUID>();
		queues.add(qr1.getId());
		queues.add(qr2.getId());
		queues.add(qr3.getId());

		MessageRecord mr = createMessage("Test", "12abCD", 4, C1, null);
		
		Collection<MessageRecord> answer = dt.sendMessageToQueues(mr, queues);
		Iterator<MessageRecord> it = answer.iterator();
		assertTrue(it.hasNext());
		
		for(int i = 0; i < 3; ++i)
		{
			MessageRecord answerMr = it.next();
			assertTrue(answerMr.getId() != null);
			assertTrue(answerMr.getArrivalTime() > 0);
			assertTrue(answerMr.getContext().equals(mr.getContext()));
			assertTrue(answerMr.getMessage().equals(mr.getMessage()));
			assertTrue(answerMr.getPriority() == mr.getPriority());
			assertTrue(contains(queues,answerMr.getQueueId()));
			assertTrue(answerMr.getReceiverId() == null);
			assertTrue(answerMr.getSenderId().equals(C1));
			
			dt.deleteMessageById(answerMr.getId());
			assertFalse(dt.existsMessage(answerMr.getId()));
		}
	}
	
	@Test
	public void testReadSingleMessageByQueueId() throws QueueCreationException, QueueNonExistantException, QueueEmptyException
	{
		ClientRecord cr = new ClientRecordImpl(C1);
		cr = dt.registerClient(cr);
		
		QueueRecordImpl qr1 = new QueueRecordImpl(UUID.randomUUID());
		dt.createQueue(qr1);
		
		ArrayList<UUID> queues = new ArrayList<UUID>();
		queues.add(qr1.getId());
		
		MessageRecord mr = createMessage("Test", "12abCD", 4, C1, null);
		dt.sendMessageToQueues(mr, queues);
		
		MessageRecord mr2 = dt.read(qr1.getId(), C1, QueueReadOrder.TIME, false,true);
		
		assertTrue(mr2.getMessage().equals("Test"));
		assertTrue(mr2.getContext().equals("12abCD"));
		assertTrue(mr2.getPriority() == 4);
		assertTrue(dt.existsMessage(mr2.getId()));
		
		mr2 = dt.read(qr1.getId(), C1, QueueReadOrder.TIME, true,true);
		
		assertTrue(mr2.getMessage().equals("Test"));
		assertTrue(mr2.getContext().equals("12abCD"));
		assertTrue(mr2.getPriority() == 4);
		assertFalse(dt.existsMessage(mr2.getId()));
	}
	
	@Test
	public void testGetAllQueuesForClient() throws QueueCreationException, QueueNonExistantException
	{
		ClientRecord cr = new ClientRecordImpl(C1);
		cr = dt.registerClient(cr);

		ClientRecord cr2 = new ClientRecordImpl(UUID.randomUUID());
		cr2 = dt.registerClient(cr2);
		
		QueueRecord qr1 = new QueueRecordImpl(UUID.randomUUID());
		QueueRecord qr2 = new QueueRecordImpl(UUID.randomUUID());
		QueueRecord qr3 = new QueueRecordImpl(UUID.randomUUID());
		QueueRecord qr4 = new QueueRecordImpl(UUID.randomUUID());
		QueueRecord qr5 = new QueueRecordImpl(UUID.randomUUID());
		
		dt.createQueue(qr1);
		dt.createQueue(qr2);
		dt.createQueue(qr3);
		dt.createQueue(qr4);
		dt.createQueue(qr5);
		
		ArrayList<UUID> ids = new ArrayList<UUID>();
		ids.add(qr1.getId());
		ids.add(qr2.getId());
		ids.add(qr4.getId());
		
		ArrayList<UUID> queues = new ArrayList<UUID>();

		MessageRecord mr = createMessage("Test", "12abCD", 4, C1, null);
		queues.add(qr1.getId());
		dt.sendMessageToQueues(mr, queues);
		
		mr = createMessage("DJOI WJA10923.$ü0'/ \\djw\" jidw", "abcdefg", 10, C1, C1);
		queues.clear();
		queues.add(qr2.getId());
		dt.sendMessageToQueues(mr, queues);

		mr = createMessage("]¢|¬½¼#@|", "+\"*ç%&/()=?", 1, C1, cr2.getId());
		queues.clear();
		queues.add(qr3.getId());
		dt.sendMessageToQueues(mr, queues);

		mr = createMessage("]¢|¬½¼#@|", "+\"*ç%&/()=?", 1, cr2.getId(), C1);
		queues.clear();
		queues.add(qr4.getId());
		dt.sendMessageToQueues(mr, queues);
		
		Collection<QueueRecord> qrs = dt.getQueuesForClient(C1,null,true);
		Iterator<QueueRecord> it = qrs.iterator();
		assertTrue(qrs.size() == 3);
		while(it.hasNext())
		{
			assertTrue(contains(ids,it.next().getId()));
		}
		
		qrs = dt.getQueuesForClientBySender(C1, cr2.getId());
		it = qrs.iterator();
		assertTrue(qrs.size() == 1);
		while(it.hasNext())
		{
			assertTrue(qr4.getId().equals(it.next().getId()));
		}
	}

	@Test
	public void testGetAllQueuesAndMessagesForClientFromSenderSortedByPriority() throws QueueCreationException, QueueNonExistantException
	{
		ClientRecord cr = new ClientRecordImpl(C1);
		cr = dt.registerClient(cr);

		ClientRecord cr2 = new ClientRecordImpl(UUID.randomUUID());
		cr2 = dt.registerClient(cr2);
		UUID C2 = cr2.getId();
		
		QueueRecord qr1 = new QueueRecordImpl(UUID.randomUUID());
		QueueRecord qr2 = new QueueRecordImpl(UUID.randomUUID());
		QueueRecord qr3 = new QueueRecordImpl(UUID.randomUUID());
		
		dt.createQueue(qr1);
		dt.createQueue(qr2);
		dt.createQueue(qr3);
		
		ArrayList<UUID> ids = new ArrayList<UUID>();
		ids.add(qr2.getId());
		ids.add(qr3.getId());
		
		ArrayList<UUID> queues = new ArrayList<UUID>();

		MessageRecord mr = createMessage("Test", "12abCD", 2, C1, C2);
		queues.add(qr1.getId());
		Collection<MessageRecord> ms1 = dt.sendMessageToQueues(mr, queues);
		Collection<UUID> id1 = new ArrayList<UUID>(); 
		id1.add(ms1.iterator().next().getId());
		
		mr = createMessage("DJOI WJA10923.$ü0'/ \\djw\" jidw", "abcdefg", 3, C2, C1);
		queues.clear();
		queues.add(qr2.getId());
		Collection<MessageRecord> ms2 = dt.sendMessageToQueues(mr, queues);
		Collection<UUID> id2 = new ArrayList<UUID>(); 
		id2.add(ms2.iterator().next().getId());

		mr = createMessage("]¢|¬½¼#@|", "+\"*ç%&/()=?", 1, C2, null);
		queues.clear();
		queues.add(qr3.getId());
		Collection<MessageRecord> ms3 = dt.sendMessageToQueues(mr, queues);
		Collection<UUID> id3 = new ArrayList<UUID>(); 
		id3.add(ms3.iterator().next().getId());
		
		MessageRecord msg = dt.getMessageForClientBySender(C1, C2, QueueReadOrder.PRIORITY, false);
		assertFalse(contains(id1, msg.getId()));
		assertTrue(contains(id2, msg.getId()));
		assertFalse(contains(id3, msg.getId()));

		msg = dt.getMessageForClientBySender(C1, C2, QueueReadOrder.PRIORITY, true);
		assertFalse(contains(id1, msg.getId()));
		assertTrue(contains(id2, msg.getId()));
		assertFalse(contains(id3, msg.getId()));

		msg = dt.getMessageForClientBySender(C1, C2, QueueReadOrder.PRIORITY, true);
		assertFalse(contains(id1, msg.getId()));
		assertFalse(contains(id2, msg.getId()));
		assertTrue(contains(id3, msg.getId()));

		msg = dt.getMessageForClientBySender(C1, C2, QueueReadOrder.PRIORITY, false);
		assertTrue(msg == null);

		msg = dt.getMessageForClientBySender(C2, C1, QueueReadOrder.PRIORITY, true);
		assertTrue(contains(id1, msg.getId()));
		assertFalse(contains(id2, msg.getId()));
		assertFalse(contains(id3, msg.getId()));

		msg = dt.getMessageForClientBySender(C2, C1, QueueReadOrder.PRIORITY, false);
		assertTrue(msg == null);
	}

	@Test
	public void testGetAllQueuesAndMessagesForClientFromSenderSortedByTime() throws QueueCreationException, QueueNonExistantException
	{
		ClientRecord cr = new ClientRecordImpl(C1);
		cr = dt.registerClient(cr);

		ClientRecord cr2 = new ClientRecordImpl(UUID.randomUUID());
		cr2 = dt.registerClient(cr2);
		UUID C2 = cr2.getId();
		
		QueueRecord qr1 = new QueueRecordImpl(UUID.randomUUID());
		QueueRecord qr2 = new QueueRecordImpl(UUID.randomUUID());
		QueueRecord qr3 = new QueueRecordImpl(UUID.randomUUID());
		
		dt.createQueue(qr1);
		dt.createQueue(qr2);
		dt.createQueue(qr3);
		
		ArrayList<UUID> ids = new ArrayList<UUID>();
		ids.add(qr2.getId());
		ids.add(qr3.getId());
		
		ArrayList<UUID> queues = new ArrayList<UUID>();

		MessageRecord mr = createMessage("Test", "12abCD", 2, C1, C2);
		queues.add(qr1.getId());
		Collection<MessageRecord> ms1 = dt.sendMessageToQueues(mr, queues);
		Collection<UUID> id1 = new ArrayList<UUID>(); 
		id1.add(ms1.iterator().next().getId());

		mr = createMessage("]¢|¬½¼#@|", "+\"*ç%&/()=?", 1, C2, null);
		queues.clear();
		queues.add(qr3.getId());
		Collection<MessageRecord> ms3 = dt.sendMessageToQueues(mr, queues);
		Collection<UUID> id3 = new ArrayList<UUID>(); 
		id3.add(ms3.iterator().next().getId());
		
		mr = createMessage("DJOI WJA10923.$ü0'/ \\djw\" jidw", "abcdefg", 3, C2, C1);
		queues.clear();
		queues.add(qr2.getId());
		Collection<MessageRecord> ms2 = dt.sendMessageToQueues(mr, queues);
		Collection<UUID> id2 = new ArrayList<UUID>(); 
		id2.add(ms2.iterator().next().getId());
		
		MessageRecord msg = dt.getMessageForClientBySender(C1, C2, QueueReadOrder.TIME, false);
		assertFalse(contains(id1, msg.getId()));
		assertFalse(contains(id2, msg.getId()));
		assertTrue(contains(id3, msg.getId()));

		msg = dt.getMessageForClientBySender(C1, C2, QueueReadOrder.TIME, true);
		assertFalse(contains(id1, msg.getId()));
		assertFalse(contains(id2, msg.getId()));
		assertTrue(contains(id3, msg.getId()));

		msg = dt.getMessageForClientBySender(C1, C2, QueueReadOrder.TIME, true);
		assertFalse(contains(id1, msg.getId()));
		assertTrue(contains(id2, msg.getId()));
		assertFalse(contains(id3, msg.getId()));

		msg = dt.getMessageForClientBySender(C1, C2, QueueReadOrder.TIME, false);
		assertTrue(msg == null);

		msg = dt.getMessageForClientBySender(C2, C1, QueueReadOrder.TIME, true);
		assertTrue(contains(id1, msg.getId()));
		assertFalse(contains(id2, msg.getId()));
		assertFalse(contains(id3, msg.getId()));

		msg = dt.getMessageForClientBySender(C2, C1, QueueReadOrder.TIME, false);
		assertTrue(msg == null);
	}
	
	@Test
	public void testGetQueueById() throws QueueCreationException, QueueNonExistantException
	{
		QueueRecordImpl qr1 = new QueueRecordImpl(UUID.randomUUID());
		dt.createQueue(qr1);
		
		QueueRecord qr = dt.getQueueById(qr1.getId());
		assertTrue(qr.getId().equals(qr1.getId()));
	}
	
	@Test(expected=QueueNonExistantException.class)
	public void testGetNonExistantQueueById() throws QueueNonExistantException
	{
		dt.getQueueById(UUID.randomUUID());
	}
	
	//@Test
	public void testGetMessageForClientBySenderOverTime() throws QueueCreationException, QueueNonExistantException
	{
		ClientRecord cr = new ClientRecordImpl(C1);
		cr = dt.registerClient(cr);

		ClientRecord cr2 = new ClientRecordImpl(UUID.randomUUID());
		cr2 = dt.registerClient(cr2);
		UUID C2 = cr2.getId();

		QueueRecord qr1 = new QueueRecordImpl(UUID.randomUUID());
		QueueRecord qr2 = new QueueRecordImpl(UUID.randomUUID());
		QueueRecord qr3 = new QueueRecordImpl(UUID.randomUUID());
		
		dt.createQueue(qr1); //holds half the messages on average
		dt.createQueue(qr2); //holds 1/3rd of the messages on average
		dt.createQueue(qr3); //holds 1/6th of the messages on average
		
		int readsPerLoop = 20+(int)(Math.random()*30);
		
		int overflow = 0;
		for(int i = 0; i < 1000; ++i) //graphs of #messages in a single queues should have a similar trace as the #messages in all queues but with a different amplitude (and there can be noise)
		{
			int n = 0;
			while(Math.random() > 0.05)
			{
				ArrayList<UUID> ids = new ArrayList<UUID>();
				ids.add(qr1.getId());
				double r = Math.random();
				if(r > 0.33)
					ids.add(qr2.getId());
				if(r > 0.66)
					ids.add(qr3.getId());
				if(dt instanceof DataTierInDBV2Impl)
					++n;
				else // in 2/3 of the case
					n += ids.size();
				dt.sendMessageToQueues(createMessage("Test "+r, null, 1, C1, null), ids);
			}
			
			int num = 0;
			while(dt.getMessageForClientBySender(C2, C1, QueueReadOrder.TIME, true) != null && num < readsPerLoop)
			{
				++num;
			}
			assertTrue(num==n+overflow || num==readsPerLoop);
			overflow = Math.max(n+overflow-num-1,0);
		}
		
		while(dt.getMessageForClientBySender(C2, C1, QueueReadOrder.TIME, true) != null){}
		assertTrue(dt.getNumMessages()==0);
	}
}
