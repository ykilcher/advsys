package ch.ethz.grmbl.data;

import static org.junit.Assert.*;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import ch.ethz.grmbl.model.ClientRecord;
import ch.ethz.grmbl.model.ClientRecordImpl;
import ch.ethz.grmbl.msg.MessageTierImpl;

public class DataTierInMemoryImplTest {

	private static final UUID MT1 = UUID.randomUUID();
	private static final UUID C1 = UUID.randomUUID();

	@Before
	public void setUp() throws Exception {
	}

	private DataTierInMemoryImpl createDataTier() {
		DataTierInMemoryImpl dt = new DataTierInMemoryImpl();
		return dt;
	}

	@Test
	public void registerUnregisterTest() {
		DataTierInMemoryImpl dt = createDataTier();
		MessageTierImpl mt = new MessageTierImpl(MT1,dt);
		ClientRecord cr = new ClientRecordImpl(C1);
		cr = dt.registerClient(cr);
		assertTrue(dt.getClients().containsKey(C1));
		dt.unregisterClient(C1);
		assertFalse(dt.getClients().containsKey(C1));
		cr = dt.registerClient(cr);
	}

}
