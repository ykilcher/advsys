package ch.ethz.grmbl.helper;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.postgresql.ds.PGPoolingDataSource;

import ch.ethz.grmbl.util.Config;

public class DBHandler 
{
	private static PGPoolingDataSource source;
	
	static
	{
		try {
			init();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private static void init() throws SQLException
	{
		Config c = Config.get();
		source = new PGPoolingDataSource();
		source.setDataSourceName("aslTestPool");
		source.setServerName(c.dbServerName);
		source.setDatabaseName(c.dbName);
		source.setUser(c.dbUsername);
		source.setPassword(c.dbPassword);
		source.setPortNumber(c.dbPort);
		source.setMaxConnections(2);
		source.setSsl(false);
	}
	
	public static void resetDB() throws SQLException
	{
		Connection con = source.getConnection();
		con.setAutoCommit(false);
		Statement stmt = con.createStatement();
		stmt.addBatch("DELETE FROM asl.message"); //DB v1
		stmt.addBatch("DELETE FROM asl.queue");
		stmt.addBatch("DELETE FROM asl.client");
		stmt.addBatch("DELETE FROM asl2.isIn"); //DBv2
		stmt.addBatch("DELETE FROM asl2.message");
		stmt.addBatch("DELETE FROM asl2.queue");
		stmt.addBatch("DELETE FROM asl2.client");
		stmt.executeBatch();
		con.commit();
		con.close();
	}
	
	/*public static UUID[] registerClients(int num) throws SQLException
	{
		Connection con = source.getConnection();
		con.setAutoCommit(false);
		PreparedStatement stmt = con.prepareStatement("INSERT INTO asl.client VALUES (?)"); //TODO: Stored procedure!
		
		UUID[] uuids = new UUID[num];
		for(int i = 0; i < num; ++i)
		{
			uuids[i] = UUID.randomUUID();
			stmt.setObject(1, uuids[i]);
			
			stmt.execute();
		}
		con.commit();
		con.close();
		return uuids;
	}
	
	public static void main(String[] args) throws SQLException //testDB
	{
		try
		{
			resetDB();
			registerClients(100);
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			SQLException s = e.getNextException();
			while(s != null)
			{
				s.printStackTrace();
				s = s.getNextException();
			}
		}
	}*/
}
