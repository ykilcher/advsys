package ch.ethz.grmbl.jython;

import java.util.UUID;

import org.junit.Test;

import ch.ethz.grmbl.client.ClientAPI;
import ch.ethz.grmbl.client.ClientAPIFactory;
import ch.ethz.grmbl.client.ClientMessage;
import ch.ethz.grmbl.client.ClientQueue;
import ch.ethz.grmbl.exception.QueueCreationException;
import ch.ethz.grmbl.exception.QueueEmptyException;
import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.util.Config;

public class JavaJythonComp {

	@Test
	public void test() throws QueueCreationException, QueueNonExistantException, QueueEmptyException {
		    Config cnf = Config.get();
		    
		    cnf.remoteMessageTier = false;
		    cnf.dataTierImplementation = Config.DataTierImplementation.IN_MEMORY;
		    ClientAPI api = ClientAPIFactory.createAPI(UUID.randomUUID(), "localhost");
		    long t1 = System.currentTimeMillis();
		    for (int i=0;i<200000;i++){
		        UUID qid = UUID.randomUUID();
		        ClientQueue q = api.createQueue(qid);
		        q.sendMessage("hello");
		        ClientQueue q2 = api.getQueueById(qid);
		        ClientMessage m = q2.pop();
		        if(m == null || !"hello".equals(m.getMessage())){
		        	throw new RuntimeException("NO");
		        }
	}
		    long t2 = System.currentTimeMillis();
		    System.out.println(t2 - t1);
		    
	}

}
