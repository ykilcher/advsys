package ch.ethz.grmbl.client;

import static org.junit.Assert.*;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import ch.ethz.grmbl.exception.QueueDeletionException;
import ch.ethz.grmbl.exception.QueueNonExistantException;

public class ClientQueueTest {
	
	private static final String MSG = "da message";
	private static final UUID RID = UUID.randomUUID();
	private static final UUID QID = UUID.randomUUID();

	private ClientQueueImpl createQueue(){
		ClientQueueImpl queue = new ClientQueueImpl(null, null, null);
		assertNotNull(queue);
		return queue;
	}

	@Test
	public void deleteTest() throws QueueDeletionException {
		ClientQueue queue = createQueue();
		queue.delete();
	}
	
	@Test
	public void sendMessageTest() throws QueueNonExistantException{
		ClientQueue queue = createQueue();
		queue.sendMessage(MSG, RID,1);
	}

}
