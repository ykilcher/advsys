package ch.ethz.grmbl.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Set;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import ch.ethz.grmbl.exception.QueueCreationException;
import ch.ethz.grmbl.exception.QueueEmptyException;
import ch.ethz.grmbl.exception.QueueNonExistantException;

public class ClientFunctionalityRequirementsTest {

	private ClientAPI clientAPI;
	
	@Before
	public void setUp() throws Exception {
		
		assertNotNull(clientAPI);
	}

	@Test
	public void clientCreateQueueTest() throws QueueCreationException,  QueueNonExistantException {
		ClientQueue queue = clientAPI.createQueue(UUID.randomUUID());
		assertNotNull(queue);
		try{
			queue.peek();
			fail();
		}catch(QueueEmptyException e){
			//good
		}
		Set<ClientQueue> qs = clientAPI.getAllQueues();
		assertTrue(qs.contains(queue));
		ClientQueue qbid = clientAPI.getQueueById(queue.getId());
		assertEquals(qbid, qbid);
	}

}
