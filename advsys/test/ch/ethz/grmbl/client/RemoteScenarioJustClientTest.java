package ch.ethz.grmbl.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Set;
import java.util.UUID;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import ch.ethz.grmbl.exception.QueueCreationException;
import ch.ethz.grmbl.exception.QueueEmptyException;
import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.util.Config;

public class RemoteScenarioJustClientTest {

	public static class TestClient {
		UUID id;
		String mta;
		ClientAPI api;
	}

	private static final String HELLOBOB = "hello, bob";

	private TestClient alice, bob, carl;

	private static int PORT;
	private static String MTA;

	@Before
	public void setUp() throws Exception {
		PORT = 44444;
		MTA = "localhost:" + PORT;

		System.setOut(new PrintStream(new OutputStream() {

			@Override
			public void write(int b) throws IOException {
			}
		}));

		Logger.getLogger("ch.ethz.grmbl").setLevel(Level.ALL);
		Logger.getLogger("ch.ethz.grmbl").addHandler(new ConsoleHandler());
		Logger.getLogger("ch.ethz.grmbl").setUseParentHandlers(false);
		Config.get().remoteMessageTier = true;


		System.out.println("creating test clients");
		alice = new TestClient();
		alice.id = UUID.randomUUID();
		alice.mta = MTA;
		alice.api = ClientAPIFactory.createAPI(alice.id, alice.mta);
		bob = new TestClient();
		bob.id = UUID.randomUUID();
		bob.mta = MTA;
		bob.api = ClientAPIFactory.createAPI(bob.id, bob.mta);
		carl = new TestClient();
		carl.id = UUID.randomUUID();
		carl.mta = MTA;
		carl.api = ClientAPIFactory.createAPI(carl.id, carl.mta);
	}

	@After
	public void shudown() throws IOException, InterruptedException {
		alice.api.unregister();
		bob.api.unregister();
		carl.api.unregister();
	}

	@Test
	@Ignore
	public void test() throws QueueEmptyException,
			QueueNonExistantException, QueueCreationException {
		ClientQueue aliceQueue1 = alice.api.createQueue(UUID.randomUUID());
		assertNotNull(aliceQueue1);
		Set<ClientQueue> bobQueues = bob.api
				.queryForQueuesWithWaitingMessages();
		assertTrue(bobQueues.isEmpty());
		System.out.println("sending message");
		ClientMessage alicetobob = aliceQueue1.sendMessage(HELLOBOB, bob.id,1);
		System.out.println("sent message");
		assertNotNull(alicetobob);
		bobQueues = bob.api.queryForQueuesWithWaitingMessages();
		assertEquals(1, bobQueues.size());
		System.out.println("message recieved");
		for (ClientQueue cq : bobQueues) {
			ClientMessage peek = cq.peek();
			assertNotNull(peek);
			assertEquals(HELLOBOB, peek.getMessage());
			ClientMessage pop = cq.pop();
			assertNotNull(pop);
			assertEquals(HELLOBOB, pop.getMessage());
			assertTrue(cq.isEmpty());
			try {
				cq.peek();
				fail();
			} catch (QueueEmptyException e) {
				// good
			}
		}

	}

	@Test
//	@Ignore
	public void sendLotsOfMsgs() throws 
			QueueEmptyException, QueueNonExistantException,
			QueueCreationException, InterruptedException {
		final long limit = 100;
		final ClientQueue aliceQueue1 = alice.api.createQueue(UUID.randomUUID());
		assertNotNull(aliceQueue1);
		Thread at = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					for (int i = 0; i < limit; i++) {
						if (i % Math.max(limit / 100, 1) == 0) {
							System.out.println(System.currentTimeMillis()
									+ " alice: sending message " + i);
						}
						aliceQueue1.sendMessage(HELLOBOB, null,1);
					}

				} catch (QueueNonExistantException e) {
					e.printStackTrace();
				}
			}
		});
		at.start();
		at.join();
	}

	@Test
	 @Ignore
	public void sendLotsOfMsgs2() throws 
			QueueEmptyException, QueueNonExistantException,
			QueueCreationException, InterruptedException {
		final long limit = 1000;
		final ClientQueue aliceQueue1 = alice.api.createQueue(UUID.randomUUID());
		assertNotNull(aliceQueue1);
		Thread at = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					for (int i = 0; i < limit; i++) {
						if (i % (limit / 100) == 0) {
							System.out.println("alice: sending message " + i);
						}
						aliceQueue1.sendMessage(HELLOBOB, null,1);
					}

				} catch (QueueNonExistantException e) {
					e.printStackTrace();
				}
			}
		});
		Thread bt = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					ClientQueue bobQueue1 = bob.api.getQueueById(aliceQueue1
							.getId());
					for (int i = 0; i < limit; i++) {
						if (i % (limit / 100) == 0) {
							System.out.println("bob: sending message " + i);
						}
						bobQueue1.sendMessage(HELLOBOB, null,1);
					}

				} catch (QueueNonExistantException e) {
					e.printStackTrace();
				}
			}
		});
		int clients = 100;
		final Thread tcs[] = new Thread[clients];
		for (int i = 0; i < clients; i++) {
			final TestClient tc = new TestClient();
			tc.id = UUID.randomUUID();
			tc.mta = MTA;
			tc.api = ClientAPIFactory.createAPI(tc.id, tc.mta);
			Thread tct = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						ClientQueue q = tc.api.getQueueById(aliceQueue1.getId());
						for (int i = 0; i < limit; i++) {
							if (i % (limit / 100) == 0) {
								System.out.println("tc " + tc.id
										+ ": sending message " + i);
							}
							q.sendMessage(HELLOBOB, null,1);
						}

					} catch (QueueNonExistantException e) {
						e.printStackTrace();
					}
				}
			});
			tcs[i] = tct;
		}
		at.start();
		bt.start();
		for (int i = 0; i < clients; i++) {
			tcs[i].start();
		}
		at.join();
		bt.join();
		for (int i = 0; i < clients; i++) {
			tcs[i].join();
		}
	}

}
