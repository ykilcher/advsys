package ch.ethz.grmbl.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import ch.ethz.grmbl.exception.DBConnectionException;
import ch.ethz.grmbl.exception.QueueCreationException;
import ch.ethz.grmbl.exception.QueueDeletionException;
import ch.ethz.grmbl.exception.QueueEmptyException;
import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.monitoring.DataMonitor;
import ch.ethz.grmbl.util.Config;
import ch.ethz.grmbl.util.Config.DataTierImplementation;

public class ScenarioTest {

	public static class TestClient {
		UUID id;
		String mta;
		ClientAPI api;
	}

	private static final String HELLOBOB = "hello, bob";

	private TestClient alice, bob, carl;

	@Before
	public void setUp() throws Exception {
		Config.get().dataTierImplementation = DataTierImplementation.IN_MEMORY;
		Config.get().remoteMessageTier = false;
		alice = new TestClient();
		alice.id = UUID.randomUUID();
		alice.mta = "mt1";
		alice.api = ClientAPIFactory.createAPI(alice.id, alice.mta);
		bob = new TestClient();
		bob.id = UUID.randomUUID();
		bob.mta = "mt1";
		bob.api = ClientAPIFactory.createAPI(bob.id, bob.mta);
		carl = new TestClient();
		carl.id = UUID.randomUUID();
		carl.mta = "mt1";
		carl.api = ClientAPIFactory.createAPI(carl.id, carl.mta);
	}

	@Test
	public void test() throws QueueCreationException,
			QueueNonExistantException, QueueDeletionException {
		ClientQueue aliceQueue1 = alice.api.createQueue(UUID.randomUUID());
		assertNotNull(aliceQueue1);
		Set<ClientQueue> bobQueues = bob.api
				.queryForQueuesWithWaitingMessages();
		assertTrue(bobQueues.isEmpty());
		ClientMessage alicetobob = aliceQueue1.sendMessage(HELLOBOB, bob.id, 1);
		assertNotNull(alicetobob);
		bobQueues = bob.api.queryForQueuesWithWaitingMessages();
		assertEquals(1, bobQueues.size());
		aliceQueue1.delete();
	}

	@Test
	public void sendToMultipleTest() throws QueueCreationException,
			QueueNonExistantException, QueueDeletionException {
		ClientQueue[] qs = new ClientQueue[] {
				alice.api.createQueue(UUID.randomUUID()),
				alice.api.createQueue(UUID.randomUUID()),
				alice.api.createQueue(UUID.randomUUID()) };
		Set<ClientQueue> bobQueues = bob.api
				.queryForQueuesWithWaitingMessages();
		assertTrue(bobQueues.isEmpty());
		alice.api.sendMessageToMultipleQueues(HELLOBOB, Arrays.asList(qs), 1);
		bobQueues = bob.api.queryForQueuesWithWaitingMessages();
		assertEquals(3, bobQueues.size());
		for (ClientQueue cq : qs) {
			cq.delete();
		}
	}

	@Test
	public void test2() throws QueueCreationException,
			QueueNonExistantException, QueueDeletionException {
		ClientQueue aliceQueue1 = alice.api.createQueue(UUID.randomUUID());
		assertNotNull(aliceQueue1);
		ClientQueue aliceQueue2 = alice.api.createQueue(UUID.randomUUID());
		assertNotNull(aliceQueue2);
		Set<ClientQueue> bobQueues = bob.api
				.queryForQueuesWithWaitingMessages();
		assertTrue(bobQueues.isEmpty());
		ClientMessage alicetobob = aliceQueue1.sendMessage(HELLOBOB, bob.id, 1);
		assertNotNull(alicetobob);
		bobQueues = bob.api.queryForQueuesWithWaitingMessages();
		assertEquals(1, bobQueues.size());
		aliceQueue1.delete();
	}

	@Test
	public void longTest() throws QueueCreationException,
			QueueNonExistantException, QueueDeletionException,
			InterruptedException, DBConnectionException, QueueEmptyException {
		DataMonitor.start(true);
		Random r = new Random();
		for (int i = 0; i < 1000; i++) {
			Thread.sleep(r.nextInt(300));
			ArrayList<ClientQueue> qs = new ArrayList<ClientQueue>();
			for (int q = 0; q < r.nextInt(5) + 1; q++) {
				ClientQueue aliceQueue1 = alice.api.createQueue(UUID
						.randomUUID());
				assertNotNull(aliceQueue1);
				qs.add(aliceQueue1);
			}
			Set<ClientQueue> bobQueues = bob.api
					.queryForQueuesWithWaitingMessages();
			int lim = r.nextInt(qs.size());
			for (int q = 0; q < lim; q++) {
				ClientMessage alicetobob = qs.get(q).sendMessage(HELLOBOB,
						bob.id, 1);
				assertNotNull(alicetobob);
			}
			bobQueues = bob.api.queryForQueuesWithWaitingMessages();
			for (int q = 0; q < lim; q++) {
				try{
				qs.get(0).pop();
				}catch(QueueEmptyException e){
					
				}
			}
		}
	}

}
