from ch.ethz.grmbl.msg import MessageTierFactory
from testcase import MessageTierNode, ClientNode, Node
import sys
from java.lang import Thread

mtn = MessageTierNode()
c1 = ClientNode("client1")
c2 = ClientNode("client2")

mtn.init()
c1.init()
c2.init()
mtn.run()
c1.run()
c2.run()

Thread.sleep(6000)

q = c1.api.createQueue()
q2 = c2.api.getQueueById(q.id)
q.sendMessage("asdf")
msg = q2.pop()
print msg.message == "asdf"


