from ch.ethz.grmbl.util import Config
from ch.ethz.grmbl.client import ClientAPIFactory
from java.util import UUID
from java.lang import System

if __name__== '__main__':
    cnf = Config.get()
    
    cnf.remoteMessageTier = False
    cnf.dataTierImplementation = Config.DataTierImplementation.IN_MEMORY
    api = ClientAPIFactory.createAPI(UUID.randomUUID(), "localhost")
    t1 = System.currentTimeMillis()
    for i in range(200000):
        qid = UUID.randomUUID()
        q = api.createQueue(qid)
        q.sendMessage("hello")
        q2 = api.getQueueById(qid)
        m = q2.pop()
        if not m or m.message != "hello":
            raise Exception("NO!")
        
    t2 = System.currentTimeMillis()
    print t2 - t1
    