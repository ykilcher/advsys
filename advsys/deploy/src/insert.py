import sqlite3,sys,json,os,datetime

if __name__ == '__main__':
    dbfile,path = sys.argv[1],sys.argv[2]
    conn = sqlite3.connect(dbfile)
    try:
        c = conn.cursor()
        tests = os.listdir(path)
        for test in tests:
            print "inserting ",test
            c.execute('select * from exp where name = ?',(test,))
            res = c.fetchall()
            if len(res) > 0:
                print test, " already in db"
                continue
            obj = None
            startTime = None
            with open("%s/%s/info"%(path,test)) as f:
                lines = list(f)
                obj = json.loads(lines[0].replace("'",'"'))
                startTime = float(lines[2])
                obj['startTime'] = round(startTime*1000)
            obj['name'] = test
            obj['atNight'] = int(datetime.datetime.fromtimestamp(startTime).hour in range(0,8))
            attrs = ["name","startTime","nOneWaysQueues","nThreadsInPool","nDatabaseConnections","nProvidersPerService","thinkTime","nServices","nReceivers","messagelen","nPairsQueues","nConsumersPerService","pollPause","nSendersReceiversQueues","nSenders","nPairs","nOneWays","dtimpllen","mtimpllen","atNight","onAmazon","hasError"]
            c.execute('insert into exp (%s) values (%s)'%(",".join(attrs),",".join("?" for _ in attrs)),[str(obj.get(a)) for a in attrs])
        conn.commit()
    finally:
        conn.close()

