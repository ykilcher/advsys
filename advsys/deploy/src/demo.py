from testcase import CentralTestRunner
from tests import buildBattery


if __name__ == '__main__':
    
    dataTierHost = "localhost"
    messageTierHosts = ["localhost"]

    user = "yk"
    asldir = "/usr/tmp/asl"
    libdir = "/usr/tmp/asl" + "/lib"
    localLibDir = "."
    localLogDir = "/usr/tmp/demo/logs"
    
    cases = []
    tests = dict(thinkTime=[0.05], message=["hello"*300], nPairs=[20], \
                 nOneWays=[40], nServices=[2], duration=[30],tracing=True, nThreadsInPool=[3],nDatabaseConnections=[5])
     
    cases = buildBattery("dummytt", dataTierHost, messageTierHosts, **tests)
#     tests = dict(thinkTime=[0.1], message=["hello"*300], nPairs=[20], \
#                  nOneWays=[40], nServices=[2], nThreadsInPool=[16], nDatabaseConnections=[16],duration=[8000],tracing=True)
#     
#     cases = buildBattery("trace", dataTierHost, messageTierHosts, **tests)
    
#     tests = dict(thinkTime=[0.1], message=["hello"*300], nPairs=[20], \
#                  nOneWays=[0], nServices=[0], nThreadsInPool=[16], nDatabaseConnections=[16],duration=[120],tracing=True)
#     cases += buildBattery("pairs", dataTierHost, messageTierHosts,**tests)
#     tests = dict(thinkTime=[0.1], message=["hello"*300], nPairs=[0], \
#                  nOneWays=[40], nServices=[0], nThreadsInPool=[16], nDatabaseConnections=[16],duration=[120],tracing=True)
#     cases += buildBattery("oneways", dataTierHost, messageTierHosts,**tests)
#     tests = dict(thinkTime=[0.1], message=["hello"*300], nPairs=[0], \
#                  nOneWays=[0], nServices=[2], nThreadsInPool=[16], nDatabaseConnections=[16],duration=[120],tracing=True)
#     cases += buildBattery("services", dataTierHost, messageTierHosts,**tests)
    
    print cases
    
    runner = CentralTestRunner()
    runner.setTestCases(cases)
    runner.dataTierMachine = dict(host=dataTierHost, path=asldir + "/dt", lib=libdir, user=user)
    runner.messageTierMachines = [dict(host=mth, path=asldir + "/mt1", lib=libdir, user=user) for mth in messageTierHosts]
    runner.clientTierMachines = []
    for j in range(1, 3):
        runner.clientTierMachines.append(dict(host="localhost", path=asldir + "/ct%d" % j, lib=libdir, user=user))
    runner.runTests(10, localLibDir, localLogDir)
