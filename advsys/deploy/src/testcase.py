import sched, time, os
from ch.ethz.grmbl.msg import MessageTierFactory
from threading import Thread, Timer
from ch.ethz.grmbl.util import Config, Logging
from ch.ethz.grmbl.client import ClientAPIFactory
import java.io as io
import org.python.util as util
import java.util.Properties
from java.util.logging import Logger, Level, FileHandler
from random import randint, shuffle, gauss
import traceback
from ch.ethz.grmbl.monitoring import DataMonitorThread
from java.lang.reflect import UndeclaredThrowableException
from ch.ethz.grmbl.exception import ConnectionClosedException

def serialize(obj, fn):
    fos = io.FileOutputStream(fn)
    oos = io.ObjectOutputStream(fos)
    oos.writeObject(obj)
    fos.close()

def deserialize(fn):
    fis = io.FileInputStream(fn)
    ois = util.PythonObjectInputStream(fis)
    obj = ois.readObject()
    fis.close()
    return obj

class Scheduled(object):
    def __init__(self):
        self._events = []
        self._scheds = []
        self._absolutes = []
        self.delay = 0
    
    def scheduleevent(self, delay, action, args=tuple()):
        self._events.append((delay + self.delay, action, args))
    
    def schedule(self, delay, action, args=tuple()):
        self._scheds.append((delay+self.delay,1,action,args))
        
    def scheduleabs(self, startTime, action, args=tuple()):
        self._absolutes.append((startTime,1,action,args))

    def run(self):
        self.beforeRun()
        self._timers = [Timer(*e) for e in self._events]
        for t in self._timers:
            t.start()
        for s in self._scheds:
            self._schd.enter(*s)
        for a in self._absolutes:
            self._schd.enterabs(*a)
            
        self._schd.run()
    
    def stop(self):
        for e in self._schd.queue:
            self._schd.cancel(e)
        for t in self._timers:
            t.cancel()
        self.running = False

    def beforeRun(self):
        pass
    
    def init(self):
        self._schd = sched.scheduler(time.time, time.sleep)
        self._timers = []
        self.running = True

class Node(Scheduled):
    def __init__(self):
        super(Node, self).__init__()
        self.config = dict()
    
    def configure(self):
        ps = java.util.Properties()
        ps.putAll(self.config)
        Config.get().loadFromProperties(ps)

    def beforeRun(self):
        super(Node,self).beforeRun()
        self.configure()
    
    def sleep(self,pause):
        time.sleep(max(gauss(pause,pause/10.0),0.0))
    
class MessageTierNode(Node):
    def __init__(self, port):
        super(MessageTierNode, self).__init__()
        self.port = port

    def runServer(self):
        self._srv = MessageTierFactory.createMessageTierServer(self.port)
        print "running server"
        self._srv.run()
        
    def stop(self):
        super(MessageTierNode, self).stop()
        self._srv.close()

    def init(self):
        super(MessageTierNode, self).init()
        self.schedule(0, self.runServer)

class ClientNode(Node):
    def __init__(self, clientId, messageTierAddress):
        super(ClientNode, self).__init__()
        self._id = clientId
        self.messageTierAddress = messageTierAddress
    
    def selectPriority(self):
        return 1

    def connect(self):
        Config.get().remoteMessageTier = True
        self.api = ClientAPIFactory.createAPI(self._id, self.messageTierAddress)
    
    def init(self):
        super(ClientNode, self).init()
        self.schedule(0, self.connect)

class DataTierNode(Node):
    def __init__(self):
        super(DataTierNode, self).__init__()
    
    def startCollecting(self):
        t = DataMonitorThread(False)
        t.setDaemon(True)
        t.start()
        while self.running:
            time.sleep(1)

    def init(self):
        super(DataTierNode,self).init()
        self.schedule(0, self.startCollecting)
        
class TestCase(Scheduled):
    def __init__(self, name, description):
        super(TestCase, self).__init__()
        self.name = name
        self.description = description

class RemoteTestCase(TestCase):
    def __init__(self, name, description, nodes, duration, machine):
        super(RemoteTestCase, self).__init__(name, description)
        self.nodes = nodes
        self.duration = duration
        self.nodeThreads = []
        self.machine = machine

    def init(self):
        print "init remotetestcase"
        print self.nodes
        super(RemoteTestCase, self).init()
        lgr = Logger.getLogger("grmbl")
        for h in lgr.getHandlers():
            lgr.removeHandler(h)
        lgr.setUseParentHandlers(False);
        machname = self.machine['host'] + "_".join(self.machine['path'].split('/'))
        h = FileHandler(self.machine['path'] + "/log/tc_" + self.name + "_" + machname + ".log")
        h.setFormatter(Logging.Formatter())
        lgr.addHandler(h)
        
        for n in self.nodes:
            n.init()
        self.schedule(self.duration, self.stop)
    
    def runNode(self, node):
        try:
            node.run()
        except ConnectionClosedException as e:
            print "connection closed"
        except UndeclaredThrowableException as e:
            if isinstance(e.getCause(),ConnectionClosedException):
                print "connection closed"
            else:
                Logging.error().log( Level.SEVERE, "UndeclaredThrowableException " + str(e) + " " + str(traceback.format_exc()).replace('\n','\t') + " " + Logging.printStackTrace(e).toString(),e.getCause())
        except Exception as e:
            Logging.error().log(Level.SEVERE, str(e) + " " + str(traceback.format_exc()).replace('\n','\t'),e)
        except:
            Logging.error().log(Level.SEVERE, "general error " + str(traceback.format_exc()).replace('\n','\t'))
    
    def run(self):
        self.nodeThreads = [Thread(target=self.runNode, args=(n,)) for n in self.nodes]
        for t in self.nodeThreads:
            t.start()
        super(RemoteTestCase, self).run()
    
    def stop(self):
        super(RemoteTestCase, self).stop()
        for n in self.nodes:
            n.stop()
        for t in self.nodeThreads:
            t.join()

class RemoteTestRunner(Scheduled):
    def __init__(self, machine, testCase):
        super(RemoteTestRunner, self).__init__()
        self._testCase = testCase
        self.machine = machine
        self.timediff = 0
        self.timediffFileCreated = 0
        self.startTime= 0

    def configureLogging(self):
        Logger.getLogger("grmbl").setLevel(Level.ALL)  
#         Logger.getLogger("grmbl.timing").setLevel(Level.SEVERE)  
#         Logger.getLogger("grmbl.counting").setLevel(Level.SEVERE)  
 
    def runTestCase(self, testCase):
        print "running testcase %s" % testCase.name
        testCase.run()
    
    def init(self):
        self.configureLogging()
        super(RemoteTestRunner, self).init()
        self._testCase.init()
        print "scheduling testcase %s at %d" % (self._testCase.name, self.startTime + self.timediff)
        self.scheduleabs(self.startTime + self.timediff, self.runTestCase, args=(self._testCase,))
        

class CentralTestCase(TestCase):
    def __init__(self, name, description, duration):
        super(CentralTestCase, self).__init__(name, description)
        self.dataTierNode = None
        self.messageTierNodes = []
        self.clientTierNodes = []
        self.dataTierDelay = 0
        self.messageTierDelay = 0
        self.clientTierDelay = 10
        self.duration = duration

class CentralTestRunner(Scheduled):

    def __init__(self):
        super(CentralTestRunner, self).__init__()
        self.dataTierMachine = ''
        self.messageTierMachines = []
        self.clientTierMachines = []
        self._testCases = []
    
    def setTestCases(self, tcs):
        self._testCases = tcs
    
    def _adjustDelays(self, testCases):
        for tc in testCases:
            if tc.dataTierNode is not None:
                tc.dataTierNode.delay = tc.dataTierDelay
            for n in tc.messageTierNodes:
                n.delay = tc.messageTierDelay
            for n in tc.clientTierNodes:
                n.delay = tc.clientTierDelay
    
    def _collectLogs(self,machine,logDir):
        numb = str(randint(0,2**30))
        os.system('/usr/bin/ssh -o StrictHostKeyChecking=no %(user)s@%(host)s \'cd %(dir)s; python kill.py\'' % dict(numb=numb,user=machine['user'], host=machine['host'], dir=machine['path'], lib=machine['lib']))
        os.system('/usr/bin/ssh -o StrictHostKeyChecking=no %(user)s@%(host)s \'cd %(dir)s/log; cp cmdlog %(logname)s ; tar czf log.%(numb)s.tar.gz ./*.log ./%(logname)s \'' % dict(numb=numb,user=machine['user'], host=machine['host'], dir=machine['path'], lib=machine['lib'],logname='cmdlog_'+machine['host']+"_".join(machine['path'].split('/'))))
        os.system("/usr/bin/scp -o StrictHostKeyChecking=no %s@%s:%s/log/log.%s.tar.gz %s/" % (machine['user'], machine['host'], machine['path'],numb,logDir))
        os.system("cd %s ; tar xzf log.%s.tar.gz ; rm log.%s.tar.gz"%(logDir,numb,numb))
        os.system('/usr/bin/ssh -o StrictHostKeyChecking=no %(user)s@%(host)s \'rm -rf %(dir)s/log/*\'' % dict(user=machine['user'], host=machine['host'], dir=machine['path'], lib=machine['lib']))

    def _afterTestCase(self,testCase,startTime,localLogDir):
        logDir = "%s/%010d_%s" % (localLogDir,startTime,testCase.name)
        os.system("mkdir -m 0700 -p %s" % logDir)
        ts = []
        for m in self.messageTierMachines + self.clientTierMachines + ([self.dataTierMachine] if self.dataTierMachine != '' else []):
            t = Thread(target=self._collectLogs,args=(m,logDir))
            ts.append(t)
            t.start()
        for t in ts:
            t.join()
        with open("%s/info"%logDir,'w') as f:
            f.write(testCase.description + '\n\n' + str(startTime))

    def _afterAllTestCases(self,libMachines):
        if self.dataTierMachine != '':
            machine = self.dataTierMachine
            os.system('/usr/bin/ssh -o StrictHostKeyChecking=no %(user)s@%(host)s \'export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:%(dir)s/db/pgsql/lib"; cd %(dir)s/db/pgsql && ./bin/pg_ctl stop -D %(dir)s/db/pgsql/data\'' % dict(user=machine['user'], host=machine['host'], dir=machine['path'], lib=machine['lib']))
        for m in self.messageTierMachines + self.clientTierMachines + ([self.dataTierMachine] if self.dataTierMachine != '' else []):
            os.system('/usr/bin/ssh -o StrictHostKeyChecking=no %(user)s@%(host)s \'rm -rf %(dir)s/*\'' % dict(user=m['user'], host=m['host'], dir=m['path'], lib=m['lib']))
        for m in libMachines:
            os.system('/usr/bin/ssh -o StrictHostKeyChecking=no %(user)s@%(host)s \'rm -rf %(lib)s/*\'' % dict(user=m['user'], host=m['host'], dir=m['path'], lib=m['lib']))
    
    def _beforeAllTestCases(self,libDirMachines,localLibDir,minilib=False):
        dltts = [Thread(target=self._distributeLibDir,args=(machine,localLibDir,minilib)) for machine in libDirMachines]
        for dlt in dltts:
            dlt.start()
        for dlt in dltts:
            dlt.join()
            
        if self.dataTierMachine != '':
            self._distributeDatabase(self.dataTierMachine)
#         self._adjustDelays(self._testCases)
        for machine in self.messageTierMachines + self.clientTierMachines + ([self.dataTierMachine] if self.dataTierMachine != '' else []):
            res = os.system('/usr/bin/ssh -o StrictHostKeyChecking=no %(user)s@%(host)s \'ulimit -S -n `ulimit -H -n` ;mkdir -m 0700 -p %(dir)s/log; cp %(lib)s/*.jar %(dir)s/;  cp %(lib)s/*.py  %(dir)s/; cd %(dir)s \'' % dict(user=machine['user'], host=machine['host'], dir=machine['path'], lib=machine['lib']))
            if int(res) != 0:
                print "ERROR %s while sshing" % res
    
    def _distributeTestRunners(self, testRunners):
        trthreads = []
        print "distributing test runners"
        for tr in testRunners:
            t = Thread(target=self._putToMachine,args=(tr.machine, tr))
            t.start()
            trthreads.append(t)
        for t in trthreads:
            t.join()
        print "distributing test done"
    
    def _startTestRunner(self,machine,testRunner,startTime):
        print "starting test runner on ",machine
        res = os.system('/usr/bin/ssh -o StrictHostKeyChecking=no %(user)s@%(host)s \'cd %(dir)s; java -jar asl.jar remotemain.py testrunner %(time)s %(start)s < /dev/null > log/cmdlog 2>&1 &\'' % dict(user=machine['user'], host=machine['host'], dir=machine['path'], lib=machine['lib'], time=str(testRunner.timediffFileCreated), start=str(startTime)))
        if int(res) != 0:
            print "ERROR %s while sshing" % res

    def _startTestRunners(self,testRunners,startTime):
        trthreads = []
        print "starting test for runners ",testRunners
        for tr in testRunners:
            t = Thread(target=self._startTestRunner,args=(tr.machine, tr, startTime))
            t.start()
            trthreads.append(t)
        for t in trthreads:
            t.join()
        print "starting test done"
    
    def _putToMachine(self, machine, testRunner):
        print ("distributing testrunner to ", machine)
        numb = randint(0,2**30)
        tmpfn = 'tmptc_' + str(numb)
        serialize(testRunner, tmpfn)
        res = os.system('cat %s | /usr/bin/ssh -o StrictHostKeyChecking=no %s@%s \' cat - > %s/testrunner\'' % (tmpfn,machine['user'], machine['host'], machine['path']))
        if int(res) != 0:
            print "ERROR %s while sshing" % res
        testRunner.timediffFileCreated = time.time()
        os.remove(tmpfn)
    
    def _distributeLibDir(self,machine,localLibDir,minilib=False):
        print "distributing lib dir to " + str(machine)
        if minilib:
            print "minilib"
        res = os.system('/usr/bin/ssh -o StrictHostKeyChecking=no %(user)s@%(host)s \'mkdir -m 0700 -p %(lib)s\'' % dict(llib=localLibDir,user=machine['user'],host=machine['host'],lib=machine['lib']))
        if int(res) != 0:
            print "ERROR %s while sshing" % res
        if minilib:
            res = os.system('/usr/bin/scp -o StrictHostKeyChecking=no  %(llib)s/*.py %(user)s@%(host)s:%(lib)s/' % dict(llib=localLibDir,user=machine['user'],host=machine['host'],lib=machine['lib']))
            res = os.system('/usr/bin/ssh -o StrictHostKeyChecking=no %(user)s@%(host)s \'cp /home/%(user)s/pgsql.tar.gz /home/%(user)s/asl.jar %(lib)s/ \'' % dict(llib=localLibDir,user=machine['user'],host=machine['host'],lib=machine['lib']))
        else:
            res = os.system('/usr/bin/scp -o StrictHostKeyChecking=no %(llib)s/*.jar %(llib)s/*.py %(llib)s/pgsql.tar.gz %(user)s@%(host)s:%(lib)s/' % dict(llib=localLibDir,user=machine['user'],host=machine['host'],lib=machine['lib']))
        if int(res) != 0:
            print "ERROR %s while scping" % res
        print "distributing lib dir done"
    
    def _distributeDatabase(self,machine):
        print "distributing database"
        res = os.system('/usr/bin/ssh -o StrictHostKeyChecking=no %(user)s@%(host)s \' mkdir -m 0700 -p %(dir)s/db ; cp %(lib)s/pgsql.tar.gz %(dir)s/db/ ; cd %(dir)s/db ; tar xzf pgsql.tar.gz ; cd pgsql; export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:%(dir)s/db/pgsql/lib"; ./bin/pg_ctl start -D %(dir)s/db/pgsql/data < /dev/null > dblog 2>&1 &\'' % dict(user=machine['user'], host=machine['host'], dir=machine['path'], lib=machine['lib']))
        if int(res) != 0:
            print "ERROR %s while sshing" % res
        time.sleep(3);
        res = os.system('/usr/bin/ssh -o StrictHostKeyChecking=no %(user)s@%(host)s \' cd %(dir)s/db ; cd pgsql; export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:%(dir)s/db/pgsql/lib"; bash deployDB.sh\'' % dict(user=machine['user'], host=machine['host'], dir=machine['path'], lib=machine['lib']))
        if int(res) != 0:
            print "ERROR %s while sshing" % res
        print "started database"

    def _distributeTestCase(self, tc,localLibDir):
        tcs = dict()
        nmtms = len(self.messageTierMachines)
        nctms = len(self.clientTierMachines)
        
        if tc.dataTierNode is not None:
            tcs[tuple(self.dataTierMachine.items())] =RemoteTestCase(tc.name, tc.description, [tc.dataTierNode], tc.duration, self.dataTierMachine)
        nmtns = len(tc.messageTierNodes)
        nctns = len(tc.clientTierNodes)
        shuffle(tc.clientTierNodes)
        for i in range(min(nmtns, nmtms)):
            tcs[tuple(self.messageTierMachines[i].items())]=RemoteTestCase(tc.name, tc.description, tc.messageTierNodes[i::nmtms], tc.duration,self.messageTierMachines[i])
        for i in range(min(nctns, nctms)):
            tcs[tuple(self.clientTierMachines[i].items())]=RemoteTestCase(tc.name, tc.description, tc.clientTierNodes[i::nctms], tc.duration, self.clientTierMachines[i])

        testRunners = [RemoteTestRunner(dict(m), t) for m, t in tcs.iteritems() if t is not None]
        print testRunners
        self._distributeTestRunners(testRunners)
        return testRunners
    
    def _startTestCase(self,testRunners,startTime):
        self._startTestRunners(testRunners, startTime)
    
    def init(self):
        super(CentralTestRunner, self).init()
    
    def runTests(self, pauseBetweenTests,localLibDir,localLogDir,libToAll=False,startDelay=20,minilib=False):
        from tests import buildCase
        self.init()
        os.system('mkdir -m 0700 -p %s' % localLogDir)
        libMachines=[self.dataTierMachine]
        if libToAll:
            libMachines = libMachines + self.messageTierMachines + self.clientTierMachines
        self._beforeAllTestCases(libMachines, localLibDir,minilib=minilib)
        for i,tct in zip(range(len(self._testCases)),self._testCases):
            print "preparing test case %s" % tct[0]
            tc = buildCase(tct[0], tct[1], tct[2], tct[3],**tct[4])
            self._adjustDelays([tc])
            print "distributing test case %s" % tc.name
            trunners = self._distributeTestCase(tc, localLibDir)
            startTime = time.time() + startDelay
            print "starting test case %s" % tc.name
            self._startTestCase(trunners,startTime)
            print "started test case %s, (%d of %d)" % (tc.name,i+1,len(self._testCases))
            cd = startDelay + tc.duration + pauseBetweenTests
            time.sleep(cd)
            print "test case %s done, collecting..." % tc.name
            self._afterTestCase(tc,startTime, localLogDir)
        
        print "all tests done, cleaning up"
        self._afterAllTestCases(libMachines)
