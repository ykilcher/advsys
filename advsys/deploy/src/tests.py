import collections
from java.util import UUID

from nodes import QueueCreatingMessageTier, createRequestRespondPairs, \
    createService, createOneWays
from testcase import CentralTestCase, DataTierNode, \
    MessageTierNode
from copy import deepcopy

def createQueueIds(num):
    return [UUID.randomUUID() for _ in range(num)]

def buildCase(tcname, dataTierHost, messageTierHosts, mtport=44444, thinkTime=0.02,\
               message="hello", nMessagesInDB=0,nPairs=0, nPairsQueues = 10, nOneWays=0, nOneWaysQueues = 10,\
                nSenders=0, nReceivers=0, nSendersReceiversQueues=10,nServices=0, \
                nProvidersPerService=5, nConsumersPerService=10, nThreadsInPool=8,nDatabaseConnections=8,dtimpl='JDBC',mtimpl='NON_BLOCKING',duration=240,clientTierDelay=10,messageTierDelay=0,tracing=True):

    pollPause = thinkTime
    
    params = dict((k+"len",len(v)) if isinstance(v,collections.Iterable) else (k,v) for k,v in locals().iteritems())
    
    print "building case " + str(params)

    clientTiers = []
    queueIds = []
    pairQueueIds = createQueueIds(nPairsQueues)
    queueIds += pairQueueIds
    clientTiers += createRequestRespondPairs("", pairQueueIds, nPairs, pollPause, thinkTime)
    for i in range(nServices):
        requestQueueId,responseQueueId = createQueueIds(2)
        queueIds.append(requestQueueId)
        queueIds.append(responseQueueId)
        clientTiers += createService("", requestQueueId, responseQueueId, nProvidersPerService, nConsumersPerService, pollPause, thinkTime)
    oneWaysQueueIds = createQueueIds(nOneWaysQueues)
    clientTiers += createOneWays("", nOneWays, oneWaysQueueIds, pollPause, thinkTime)
    queueIds += oneWaysQueueIds
    
    nmth = len(messageTierHosts)
    for i in range(nmth):
        mta = messageTierHosts[i] + ":" + str(mtport)
        for ct in clientTiers[i::nmth]:
            ct.messageTierAddress = mta

    #TODO messages in db
    mtport = 44444
    messageTiers = []
    messageTiers.append(QueueCreatingMessageTier(mtport, queueIds,nMessagesInDB))
    if len(messageTierHosts) > 1:
        for _ in messageTierHosts[1:]:
            messageTiers.append(MessageTierNode(mtport))
    
    dataTierNode = DataTierNode()
    
    dataTierNode.config['dbServerName'] = dataTierHost
    dataTierNode.config['dataTierImplementation'] = dtimpl
    
    for mt in messageTiers:
        mt.config['dbServerName'] = dataTierHost
        mt.config['nThreadsInPool']= str(nThreadsInPool)
        mt.config['dbMaxConnections'] = str(nDatabaseConnections)
        mt.config['messageTierRemotingImplementation'] = mtimpl
    
    for ct in clientTiers:
        if tracing:
            ct.tracing = True
        if 'message' in ct.__dict__:
            ct.message = message
        
    case = CentralTestCase(tcname, str(params), duration)
    case.dataTierNode = dataTierNode
    case.messageTierNodes = messageTiers
    case.clientTierNodes = clientTiers
    
    case.clientTierDelay = clientTierDelay
    case.messageTierDelay = messageTierDelay
    
    return case

def buildBattery(tcname, dataTierHost, messageTierHosts, mtport=44444,**kwargs):
    maps = []
    for param,vallist in kwargs.iteritems():
        if not isinstance(vallist,collections.Iterable):
            vallist = [vallist]
        if len(maps) > 0:
            nmaps = []
            for val in vallist:
                for mp in maps:
                    nmap = deepcopy(mp)
                    nmap[param] = val
                    nmaps.append(nmap)
            maps = nmaps
        else:
            maps = [dict([(param,val)]) for val in vallist]
    
    cases = [(tcname, dataTierHost, messageTierHosts, mtport,mp) for mp in maps]
    print "prepared %d cases" % len(cases)
    return cases
    


