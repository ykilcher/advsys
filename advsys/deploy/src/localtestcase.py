from testcase import CentralTestCase, CentralTestRunner,\
    DataTierNode,MessageTierNode
from java.util import UUID
import time
from nodes import QueueCreatingMessageTier, createRequestRespondPair

if __name__ == '__main__':
    queueIds = [UUID.randomUUID() for i in range(1, 10)]
    mtport = 44444
    messageTierHost1 = "localhost"
    mtaddress1 = messageTierHost1 + ":" + str(mtport)
    messageTiers = []
    messageTiers.append(QueueCreatingMessageTier(mtport,queueIds))
    
    dataTierHost = 'localhost'
    
    dataTierNode = DataTierNode()
    
    for mt in messageTiers:
        mt.config['dbServerName'] = dataTierHost

    nclientsPerMT = 40
    
    clientTiers = []
    for i in range(nclientsPerMT):
        reqc,respc = createRequestRespondPair(mtaddress1, queueIds)
        clientTiers.append(reqc)
        clientTiers.append(respc)
    
    case = CentralTestCase("localTest", "", 120,1)
    case.dataTierNode = dataTierNode
    case.messageTierNodes = messageTiers
    case.clientTierNodes = clientTiers
    
    case.clientTierDelay = 10
    
    user = "yk"
    host = "localhost"
    asldir= "/home/" + user + "/tmp/asl"
    libdir = "/home/"+ user+"/tmp" + "/lib"
    localLibDir = "."
    localLogDir = "/usr/tmp/local"
    
    runner = CentralTestRunner()
    runner.setTestCases([case])
    runner.dataTierMachine = dict(host=dataTierHost, path=asldir+"/dt", lib=libdir, user=user)
    runner.messageTierMachines = [dict(host=messageTierHost1, path=asldir+"/mt1", lib=libdir, user=user)]
    runner.clientTierMachines = []
    for j in range(1,3):
        runner.clientTierMachines.append(dict(host="localhost", path=asldir+"/ct%d"%j, lib=libdir, user=user))
    runner.runTests(time.time() + 20, 5,localLibDir,localLogDir)
