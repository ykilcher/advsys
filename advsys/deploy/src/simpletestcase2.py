from testcase import CentralTestRunner
from tests import buildBattery
from random import shuffle


if __name__ == '__main__':
    
    dataTierHost = "dryad09"
    messageTierHosts = ["dryad10"]

    user = "user02"
    asldir = "/home/" + user + "/tmp/asl"
    libdir = "/mnt/asl/user02" + "/lib"
    localLibDir = "."
#     localLogDir = "/usr/tmp/logs"
    localLogDir = "/home/user02/tmp/logs"
    
    cases = []
    
    tests = dict(thinkTime=[0.05,0.5], nOneWays=[20,1000])
    cases += buildBattery("doublevariance", dataTierHost, messageTierHosts,**tests)
    tests = dict(thinkTime=[0.05,0.5],nPairs=[10,500])
    cases += buildBattery("doublevariance", dataTierHost, messageTierHosts,**tests)
    tests = dict(message=["hello","hello"*200],nPairs=[10,500])
    cases += buildBattery("doublevariance", dataTierHost, messageTierHosts,**tests)
    tests = dict(nPairs=[10,500],nOneWays=[20,1000])
    cases += buildBattery("doublevariance", dataTierHost, messageTierHosts,**tests)
    tests = dict(nPairs=[10,500],nPairsQueues=[1,100])
    cases += buildBattery("doublevariance", dataTierHost, messageTierHosts,**tests)
    tests = dict(nServices=[1,30],nProvidersPerService=[1,30],nConsumersPerService=[1,30])
    cases += buildBattery("doublevariance", dataTierHost, messageTierHosts,**tests)
    tests = dict(nOneWays=[20,1000],nOneWaysQueues=[1,100])
    cases += buildBattery("doublevariance", dataTierHost, messageTierHosts,**tests)
    tests = dict(nThreadsInPool=[1,10],nDatabaseConnections=[1,50],nOneWays=[600],nPairs=[400],nServices=[50])
    cases += buildBattery("doublevariance", dataTierHost, messageTierHosts,**tests)
    tests = dict(thinkTime=[0.01,0.05,0.2,0.5],nPairs=[100], nOneWays=[100],nServices=[20])
    cases += buildBattery("singlevariance", dataTierHost, messageTierHosts,**tests)
    tests = dict(nPairs=[10,100,500,2000])
    cases += buildBattery("singlevariance", dataTierHost, messageTierHosts,**tests)
    tests = dict( nOneWays=[50,200,800,3500])
    cases += buildBattery("singlevariance", dataTierHost, messageTierHosts,**tests)
    tests = dict( nServices=[5,20,50,200])
    cases += buildBattery("singlevariance", dataTierHost, messageTierHosts,**tests)
    tests = dict(mtimpl=['NON_BLOCKING','NAIVE'],nOneWays=[600],nPairs=[400],nServices=[50])
    cases += buildBattery("singlevariance", dataTierHost, messageTierHosts,**tests)
    tests = dict(dtimpl=['JDBC','JDBCv2'],nOneWays=[600],nPairs=[400],nServices=[50])
    cases += buildBattery("singlevariance", dataTierHost, messageTierHosts,**tests)
    tests = dict(nMessagesInDB=[1,1000,50000,1000000],nOneWays=[600],nPairs=[400],nServices=[50])
    cases += buildBattery("singlevariance", dataTierHost, messageTierHosts,**tests)
    tests = dict(nThreadsInPool=[1,10,50,200],nOneWays=[600],nPairs=[400],nServices=[50])
    cases += buildBattery("singlevariance", dataTierHost, messageTierHosts,**tests)
    tests = dict(nDatabaseConnections=[1,10,50,200],nOneWays=[600],nPairs=[400],nServices=[50])
    cases += buildBattery("singlevariance", dataTierHost, messageTierHosts,**tests)
    print cases
    
    shuffle(cases)

    tests = dict(nPairs=[100], nOneWays=[100], nServices=[10],duration=600)
    cases = buildBattery("warmup", dataTierHost, messageTierHosts, **tests) + cases
    
    runner = CentralTestRunner()
    runner.setTestCases(cases)
    runner.dataTierMachine = dict(host=dataTierHost, path=asldir + "/dt", lib=libdir, user=user)
    runner.messageTierMachines = [dict(host=mth, path=asldir + "/mt1", lib=libdir, user=user) for mth in messageTierHosts]
    runner.clientTierMachines = []
    for i in [11,12]:
        for j in range(1, 3):
            runner.clientTierMachines.append(dict(host="dryad%02d" % i, path=asldir + "/ct%d" % j, lib=libdir, user=user))
    runner.runTests(10, localLibDir, localLogDir)
