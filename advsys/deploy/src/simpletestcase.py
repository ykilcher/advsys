from testcase import CentralTestRunner
from tests import buildBattery
from random import shuffle


if __name__ == '__main__':
    
    dataTierHost = "dryad11"
    messageTierHosts = ["dryad12", "dryad13", "dryad14", "dryad15"]

    user = "user02"
    asldir = "/home/" + user + "/tmp/asl"
    libdir = "/mnt/asl/user02" + "/lib"
    localLibDir = "."
    localLogDir = "/usr/tmp/logs"
    
    tests = dict(thinkTime=[0.01, 0.1], message=["hello", "hello"*300], nPairs=[20, 1000], \
                 nOneWays=[50, 2000], nServices=[1, 100], nThreadsInPool=[2, 50], nDatabaseConnections=[2, 50],\
                 dtimpl=['JDBC', 'JDBCv2'], mtimpl=['NON_BLOCKING', 'NAIVE'])
    
    cases = buildBattery("simplebattery", dataTierHost, messageTierHosts, **tests)
    
    print cases
    
    shuffle(cases)
    
    runner = CentralTestRunner()
    runner.setTestCases(cases)
    runner.dataTierMachine = dict(host=dataTierHost, path=asldir + "/dt", lib=libdir, user=user)
    runner.messageTierMachines = [dict(host=mth, path=asldir + "/mt1", lib=libdir, user=user) for mth in messageTierHosts]
    runner.clientTierMachines = []
    for i in [1, 2, 3, 4, 6, 7, 8, 9, 10]:
        for j in range(1, 3):
            runner.clientTierMachines.append(dict(host="dryad%02d" % i, path=asldir + "/ct%d" % j, lib=libdir, user=user))
    runner.runTests(10, localLibDir, localLogDir)
