import sys,os
from testcase import deserialize
from ch.ethz.grmbl.util import Logging
from java.util.logging import Level
from threading import Thread

class StatCollector():
    def __init__(self):
        self.running = True
    
    def collectStats(self):
        while self.running:
            os.system("vmstat 1 2 > stat")
            with open('stat','r') as f:
                lines = f.read().splitlines()
                if len(lines) == 4:
                    Logging.status().log(Level.FINE,"StatCollector vmstat " + lines[3])

if __name__ == '__main__':
    print "hello remote"
    with open('pid','w') as f:
        f.write(str(os.getpid()))
    abspath = os.path.abspath(__file__)
    dname=os.path.dirname(abspath)
    os.chdir(dname)
    trfn = sys.argv[1]
    centralTime = float(sys.argv[2])
    startTime = float(sys.argv[3])
    localTime = os.path.getmtime(trfn)
    timediff = localTime - centralTime
    print "timediff: %d" % timediff
    testRunner = deserialize(trfn)
    testRunner.timediff = timediff
    testRunner.startTime = startTime
    testRunner.init()
    
    sc = StatCollector()
    sct = Thread(target=sc.collectStats)
    sct.setDaemon(True)
    sct.start()
    
    testRunner.run()
    sc.running = False
    
    os.remove('pid')
