import os,subprocess,sys,time

if __name__ == '__main__':
    libdir = sys.argv[1]
    watchdir = sys.argv[2]
    targetdir = sys.argv[3]
    l = []
    while True:
        lw = os.listdir(watchdir)
        lt = os.listdir(targetdir)
        for lwe in lw:
            if lwe not in lt and 'info' in os.listdir(watchdir+'/'+lwe):
                print "analyzing " + lwe
                command = "mkdir -p %s/%s ; python %s/analyze.py %s/%s %s/%s" % (targetdir,lwe,libdir,watchdir,lwe,targetdir,lwe)
                process = subprocess.Popen(command,shell=True,stdout=subprocess.PIPE)
                process.wait()
                print process.returncode
        time.sleep(1)