from testcase import CentralTestRunner
from tests import buildBattery
from random import shuffle


if __name__ == '__main__':
    
    dataTierHost = "ec2-54-194-34-113.eu-west-1.compute.amazonaws.com"
    messageTierHosts = ["ec2-54-194-25-83.eu-west-1.compute.amazonaws.com","ec2-54-194-39-23.eu-west-1.compute.amazonaws.com"]

    user = "ec2-user"
    asldir = "/home/" + user + "/tmp/asl"
    libdir = asldir + "/lib"
    localLibDir = "."
    localLogDir = "/usr/tmp/amaintvars"
    
    cases = []
#     tests = dict(duration=[400],tracing=True,thinkTime=[0.05,0.01,0.005,0.001],nPairs=[40], nOneWays=[80],nServices=[6])
#     cases += buildBattery("singlevariance_thinktime", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,nPairs=[10,100,500,2000])
#     cases += buildBattery("singlevariance_pairs", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,message=["hello","hello"*30,"hello"*300],nPairs=[100], nOneWays=[100],nServices=[20])
#     cases += buildBattery("singlevariance_message", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,thinkTime=[0.01,0.1],nPairs=[10,200])
#     cases += buildBattery("doublevariance_thinktime_pairs", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,nPairs=[10,500],nPairsQueues=[1,100])
#     cases += buildBattery("doublevariance_pairs_pairsqueues", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,nServices=[2],nProvidersPerService=[2,30],nConsumersPerService=[4,60])
#     cases += buildBattery("doublevariance_providers_consumers", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,nOneWays=[20,1000],nOneWaysQueues=[1,100])
#     cases += buildBattery("doublevariance_oneways_onewaysqueues", dataTierHost, messageTierHosts,**tests)
    tests = dict(duration=[400],tracing=True,nThreadsInPool=[1,20],nDatabaseConnections=[1,20],nOneWays=[200],nPairs=[100],nServices=[20])
    cases += buildBattery("doublevariance_threads_connections", dataTierHost, messageTierHosts,**tests)
    tests = dict(duration=[400],tracing=True,nThreadsInPool=[1,10,50,200],nOneWays=[200],nPairs=[100],nServices=[20])
    cases += buildBattery("singlevariance_threads", dataTierHost, messageTierHosts,**tests)
    tests = dict(duration=[400],tracing=True,nDatabaseConnections=[1,10,50,200],nOneWays=[200],nPairs=[100],nServices=[20])
    cases += buildBattery("singlevariance_connections", dataTierHost, messageTierHosts,**tests)

#     tests = dict(duration=[400],tracing=True,nPairs=[400])
#     cases += buildBattery("scaleout4", dataTierHost, messageTierHosts,**tests)

#     tests = dict(tracing=True,nPairs=[100], nOneWays=[100], nServices=[10],duration=400,mtimpl=['NAIVE'])
#     cases = buildBattery("naive", dataTierHost, messageTierHosts, **tests) + cases

#     tests = dict(tracing=True,nPairs=[5], nOneWays=[10], nServices=[1],duration=400)
#     cases = buildBattery("samemachine", dataTierHost, messageTierHosts, **tests) + cases

#     tests = dict(duration=[400],tracing=True, nOneWays=[50,200,800,3500])
#     cases += buildBattery("singlevariance_oneways", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True, nServices=[5,20,50,200])
#     cases += buildBattery("singlevariance_services", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,mtimpl=['NON_BLOCKING','NAIVE'],nOneWays=[600],nPairs=[400],nServices=[50])
#     cases += buildBattery("singlevariance", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,dtimpl=['JDBC','JDBCv2'],nOneWays=[600],nPairs=[400],nServices=[50])
#     cases += buildBattery("singlevariance", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,nMessagesInDB=[1,1000,50000,1000000],nOneWays=[600],nPairs=[400],nServices=[50])
#     cases += buildBattery("singlevariance", dataTierHost, messageTierHosts,**tests)
    
#     tests = dict(thinkTime=[0.01,0.1], nOneWays=[20,1000])
#     cases += buildBattery("doublevariance", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,message=["hello","hello"*200],nPairs=[10,500])
#     cases += buildBattery("doublevariance_msgsize_pairs", dataTierHost, messageTierHosts,**tests)
#     tests = dict(nPairs=[10,500],nOneWays=[20,1000])
#     cases += buildBattery("doublevariance", dataTierHost, messageTierHosts,**tests)

#     tests = dict(thinkTime=[0.01], message=["hello"], nPairs=[20], \
#                   nOneWays=[50], nServices=[2], nThreadsInPool=[8], nDatabaseConnections=[8],duration=[7500],tracing=True)
#     cases = buildBattery("trace", dataTierHost, messageTierHosts, **tests)
    
#     shuffle(cases)

#     tests = dict(tracing=True,nPairs=[100], nOneWays=[100], nServices=[10],duration=600)
#     cases = buildBattery("warmup", dataTierHost, messageTierHosts, **tests) + cases

    print cases
    
    runner = CentralTestRunner()
    runner.setTestCases(cases)
    runner.dataTierMachine = dict(host=dataTierHost, path=asldir + "/dt", lib=libdir, user=user)
    runner.messageTierMachines = [dict(host=mth, path=asldir + "/mt1", lib=libdir, user=user) for mth in messageTierHosts]
    runner.clientTierMachines = []
    for i in [\
       "ec2-54-194-34-180.eu-west-1.compute.amazonaws.com",\
        "ec2-54-194-25-124.eu-west-1.compute.amazonaws.com",\
        "ec2-54-194-20-63.eu-west-1.compute.amazonaws.com",\
        "ec2-54-194-16-233.eu-west-1.compute.amazonaws.com",\
        "ec2-54-194-30-249.eu-west-1.compute.amazonaws.com" ,\
       "ec2-54-194-42-118.eu-west-1.compute.amazonaws.com"\
        ]:
        for j in range(1, 3):
            runner.clientTierMachines.append(dict(host="%s" % i, path=asldir + "/ct%d" % j, lib=libdir+"/ct%d"%j, user=user))
    runner.runTests(30, localLibDir, localLogDir,libToAll=True,startDelay=30,minilib=True)
