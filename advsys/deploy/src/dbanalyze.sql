-- Describe EXP
CREATE TABLE exp (
        "id" INTEGER PRIMARY KEY,
        "name" TEXT NOT NULL,
        "startTime" INTEGER NOT NULL,
        "nOneWaysQueues" INTEGER,
        "nThreadsInPool" INTEGER,
        "nDatabaseConnections" INTEGER,
        "nProvidersPerService" INTEGER,
        "thinkTime" INTEGER,
        "nServices" INTEGER,
        "nReceivers" INTEGER,
        "messagelen" INTEGER,
        "nPairsQueues" INTEGER,
        "nConsumersPerService" INTEGER,
        "pollPause" INTEGER,
        "nSendersReceiversQueues" INTEGER,
        "nSenders" INTEGER,
        "nPairs" INTEGER,
        "nOneWays" INTEGER,
        "dtimpllen" INTEGER,
        "mtimpllen" INTEGER,
        "atNight" INTEGER,
        "onAmazon" INTEGER,
        "hasError" INTEGER
        , "nMessagesInDB" INTEGER);
