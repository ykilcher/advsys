import sys,os,json,re
from matplotlib import pyplot, cm, rcParams
from scipy.stats.morestats import bayes_mvs
from numpy.core.numeric import arange, array
from numpy.lib.function_base import percentile, meshgrid
from itertools import count
from numpy.core.fromnumeric import argsort
from numpy.ma.extras import dot
from mpl_toolkits.mplot3d import axes3d
from numpy.core.function_base import linspace
from matplotlib.mlab import griddata

rcParams['legend.loc'] = 'best'

folder = None
showStuff = True
tests = []
mode = None
parameterMap=dict(messageTierHostslen="Number of Middleware Machines",\
                  nPairs="Number of Request-Response Pairs",\
                  messagelen="message size in bytes",\
                  thinkTime="Think Time")

interestList = [("Timing_RemotingClient_doRequest",'milliseconds','Client Response Time'),\
                ("Timing_DataTierInDBImpl","milliseconds","Data Tier response time"),\
                ("Timing_PendingTask_inQueue","milliseconds","Middleware queue time"),\
                ("Timing_PendingTask_run","milliseconds","Middleware run time"),\
                ("Number_of_times_messages_exchanged_OneWayClient","number of exchanged messages","Number of messages for One Way Clients"),\
                ("Number_of_times_messages_exchanged_RequestClient","number of exchanged messages","Number of messages for Requesting Clients"),\
                ("Number_of_times_messages_exchanged_RespondingClient","number of exchanged messages","Number of messages for Responding Clients"),\
                ("Number_of_times_messages_exchanged_ServiceConsumingClient","number of exchanged messages","Number of messages for Service Consuming Clients"),\
                ("Number_of_times_messages_exchanged_ServiceProvidingClient","number of exchanged messages","Number of messages for Service Providing Clients"),\
                ("data_on_network_RemotingClient_writeRequest",'bytes per second','Outgoing Throughput on client'),\
                ("data_on_network_ChannelBuffer_readDataBuffer",'bytes per second','Incoming Throughput on middleware'),\
                ("data_on_network_ChannelBuffer_writeBuffer",'bytes per second','Outgoing Throughput on middleware'),\
                ("throughput_requests_per_second_DataTierInDBImpl",'requests per second','Requests per second on database'),\
                ("throughput_requests_per_second_PendingTask_run",'requests per second','Requests per second on middleware'),\
                ("throughput_requests_per_second_RemotingClient_writeRequest",'requests per second','Outgoing Throughput on client')\
                ]

def getName(var):
    found = None
    for f in interestList:
        if f[0] == var:
            found = f[2]
            break
    return parameterMap.get(var) or found or var

def roundsig(flt,dgts=3):
    return "%s" % float(('%.'+str(dgts)+'g')%flt)

signTable = array([[1,1,1,1],[-1,1,-1,1],[-1,-1,1,1],[1,-1,-1,1]])

signTableTemplate = r"""
\begin{table}
\centering
\subsubsection*{Sign Table (%(Y)s)}
\begin{tabular}[c]{ccccl}
I & A & B & AB & y \\
\hline
1 & -1 & -1 & 1 & %(res_I)s \\
1 & 1 & -1 & -1 & %(res_A)s \\
1 & -1 & 1 & -1 & %(res_B)s \\
1 & 1 & 1 & 1 & %(res_AB)s \\
\hline
%(tot_I)s & %(tot_A)s & %(tot_B)s & %(tot_AB)s & Total \\
%(tot_I_4)s & %(tot_A_4)s & %(tot_B_4)s & %(tot_AB_4)s & Total/4 \\
\end{tabular}
\subsubsection*{Allocation of Variance}
\begin{tabular}[p]{rrr}
Total Variation: & %(var_tot)s & %(var_tot_p)s\%(prc)s \\
%(A)s: & %(var_A)s & %(var_A_p)s\%(prc)s \\
%(B)s: & %(var_B)s & %(var_B_p)s\%(prc)s \\
Both: & %(var_AB)s & %(var_AB_p)s\%(prc)s \\
\end{tabular}
\caption{$2^k$ factorial analysis,
A = %(A)s, B = %(B)s}
\end{table}
"""

def print2kTable(var1,var2,vary,As,Bs,Ys,stres,SS,fracvar):
    stres4 = stres*4
    ks = dict(res_I=roundsig(Ys[0]),res_A=roundsig(Ys[1]),res_B=roundsig(Ys[2]),res_AB=roundsig(Ys[3]),\
              tot_I = roundsig(stres4[0]),tot_A=roundsig(stres4[1]),tot_B=roundsig(stres4[2]),tot_AB=roundsig(stres4[3]),\
              tot_I_4 = roundsig(stres[0]),tot_A_4=roundsig(stres[1]),tot_B_4=roundsig(stres[2]),tot_AB_4=roundsig(stres[3]),\
              var_tot = roundsig(sum(SS)),var_A=roundsig(SS[0]),var_B=roundsig(SS[1]),var_AB=roundsig(SS[2]),\
              var_tot_p = 100,var_A_p=roundsig(fracvar[0]),var_B_p=roundsig(fracvar[1]),var_AB_p=roundsig(fracvar[2]),\
              A = getName(var1), B = getName(var2),Y=vary[2],prc='%')
    return signTableTemplate % ks

def do2kForY(var1,var2,vary):
    if len(tests) != 4:
        raise Exception("not enough tests")
    for t in tests:
        if var1 not in t[1] or var2 not in t[1]:
            raise Exception('vars not here')
    As,Bs = zip(*[(t[1][var1],t[1][var2]) for t in tests])
    aAs,aBs = argsort(As)/2,argsort(Bs)/2
    tups = zip(aAs,aBs)
    for i,tup in zip(count(),[(0,0),(1,0),(0,1),(1,1)]):
        iI = tups.index(tup)
        if iI != i:
            tups[iI],tups[i],tests[iI],tests[i] = tups[i],tups[iI],tests[i],tests[iI]
    As,Bs,Ys = array(zip(*[(t[1][var1],t[1][var2],t[2][vary[0]]['mean'][0]) for t in tests]))
    stres = array([dot(Ys,signTable[i]) for i in range(len(tests))])/4.
    SS = stres[1:]**2*4
    fracvar = SS/sum(SS)
    print vary,As,Bs,Ys,stres,fracvar
    print print2kTable(var1, var2, vary, As, Bs, Ys, stres, SS, fracvar)
    print '\n\n'
    
    fig = pyplot.figure()
    ax = fig.gca(projection='3d')
    X = linspace(min(As),max(As),100)
    Y = linspace(min(Bs),max(Bs),100)
    X,Y = meshgrid(X,Y)
    Z = griddata(As,Bs,Ys,X,Y)
    
    ax.plot_surface(X, Y, Z, rstride=8, cstride=8, alpha=0.3)

    ax.set_title('%s (%s vs %s)' % tuple(getName(n) for n in [vary[0],var1,var2]))
    ax.set_xlabel(getName(var1))
    ax.set_ylabel(getName(var2))
    ax.set_zlabel(vary[1])
    ax.set_zlim(bottom=0)

    savePlot(fig,'2kf_' + vary[0] + "_" + vary[1])
    

def do2k(var):
    for interest in interestList:
        if any([interest[0] not in t[2] for t in tests]): continue
        do2kForY(var[0], var[1], interest)


def savePlot(plot,name):
    if folder:
        plot.savefig(folder + '/' + name + ".png")

def doSingleForY(var,vary):
    stats = array(zip(*[(t[1][var],t[2][vary[0]]['mean'][0],t[2][vary[0]]['std'][0],t[2][vary[0]]['pc']) for t in tests]))
    fig = pyplot.figure()
    ax = fig.add_subplot(1,1,1)
    ax.set_xlabel(getName(var))
    ax.set_ylabel(vary[1])
    ax.set_title(vary[2] + ' depending on ' + getName(var))
    ax.plot(stats[0],stats[1],'b*-',label=vary[2])
    ax.plot(stats[0],stats[1]-stats[2],'b*--')
    ax.plot(stats[0],stats[1]+stats[2],'b*--')
    ax.plot(stats[0],stats[3],'b*-.',label='95th percentile')
    ax.set_ylim(bottom=0)
    ax.set_xticks(stats[0])
    ax.set_xticklabels(tuple(str(i) for i in [t[1][var] for t in tests]))
    ax.legend()
    fileName = ''
    savePlot(fig, fileName)

def doSingle(var):
    global tests
    tests = sorted(tests,key=lambda t: t[1][var])
    for interest in interestList:
        if any([interest[0] not in t[2] for t in tests]): continue
        doSingleForY(var,interest)
    
    rplist = [t[2]['request_parts'] for t in tests]
    fig = pyplot.figure()
    ax = fig.add_subplot(1,1,1)
    ax.set_xlabel(parameterMap.get(var) or var)
    ax.set_ylabel('duration (ms)')
    ax.set_title('Request Parts depending on ' + getName(var))
    name = 'Request Parts'
    css = [( (rparts['on_client']/2,'r'), (rparts['on_mt_read'],'y'), (rparts['on_mt_inQueue'],'b'), (rparts['on_mt_run']/2,'k'),\
          (rparts['on_db'],'g'), (rparts['on_mt_run']/2,'k'), (rparts['on_mt_outQueue'],'b'), (rparts['on_mt_write'],'y'), (rparts['on_client']/2,'r') ) for rparts in rplist]
    width = 1.0/len(css)
    inds = arange(len(tests))-width/2.
    csz = zip(*css)
    ps = [ax.bar(inds,[csi[0] for csi in cs],width,bottom=[sum([csj[j][0] for csj in csz[0:i]]) for j,csi in zip(count(),cs)],color=cs[0][1]) for i,cs in zip(count(),csz)]
    ax.set_xticks(inds+width/2.)
    ax.set_xticklabels(tuple(str(i) for i in [t[1][var] for t in tests]))
    ax.legend((p[0] for p in ps[:5]),('Between Client and Middleware','Middleware Read/Write','On Middleware in queue','On Middleware run','Database request'))
    savePlot(fig, name)

if __name__ == '__main__':
    print sys.argv
    mode = sys.argv[1]
    if mode not in ['2k','single']:
        raise Exception("put 2k or single as first param")
    var = sys.argv[2:4] if mode == '2k' else sys.argv[2:3]
    directories = sys.argv[2+len(var):]
    if directories[-1] == 'show':
        showStuff = True
        directories = directories[:-1]
    _folder = directories[-1]
    ls = os.listdir(_folder)
    if 'info' in ls or 'params' in ls or 'statistics' in ls or any([fn.endswith('.log') for fn in ls]):
        print "no folder given"
    else:
        directories = directories[:-1]
        folder = _folder
    paramslist = []
    for dr in directories:
        with open(dr+'/params','r') as f:
            paramslist.append(json.load(f))
    statslist = []
    for dr in directories:
        with open(dr+'/statistics','r') as f:
            statslist.append(json.load(f))
    
    tests = zip(directories,paramslist,statslist)
    if mode == '2k':
        do2k(var)
    elif mode == 'single':
        doSingle(var[0])
    if showStuff:
        pyplot.show()
