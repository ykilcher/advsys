from testcase import ClientNode, MessageTierNode
from ch.ethz.grmbl.model import QueueRecordImpl
import time
from random import choice, randint
from java.util import UUID
from ch.ethz.grmbl.data import DataTierFactory
from java.lang import NegativeArraySizeException
from ch.ethz.grmbl.util import Logging
from java.util.logging import Level
from ch.ethz.grmbl.exception import QueueNonExistantException

class QueueCreatingMessageTier(MessageTierNode):
    def __init__(self,port,queueIds,nMessagesInDB=0):
        super(QueueCreatingMessageTier, self).__init__(port)
        self.queueIds = queueIds
        self.nMessagesInDB = nMessagesInDB
    
    def createQueues(self):
        print "creating queues"
        dt = DataTierFactory.getDataTier()
        dt.reset()
        for qid in self.queueIds:
            q = dt.createQueue(QueueRecordImpl(qid))
            print q
        #print dt.getAllQueues()
    
    def runServer(self):
        self.createQueues()
        #print DataTierFactory.getDataTier().getAllQueues()
        MessageTierNode.runServer(self)

class RequestClient(ClientNode):
    def __init__(self,clientId,messageTierAddress,partnerId,queueIds,pollPause=0.1,thinkTime=0,tracing=False):
        super(RequestClient, self).__init__(clientId,messageTierAddress)
        self.partnerId = partnerId
        self.thinkTime = thinkTime
        self.pollPause = pollPause
        self.queueIds = queueIds
        self.queues = []
        self.message = "hello"
        self.tracing = tracing
        self.receivedMessage = None
    
    def getQueues(self):
        self.queues = [self.api.getQueueById(qid) for qid in self.queueIds]
    
    def requesting(self):
        while self.running:
            try:
                if len(self.queues) > 0:
                    q = choice(self.queues)
                    priority = self.selectPriority()
                    message = self.message
                    if self.tracing:
                        if self.receivedMessage is not None:
                            mp = self.receivedMessage.split(' ',2)
                            message = mp[0] + " " + str(int(mp[1])+1) + " " + message
                        else:
                            message = UUID.randomUUID().toString() + " " + str(1) + " " + message
                        message = str(message)
                    q.sendMessage(message,self.partnerId,priority)
                    if self.tracing:
                        self.receivedMessage = None
                    sentTime = time.time()
#                     print "sent"
                    while self.running:
#                         if time.time() - sentTime > 5:
#                             break
                        m = self.api.queryBySender(self.partnerId)
                        if m is not None:
#                             print "m.message ", m.message
#                             print "m.getMessage ", m.getMessage()
#                             print "strmessage ", str(m.message)
#                             print "m.context ", m.context
#                             print "m.getContext ", m.getContext()
#                             print "strcontext ", str(m.context)
                            if self.tracing:
                                mp = m.message.split(' ',2)
                                Logging.trace().log(Level.FINE,"RequestClient message %s %s" % (mp[0],mp[1]))
                                self.receivedMessage = m.message
                            break
                        self.sleep(self.pollPause)
                else:
                    self.getQueues()
            except NegativeArraySizeException:
                pass
            except QueueNonExistantException:
                self.queues = []
            self.sleep(self.thinkTime)
    
    def init(self):
        super(RequestClient, self).init()
        self.schedule(10, self.requesting)

class RespondingClient(ClientNode):
    def __init__(self,clientId,messageTierAddress,partnerId,pollPause=0.1,thinkTime=0,tracing=False):
        super(RespondingClient, self).__init__(clientId,messageTierAddress)
        self.partnerId = partnerId
        self.thinkTime = thinkTime
        self.pollPause = pollPause
        self.message = "hello"
        self.tracing = tracing
    
    def responding(self):
#         print "responding"
        while self.running:
            try:
                m = self.api.queryBySender(self.partnerId)
                if m is not None:
                    message = self.message
                    if self.tracing:
                        mp = m.message.split(' ',2)
                        Logging.trace().log(Level.FINE, "RespondingClient message %s %s" % (mp[0],mp[1]))
                        message = mp[0] + " " + str(int(mp[1]) + 1) + " " + message
                        message = str(message)
                    self.sleep(self.thinkTime)
#                     print "replying"
                    m.reply(message,m.queueId,m.context)
#                     print "responded"
            except NegativeArraySizeException:
                print "NegArray"
                pass
            except QueueNonExistantException:
                print "QNE"
                pass
            self.sleep(self.pollPause)
    
    def init(self):
        super(RespondingClient, self).init()
        self.schedule(10, self.responding)

class OneWayClient(ClientNode):
    def __init__(self,clientId,messageTierAddress,otherIds,queueIds,pollPause=0.1,thinkTime=0,tracing=False):
        super(OneWayClient, self).__init__(clientId,messageTierAddress)
        self.queueIds = queueIds
        self.otherIds = otherIds
        self.thinkTime = thinkTime
        self.pollPause = pollPause
        self.message = "hello"
        self.sentToId = None
        self.sentContext = None
        self.tracing = tracing
        self.sentTime = 0
        self.receivedMessage = None
        self.queues = None

    def getQueues(self):
        self.queues = [self.api.getQueueById(qid) for qid in self.queueIds]
    
    def sendToRandom(self):
        self.sentToId,self.sentContext = choice(self.otherIds),str(randint(1,2**30))
        q = choice(self.queues)
        message = self.message
        if self.tracing:
            if self.receivedMessage is None:
                message = UUID.randomUUID().toString() + " " + str(1) + " " + message
            else:
                mp = self.receivedMessage.split(' ',2)
                message = mp[0] + " " + str(int(mp[1])+1) + " " + message
            message = str(message)
        q.sendMessage(message,self.sentToId,self.sentContext,self.selectPriority())
        self.receivedMessage = None
        self.sentTime = time.time()
    
    def onewaying(self):
        while self.running:
            try:
                if self.queues is not None:
                    if self.sentToId is None:# and time.time() - self.sentTime < 5:
                        self.sendToRandom()
                    qs = list(self.api.queryForQueuesWithSpecificWaitingMessages())
                    if len(qs) > 0:
                        try:
                            m = choice(qs).popSpecific()
                            if m is not None:
                                mp = m.message.split(' ',2)
                                if self.tracing:
                                    Logging.trace().log(Level.FINE,"OneWayClient message %s %s" % (mp[0],mp[1]))
                                if m.senderId == self.sentToId and m.context == self.sentContext:
    #                                 print("received ",m)
                                    self.sleep(self.thinkTime)
                                    self.sentToId = None
                                    self.receivedMessage = m.message
                                else:
                                    message = self.message
                                    if self.tracing:
                                        message = mp[0] + " " + str(int(mp[1])+1) + " " + message
                                        message = str(message)
                                    self.sleep(self.thinkTime)
                                    m.reply(message,m.queueId,m.context)
                                
                        except:
                            pass
                else:
                    self.getQueues()
            except NegativeArraySizeException:
                pass
            except QueueNonExistantException:
                pass

            self.sleep(self.pollPause)
    
    def init(self):
        super(OneWayClient, self).init()
        self.schedule(10, self.onewaying)

class SendingClient(ClientNode):
    def __init__(self,clientId,messageTierAddress,queueIds,thinkTime=0.1):
        super(SendingClient, self).__init__(clientId,messageTierAddress)
        self.thinkTime = thinkTime
        self.queueIds = queueIds
        self.queues = []
    
    def getQueues(self):
        self.queues = [self.api.getQueueById(qid) for qid in self.queueIds]
    
    def sending(self):
        while self.running:
            try:
                if len(self.queues) > 0:
                    q = choice(self.queues)
                    priority = self.selectPriority()
                    message = "hello"
                    q.sendMessage(message,self.partnerId,priority)
    #                 print "sent"
                else:
                    self.getQueues()
            except NegativeArraySizeException: pass
            except QueueNonExistantException:
                self.queues= []
            self.sleep(self.thinkTime)
    
    def init(self):
        super(SendingClient, self).init()
        self.schedule(10, self.sending)
    
class ReceivingClient(ClientNode):
    def __init__(self,clientId,messageTierAddress,pollPause=0.1):
        super(ReceivingClient, self).__init__(clientId,messageTierAddress)
        self.pollPause = pollPause
    
    def receiving(self):
        while self.running:
            try:
                qs = self.api.queryForQueuesWithWaitingMessages();
                if len(qs) > 0:
                    q = choice(qs)
                    m = q.poll()
    #                 if m is not None:
    #                     print 'received'
            except NegativeArraySizeException,QueueNonExistantException:pass
            self.sleep(self.pollPause)
    
    def init(self):
        super(ReceivingClient, self).init()
        self.schedule(10, self.receiving)

class ServiceConsumingClient(ClientNode):
    def __init__(self,clientId,messageTierAddress,requestQueueId,responseQueueId,pollPause=0.1,thinkTime=0,tracing=False):
        super(ServiceConsumingClient, self).__init__(clientId,messageTierAddress)
        self.thinkTime = thinkTime
        self.pollPause = pollPause
        self.requestQueueId = requestQueueId
        self.responseQueueId = responseQueueId
        self.requestQueue = None
        self.responseQueue = None
        self.message = "hello"
        self.tracing = tracing
        self.receivedMessage = None
    
    def getQueues(self):
        self.requestQueue,self.responseQueue = self.api.getQueueById(self.requestQueueId),self.api.getQueueById(self.responseQueueId)
    
    def requesting(self):
        while self.running:
#             print "requesting " + self._id.toString()
            try:
                if self.requestQueue is not None and self.responseQueue is not None:
                    q = self.requestQueue
                    priority = self.selectPriority()
                    message = self.message
                    if self.tracing:
                        if self.receivedMessage is None:
                            message = UUID.randomUUID().toString() + " " + str(1) + " " + message
                        else:
                            mp = self.receivedMessage.split(' ',2)
                            message = mp[0] + " " + str(int(mp[1])+1) + " " + message
                        message = str(message)
#                     print 'sending to request queue ' + q.id.toString() + " " + self._id.toString()
                    q.sendMessage(message,priority)
                    self.receivedMessage = None
                    sentTime =  time.time()
    #                 print "sent"
                    while self.running:
#                         if time.time() - sentTime > 5:
#                             break
                        try:
#                             print 'popping response queue ' + self.responseQueue.id.toString() + " " + self._id.toString()
                            m = self.responseQueue.popSpecific()
                            if m is not None:
                                if self.tracing:
                                    mp = m.message.split(' ',2)
                                    Logging.trace().log(Level.FINE,"ServiceConsumingClient message %s %s" % (mp[0],mp[1]))
                                    self.receivedMessage = m.message
                                break
                        except:
                            pass
                        self.sleep(self.pollPause)
                else:
                    self.getQueues()
            except NegativeArraySizeException: pass
            except QueueNonExistantException:
                self.requestQueue,self.responseQueue = None,None
            self.sleep(self.thinkTime)
    
    def init(self):
        super(ServiceConsumingClient, self).init()
        self.schedule(10, self.requesting)

class ServiceProvidingClient(ClientNode):
    def __init__(self,clientId,messageTierAddress,requestQueueId,responseQueueId,pollPause=0.1,thinkTime=0,tracing=False):
        super(ServiceProvidingClient, self).__init__(clientId,messageTierAddress)
        self.thinkTime = thinkTime
        self.pollPause = pollPause
        self.requestQueueId = requestQueueId
        self.responseQueueId = responseQueueId
        self.requestQueue = None
        self.responseQueue = None
        self.message = "hello"
        self.tracing = tracing
    
    def getQueues(self):
        self.requestQueue,self.responseQueue = self.api.getQueueById(self.requestQueueId),self.api.getQueueById(self.responseQueueId)
    
    def providing(self):
        while self.running:
            try:
                if self.requestQueue is not None and self.responseQueue is not None:
                    try:
                        m = self.requestQueue.pop()
                        if m is not None:
                            message = self.message
                            if self.tracing:
                                mp = m.message.split(' ',2)
                                Logging.trace().log(Level.FINE,"ServiceProvidingClient message %s %s" % (mp[0],mp[1]))
                                message = mp[0] + " " + str(int(mp[1])+1) + " " + message
                                message = str(message)

                            self.sleep(self.thinkTime)
                            m.reply(message,self.responseQueueId,m.context)
                    except:
                        pass
                    self.sleep(self.pollPause)
                else:
                    self.getQueues()
            except NegativeArraySizeException: pass
            except QueueNonExistantException:
                self.requestQueue,self.responseQueue = None,None
    
    def init(self):
        super(ServiceProvidingClient, self).init()
        self.schedule(10, self.providing)

def createRequestRespondPairs(messageTierAddress,queueIds,numPairs,pollPause=0.1,thinkTime=0):
    clients = []
    for _ in range(numPairs):
        id1,id2 = UUID.randomUUID(),UUID.randomUUID()
        req = RequestClient(id1, messageTierAddress, id2, queueIds, pollPause, thinkTime)
        res = RespondingClient(id2, messageTierAddress, id1, pollPause, thinkTime)
        clients.append(req)
        clients.append(res)
    return clients

def createService(messageTierAddress,requestQueueId,responseQueueId,numProviders,numConsumers,pollPause=0.1,thinkTime=0):
    return [ServiceProvidingClient(UUID.randomUUID(),messageTierAddress,requestQueueId,responseQueueId,pollPause,thinkTime) for _ in range(numProviders)] + [ServiceConsumingClient(UUID.randomUUID(),messageTierAddress,requestQueueId,responseQueueId,pollPause,thinkTime) for _ in range(numConsumers)]

def createSenders(messageTierAddress,queueIds,numClients,thinkTime=0.1):
    return [SendingClient(UUID.randomUUID(), messageTierAddress, queueIds, thinkTime) for _ in range(numClients)]

def createReceivers(messageTierAddress,numClients,pollPause=0.1):
    return [ReceivingClient(UUID.randomUUID(), messageTierAddress, pollPause) for _ in range(numClients)]

def createOneWays(messageTierAddress,numOneWays,queueIds,pollPause=0.1,thinkTime=0):
    clientIds = [UUID.randomUUID() for _ in range(numOneWays)]
    return [OneWayClient(cid,messageTierAddress,list(set(clientIds) - set([cid])),queueIds,pollPause,thinkTime) for cid in clientIds]
