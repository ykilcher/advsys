from testcase import CentralTestRunner
from tests import buildBattery
from random import shuffle


if __name__ == '__main__':
    
    dataTierHost = "ec2-54-194-32-20.eu-west-1.compute.amazonaws.com"
    messageTierHosts = ["ec2-54-194-33-153.eu-west-1.compute.amazonaws.com"]

    user = "ec2-user"
    asldir = "/home/" + user + "/tmp/asl"
    libdir = asldir + "/lib"
    localLibDir = "."
    localLogDir = "/usr/tmp/amtrc"
    
    tests = dict(thinkTime=[0.2], message=["hello"], nPairs=[20], \
                 nOneWays=[50], nServices=[2], nThreadsInPool=[16], nDatabaseConnections=[16],duration=[8000],tracing=True)
    
    cases = buildBattery("trace", dataTierHost, messageTierHosts, **tests)
    
    print cases
    
    runner = CentralTestRunner()
    runner.setTestCases(cases)
    runner.dataTierMachine = dict(host=dataTierHost, path=asldir + "/dt", lib=libdir, user=user)
    runner.messageTierMachines = [dict(host=mth, path=asldir + "/mt1", lib=libdir, user=user) for mth in messageTierHosts]
    runner.clientTierMachines = []
    for i in ["ec2-54-194-33-172.eu-west-1.compute.amazonaws.com","ec2-54-194-29-164.eu-west-1.compute.amazonaws.com","ec2-54-194-33-171.eu-west-1.compute.amazonaws.com"]:
        for j in range(1, 3):
            runner.clientTierMachines.append(dict(host="%s" % i, path=asldir + "/ct%d" % j, lib=libdir+"/ct%d"%j, user=user))
    runner.runTests(30, localLibDir, localLogDir,libToAll=True,startDelay=60)
