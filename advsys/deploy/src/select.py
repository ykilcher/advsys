import sqlite3,sys,json,os

if __name__ == '__main__':
    dbfile,query,attrs = sys.argv[1],(sys.argv[2] if len(sys.argv) > 2 else ""),(sys.argv[3] if len(sys.argv) > 3 else "name")
    conn = sqlite3.connect(dbfile)
    c = conn.cursor()
    ls = []
    try:
        c.execute('select %s from exp %s'%(attrs,query))
        res = c.fetchall()
        ls = list("\t".join(str(ri) for ri in r) for r in res)

    finally:
        c.close()
    for l in ls:
        print l

