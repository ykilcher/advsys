import sys,os,json,re,gc
from matplotlib import pyplot,rcParams
from scipy.stats.morestats import bayes_mvs
from numpy.core.numeric import arange, array
from numpy.lib.function_base import percentile
from itertools import count
import numpy
from numpy.ma.extras import unique
from numpy.core.fromnumeric import around,mean
from numpy import isnan

rcParams['legend.loc'] = 'best'

params = None
statistics = dict()
folder = None
showStuff = False

warmup = 260000
cooldown = 5000

def simpleStatistic(vals):
    if not len(vals): return None
    (mn,_,sd) = bayes_mvs(vals,0.95)
    pc = percentile(vals,95)
    return dict(mean=mn,std=sd,pc=pc)

def calculateSums(times,vals,interval=1000):
    trc = around(times/interval)
    utrc = arange(min(trc),max(trc)+1)
    return array([(ue*interval,sum(vals[trc == ue])) for ue in utrc][1:-1]).transpose()

def calculateAvgs(times,vals,interval=1000):
    trc = around(times/interval)
    utrc = arange(min(trc),max(trc)+1)
    return array([(ue*interval,mean(vals[trc == ue])) for ue in utrc][1:-1]).transpose()

def calculateCounts(times,vals,interval=1000):
    trc = around(times/interval)
    utrc = arange(min(trc),max(trc)+1)
    return array([(ue*interval,sum(trc == ue)) for ue in utrc][1:-1]).transpose()

def savePlot(plot,name):
    if folder:
        plot.savefig(folder + '/' + name + ".png")

def showPlots():
    if showStuff:
        pyplot.show()

def createHistogram(stats,label,title,fileName,xlabel=None,ylabel="amount"):
    fig = pyplot.figure()
    ax = fig.add_subplot(1,1,1)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    ax.hist(stats,label=label)
    ax.legend()
    savePlot(fig, fileName)

def createSimplePlot(stats,indices,labels,title,fileName,xlabel=None,ylabel=None,showIntervals=True):
    if xlabel == 'time' or xlabel == 'time (ms)':
        xlabel = 'time (s)'
    times = stats[0]/1000 if xlabel == 'time (s)' else stats[0]
    fig = pyplot.figure()
    ax = fig.add_subplot(1,1,1)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    for i in zip(indices,labels):
        (mn,_),_,(sd,_) = bayes_mvs(stats[i[0]])
        ax.plot(times,stats[i[0]],label=i[1])
        if showIntervals and not isnan(mn):
            mm = [min(times),max(times)]
            ax.plot(mm,[mn,mn],'r-',label=("mean (%s +-[%s,%s])"%(float('%.3g'%mn),float('%.3g'%(mn-sd)),float('%.3g'%(mn+sd)))) if not isnan(mn) else "mean N/A")
            ax.plot(mm,[mn-sd,mn-sd],'r--')
            ax.plot(mm,[mn+sd,mn+sd],'r--')
    ax.set_ylim(bottom=0)
    ax.legend()
    savePlot(fig, fileName)


def getParams(name):
    with open("%s/info"%name) as f:
        lns = list(f)
        params = json.loads(lns[0].replace("'",'"').replace("True","true"))
        startTime = float(lns[2])
        params['startTime'] = round(startTime*1000)
        params['name'] = name
        params['nClients'] = params['nOneWays'] + params['nPairs']*2 + params['nServices']*(params['nConsumersPerService']+params['nProvidersPerService'])
        return params

def goThroughFile(fileName,lineHandler,linePattern=None):
    print "going through " + fileName
    p = linePattern or lineHandler.linePattern
    with open(fileName) as f:
        for line in f:
            if p.search(line):
                lineHandler(line)

def goThroughDirectory(directoryName,lineHandler,filePattern=None,linePattern=None):
    print "going through " + directoryName
    files = os.listdir(directoryName)
    p = filePattern or lineHandler.filePattern
    for fn in files:
        if p.search(fn):
            goThroughFile(directoryName + "/" + fn, lineHandler, linePattern)

def getEventTime(parts):
    return int(parts[2]) - params['startTime'] - warmup

def getEventSignature(parts):
    return (parts[3],parts[4])

def isGoodTime(eventTime):
    return eventTime < params['duration']*1000 - cooldown and eventTime > 0

def makeFileName(*names):
    name = " ".join(filter(None,names))
    return name.replace(" ","_")

def calculateRequestParts(stats):
    on_db = stats['Timing_DataTierInDBImpl']['mean'][0]
    on_mt_run = stats['Timing_PendingTask_run']['mean'][0]
    on_mt_inQueue = stats['Timing_PendingTask_inQueue']['mean'][0]
    on_mt_readSize = stats['Timing_ChannelBuffer_readSizeBuffer']['mean'][0]
    on_mt_readData = stats['Timing_ChannelBuffer_readDataBuffer']['mean'][0]
    on_mt_outQueue = stats['Timing_ChannelBuffer_enqueueOutbuf']['mean'][0]
    on_mt_write = stats['Timing_ChannelBuffer_writeBuffer']['mean'][0]
    on_client = stats['Timing_RemotingClient_doRequest']['mean'][0]
    
    on_mt_read = on_mt_readSize + on_mt_readData
    on_mt = on_mt_read + on_mt_write + on_mt_inQueue + on_mt_run + on_mt_outQueue
    on_client -= on_mt
    on_mt_run -= on_db
    
    rparts=dict(on_db=on_db,on_mt_run=on_mt_run,on_mt_read=on_mt_read,on_client=on_client,on_mt_write=on_mt_write,on_mt=on_mt,on_mt_inQueue=on_mt_inQueue,on_mt_outQueue=on_mt_outQueue)
    
    return rparts

def drawRequestParts(rparts):
    fig = pyplot.figure()
    ax = fig.add_subplot(1,1,1)
    ax.set_ylabel('duration (ms)')
    ax.set_title('Request Parts')
    name = 'Request Parts'
    cs = [ (rparts['on_client']/2,'r'), (rparts['on_mt_read'],'y'), (rparts['on_mt_inQueue'],'b'), (rparts['on_mt_run']/2,'k'),\
          (rparts['on_db'],'g'), (rparts['on_mt_run']/2,'k'), (rparts['on_mt_outQueue'],'b'), (rparts['on_mt_write'],'y'), (rparts['on_client']/2,'r') ]
    ps = [ax.bar(0,c[0],color=c[1],bottom=sum([cs[j][0] for j in range(i)])) for i,c in zip(count(),cs)]
    ax.set_xticks([])
    ax.legend((p[0] for p in ps[:5]),('Between Client and Middleware','Middleware Read','On Middleware in queue','On Middleware run','Database request'))
    savePlot(fig, makeFileName(name))
    

def finalize():
    rparts = calculateRequestParts(statistics)
    statistics['request_parts'] = rparts
    
    drawRequestParts(rparts)
    
    if showStuff:
        print json.dumps(statistics,sort_keys=True,indent=4)
        print json.dumps(params,sort_keys=True,indent=4)
    if folder:
        with open(folder + '/statistics','w') as f:
            json.dump(statistics,f)
        with open(folder + '/params','w') as f:
            json.dump(params,f)

class EventMap(object):
    def __init__(self, flt=None):
        self.d = dict()
        self.filter = flt
    
    def add(self,name,val,dct=None):
        if not isinstance(name,tuple):
            name = (name,)
        if dct is None:
            dct = self.d
            if self.filter is not None and name not in self.filter:
                return
        if not isinstance(dct,dict):
            raise "%s is not a dict" % str(name)
        if len(name) > 1:
            if name[0] not in dct:
                dct[name[0]] = dict()
            self.add(name[1:], val, dct[name[0]])
        else:
            if name[0] not in dct:
                dct[name[0]] = [val]
            else:
                dct[name[0]].append(val)
    
    def _getTimeSeries(self,name,dct):
        if name == ():
            name = (None,)
        if not isinstance(name,tuple):
            name = (name,)
        if not isinstance(dct,dict):
            if isinstance(dct,list):
                return dct
            raise Exception("%s is a strange type" % str(name))
        if name[0] is not None:
            if name[0] not in dct:
                return None
            return self._getTimeSeries(name[1:], dct[name[0]])
        return reduce(lambda x, y:(x or [])+(y or []),[self._getTimeSeries(name[1:], dct[n]) for n in dct.iterkeys()])
    
    def getTimeSeries(self,name=None):
        l = self._getTimeSeries(name, self.d)
        if l: l.sort()
        return l
        
        

class LineHandler(object):
    def __init__(self,linePattern=None,filePattern=None):
        self.linePattern = re.compile(linePattern or '')
        self.filePattern = re.compile(filePattern or '')
    
    def __call__(self,line):
        self.handleLine(line)

    def handleLine(self,line):
        if line.startswith("LOG"):
            parts = line.split()
            eventTime = getEventTime(parts)
            if isGoodTime(eventTime):
                self.handleParts(eventTime,parts)
        else:
            pass

    def handleParts(self,eventTime,parts):
        pass
    
    def finish(self):
        pass

class ErrorLineHandler(LineHandler):
    def __init__(self):
        super(ErrorLineHandler,self).__init__(linePattern=r"grmbl\.error",filePattern=r"\.log$")
    def handleParts(self,eventTime,parts):
        print " ".join(parts)

class StatusLineHandler(LineHandler):
    def __init__(self,filePattern,title,fileName):
        super(StatusLineHandler,self).__init__(linePattern=r"grmbl\.status",filePattern=filePattern)
        self.vmstat = []
        self.title=title
        self.fileName = fileName
    def handleParts(self,eventTime,parts):
        if parts[3] == 'StatCollector':
            if parts[4] == 'vmstat':
                self.vmstat.append((eventTime,) + tuple(int(i) for i in parts[5:]))
    
    def finish(self):
        self.vmstat.sort()
        stats = array(zip(*self.vmstat))
        createSimplePlot(stats, [3,4,5,6], ['swapd','free','buff','cache'],self.title, makeFileName(self.fileName + " " + 'memory'),'time', 'memory in kilobytes',showIntervals=False)
        createSimplePlot(stats, [13,14,15,16], ['user','system','idle','wait'],self.title,makeFileName(self.fileName + " " + 'cpu'), 'time', 'cpu usage percent',showIntervals=False)
        for i,k in zip([3,4,5,6,13,14,15,16], ['swapd','free','buff','cache','user','system','idle','wait']):
            name = makeFileName(self.fileName + " " + k)
            stat = simpleStatistic(stats[i])
            statistics[name] = stat

class DataTierStatusHandler(StatusLineHandler):
    def __init__(self):
        super(DataTierStatusHandler, self).__init__(r"dt\.log$","Data Tier Status","data_tier_status")
        self.messages = []
    def handleParts(self,eventTime,parts):
        super(DataTierStatusHandler, self).handleParts(eventTime,parts)
        if parts[3] == 'DataCollector':
            if parts[4] == 'numClients':
                pass
            elif parts[4] == 'numMessages':
                self.messages.append((getEventTime(parts),int(parts[5])))
            elif parts[4] == 'numQueues':
                pass
    def finish(self):
        super(DataTierStatusHandler, self).finish()
        self.messages.sort()
        stats = array(zip(*self.messages))
        name = "number_of_messages_in_system"
        createSimplePlot(stats, [1], ["number of messages"],"Number of messages in system", name,"time", "number of messages")
        statistics[name] = simpleStatistic(stats[1])

class MessageTierStatusHandler(StatusLineHandler):
    def __init__(self):
        super(MessageTierStatusHandler, self).__init__(r"mt\d+\.log$","Middleware Status","message_tier_status")
        self.tasks = []
    def handleParts(self,eventTime,parts):
        super(MessageTierStatusHandler, self).handleParts(eventTime,parts)
        if parts[3] == 'MessageTierDataCollector':
            if parts[4] == 'pendingTasks':
                self.tasks.append((getEventTime(parts),int(parts[5])))
    def finish(self):
        super(MessageTierStatusHandler, self).finish()
        self.tasks.sort()
        stats = array(zip(*self.tasks))
        name = "number_of_pending_tasks"
        createSimplePlot(stats, [1], ["number of pending tasks"],"Number of pending tasks on middleware",name, "time (s)", "number of pending tasks")
        statistics[name] = simpleStatistic(stats[1])

class ClientTierStatusHandler(StatusLineHandler):
    def __init__(self):
        super(ClientTierStatusHandler, self).__init__(r"ct\d+\.log$","Client Tier Status","client_tier_status")

class TimingLineHandler(LineHandler):
    def __init__(self,filePattern):
        super(TimingLineHandler,self).__init__(linePattern=r"grmbl\.timing",filePattern=filePattern)
        self.events = EventMap()

class ClientTierTimingHandler2(TimingLineHandler):
    def __init__(self):
        super(ClientTierTimingHandler2,self).__init__(filePattern=r"ct\d+\.log$")
        self.requests = dict()
    def handleParts(self,eventTime,parts):
        duration = int(parts[3])
        evname = tuple(parts[4:6])
        if evname == ('RemotingClient','doRequest'):
            rid = parts[6]
            if rid not in self.requests: return
            outcome = parts[8]
            r = self.requests[rid]
            self.events.add(evname + tuple(parts[7:9]),(eventTime,duration))
            self.events.add(r[0] + (outcome,),r[1])
            self.events.add(r[2] + (outcome,),r[3])
            del self.requests[rid]
        elif evname in [('RemotingClient','writeRequest'),('RemotingClient','readResponse')]:
            rid = parts[6]
            if evname[1] == 'writeRequest':
                self.requests[rid] = (evname + (parts[7],),(eventTime,duration,int(parts[8])))
            else:
                if rid not in self.requests: return
                self.requests[rid] += (evname + (parts[7],),(eventTime,duration,int(parts[8])))

    def finish(self):
        methodList = ['queryForClient','queryForClientBySender','read','sendMessageToQueue','getQueueById']
        for mname in methodList:
            for k,v in [(n,self.events.getTimeSeries(n)) for n in [ ('RemotingClient','doRequest') + (mname,)]]:
                if not v: continue
                stats = array(zip(*v))
                name ='Timing ' + " ".join(k)
                fname = makeFileName(name)
                createSimplePlot(stats, [1], [name],name,fname, 'time', 'duration (ms)')
                createHistogram(stats[1], name,name,makeFileName(fname + " hist"), 'duration (ms)')
                statistics[fname] = simpleStatistic(stats[1])
            for k,v in [(n,self.events.getTimeSeries(n)) for n in [('RemotingClient','writeRequest',mname,'fail'),('RemotingClient','readResponse',mname,'fail'),\
                                                                   ('RemotingClient','writeRequest',mname,'success'),('RemotingClient','readResponse',mname,'success')]]:
                if not v: continue
                stats = array(zip(*v))
                name ='Timing ' + " ".join(filter(None,k))
                fname = makeFileName(name)
                createSimplePlot(stats, [1], [name],name,fname, 'time', 'duration (ms)')
                statistics[fname] = simpleStatistic(stats[1])
                name =" ".join(filter(None,k))
                title = "Data on network - " + name
                fname = makeFileName('data on network ' + name)
                createHistogram(stats[2], name,title,makeFileName(fname + " hist"), 'bytes')
                sums = calculateSums(stats[0],stats[2])
                createSimplePlot(sums,[1],[name],title,fname,'time','throughput (bytes per second)')
                statistics[fname] = simpleStatistic(stats[2])
                counts = calculateCounts(stats[0], stats[1])
                createSimplePlot(counts, [1], [name],'Throughput of ' + name,makeFileName(fname + " throughput"), 'time', 'throughput (requests per second)')
                statistics[makeFileName("throughput requests per second",*k)] = simpleStatistic(counts[1])
            for k,v in [(n,self.events.getTimeSeries(n)) for n in [('RemotingClient','writeRequest') + (mname,),('RemotingClient','readResponse') + (mname,)]]:
                if not v: continue
                stats = array(zip(*v))
                name ='Timing ' + " ".join(filter(None,k))
                fname = makeFileName(name)
                createSimplePlot(stats, [1], [name],name,fname, 'time', 'duration (ms)')
                statistics[fname] = simpleStatistic(stats[1])
                name =" ".join(filter(None,k))
                title = "Data on network - " + name
                fname = makeFileName('data on network ' + name)
                createHistogram(stats[2], name,title,makeFileName(fname,"hist"), 'bytes')
                sums = calculateSums(stats[0],stats[2])
                createSimplePlot(sums,[1],[name],'Throughput of ' + name,makeFileName(fname,"throughput"),'time','throughput (bytes per second)')
                statistics[fname] = simpleStatistic(stats[2])
                counts = calculateCounts(stats[0], stats[1])
                createSimplePlot(counts, [1], [name],'Throughput of ' + name,makeFileName(fname + " throughput per second"), 'time', 'throughput (requests per second)')
                statistics[makeFileName("throughput requests per second",*k)] = simpleStatistic(counts[1])

class ClientTierTimingHandler(TimingLineHandler):
    def __init__(self):
        super(ClientTierTimingHandler,self).__init__(filePattern=r"ct\d+\.log$")
        self.requests = dict()
    def handleParts(self,eventTime,parts):
        duration = int(parts[3])
        evname = tuple(parts[4:6])
        if evname == ('RemotingClient','doRequest'):
            rid = parts[6]
            if rid not in self.requests: return
            outcome = parts[8]
            r = self.requests[rid]
            self.events.add(evname + tuple(parts[7:9]),(eventTime,duration))
            self.events.add(r[0] + (outcome,),r[1])
            self.events.add(r[2] + (outcome,),r[3])
            del self.requests[rid]
        elif evname in [('RemotingClient','writeRequest'),('RemotingClient','readResponse')]:
            rid = parts[6]
            if evname[1] == 'writeRequest':
                self.requests[rid] = (evname + (parts[7],),(eventTime,duration,int(parts[8])))
            else:
                if rid not in self.requests: return
                self.requests[rid] += (evname + (parts[7],),(eventTime,duration,int(parts[8])))

    def finish(self):
        for k,v in [(n,self.events.getTimeSeries(n)) for n in [('RemotingClient','doRequest')]]:
            if not v: continue
            stats = array(zip(*v))
            name ='Timing ' + " ".join(k)
            fname = makeFileName(name)
            createSimplePlot(stats, [1], [name],name,fname, 'time', 'duration (ms)')
            statistics[fname] = simpleStatistic(stats[1])
            createHistogram(stats[1], name,name,makeFileName(fname + " hist"), 'duration (ms)')
        for k,v in [(n,self.events.getTimeSeries(n)) for n in [('RemotingClient','writeRequest',None,'fail'),('RemotingClient','readResponse',None,'fail'),\
                                                               ('RemotingClient','writeRequest',None,'success'),('RemotingClient','readResponse',None,'success')]]:
            if not v: continue
            stats = array(zip(*v))
            name ='Timing ' + " ".join(filter(None,k))
            fname = makeFileName(name)
            createSimplePlot(stats, [1], [name],name,fname, 'time', 'duration (ms)')
            statistics[fname] = simpleStatistic(stats[1])
            name =" ".join(filter(None,k))
            title = "Data on network - " + name
            fname = makeFileName('data on network ' + name)
            createHistogram(stats[2], name,title,makeFileName(fname + " hist"), 'bytes')
            sums = calculateSums(stats[0],stats[2])
            createSimplePlot(sums,[1],[name],title,fname,'time','throughput (bytes per second)')
            statistics[fname] = simpleStatistic(stats[2])
            counts = calculateCounts(stats[0], stats[1])
            createSimplePlot(counts, [1], [name],'Throughput of ' + name,makeFileName(fname + " throughput"), 'time', 'throughput (requests per second)')
            statistics[makeFileName("throughput requests per second",*k)] = simpleStatistic(counts[1])
        for k,v in [(n,self.events.getTimeSeries(n)) for n in [('RemotingClient','writeRequest'),('RemotingClient','readResponse')]]:
            if not v: continue
            stats = array(zip(*v))
            name ='Timing ' + " ".join(filter(None,k))
            fname = makeFileName(name)
            createSimplePlot(stats, [1], [name],name,fname, 'time', 'duration (ms)')
            statistics[fname] = simpleStatistic(stats[1])
            name =" ".join(filter(None,k))
            title = "Data on network - " + name
            fname = makeFileName('data on network ' + name)
            createHistogram(stats[2], name,title,makeFileName(fname,"hist"), 'bytes')
            sums = calculateSums(stats[0],stats[2])
            createSimplePlot(sums,[1],[name],'Throughput of ' + name,makeFileName(fname,"throughput"),'time','throughput (bytes per second)')
            statistics[fname] = simpleStatistic(stats[2])
            counts = calculateCounts(stats[0], stats[1])
            createSimplePlot(counts, [1], [name],'Throughput of ' + name,makeFileName(fname + " throughput per second"), 'time', 'throughput (requests per second)')
            statistics[makeFileName("throughput requests per second",*k)] = simpleStatistic(counts[1])

class ChannelBufferTimingHandler(TimingLineHandler):
    def __init__(self):
        super(ChannelBufferTimingHandler,self).__init__(filePattern=r"mt\d+\.log$")
    def handleParts(self,eventTime,parts):
        duration = int(parts[3])
        evname = tuple(parts[4:6])
        if evname in [('ChannelBuffer','readBytes'),('ChannelBuffer','writeBytes'),('ChannelBuffer','writeBuffer'),('ChannelBuffer','readSizeBuffer'),('ChannelBuffer','readDataBuffer')]:
            self.events.add(evname,(eventTime,duration,int(parts[6])))
        elif evname in [('ChannelBuffer','enqueueOutbuf')]:
            self.events.add(evname,(eventTime,duration))

    def finish(self):
        for k,v in [(n,self.events.getTimeSeries(n)) for n in [('ChannelBuffer','enqueueOutbuf')]]:
            if not v: continue
            stats = array(zip(*v))
            name ='Timing ' + " ".join(k)
            fname = makeFileName(name)
            createSimplePlot(stats, [1], [name],name,fname, 'time', 'duration (ms)')
            statistics[fname] = simpleStatistic(stats[1])
        for k,v in [(n,self.events.getTimeSeries(n)) for n in [('ChannelBuffer','writeBuffer'),('ChannelBuffer','readSizeBuffer'),('ChannelBuffer','readDataBuffer')]]:
            if not v: continue
            stats = array(zip(*v))
            name ='Timing ' + " ".join(filter(None,k))
            fname = makeFileName(name)
            createSimplePlot(stats, [1], [name],name,fname, 'time', 'duration (ms)')
            statistics[fname] = simpleStatistic(stats[1])
            name =" ".join(filter(None,k))
            title = "Data on network - " + name
            fname = makeFileName('data on network ' + name)
            createHistogram(stats[2], name,title,makeFileName(fname,"hist"), 'bytes')
            sums = calculateSums(stats[0],stats[2])
            createSimplePlot(sums,[1],[name],'Throughput of ' + name,makeFileName(fname,"throughput"),'time','throughput (bytes per second)')
            statistics[fname] = simpleStatistic(stats[2])

class MessageTierTimingHandler(TimingLineHandler):
    def __init__(self):
        super(MessageTierTimingHandler,self).__init__(filePattern=r"mt\d+\.log$")
    def handleParts(self,eventTime,parts):
        duration = int(parts[3])
        evname = tuple(parts[4:6])
        if evname in [('PendingTask','inQueue'),('PendingTask','run')]:
            self.events.add(evname,(eventTime,duration))
        elif evname[0].startswith('DataTierInDBImpl') and evname[1] != 'parseMessageRecord':
            self.events.add(evname,(eventTime,duration))

    def finish(self):
        for k,v in [(n,self.events.getTimeSeries(n)) for n in [('PendingTask','inQueue'),('PendingTask','run'),('DataTierInDBImpl',)]]:
            if not v: continue
            stats = array(zip(*v))
            name = 'Timing ' + " ".join(k)
            fname = makeFileName(name)
            createSimplePlot(stats, [1], [name],name,fname, 'time', 'duration (ms)')
            counts = calculateCounts(stats[0], stats[1])
            createSimplePlot(counts, [1], [name],'Throughput of ' + name,makeFileName(fname + " throughput"), 'time', 'throughput (requests per second)')
            statistics[fname] = simpleStatistic(stats[1])
            statistics[makeFileName("throughput requests per second",*k)] = simpleStatistic(counts[1])

class TracingLineHandler(LineHandler):
    def __init__(self):
        super(TracingLineHandler,self).__init__(linePattern=r"grmbl\.trace",filePattern=r"ct\d+\.log$")
        self.message = dict(OneWayClient=dict(),RequestClient=dict(),RespondingClient=dict(),\
                            ServiceProvidingClient=dict(),ServiceConsumingClient=dict())
    def handleParts(self,eventTime,parts):
        if parts[4] == 'message':
            dct = self.message[parts[3]]
            msgid = parts[5]
            amt = int(parts[6])
            if msgid not in dct:
                dct[msgid] = []
            dct[msgid].append((eventTime,amt))
    def finish(self):
        title = 'Number of times message exchanged'
        fname = makeFileName(title)
        fig = pyplot.figure()
        ax = fig.add_subplot(1,1,1)
        ticks = []
        for i,(k,v) in zip(count(),self.message.iteritems()):
            if len(v) < 1: continue
            ticks.append(i)
            vallist = [max(val)[1] for val in v.itervalues()]
            ax.boxplot(vallist, positions=[i+1])
            statistics[makeFileName(title + " " + k)] = simpleStatistic(vallist)
        ax.set_xticklabels(['OneWay','Requester','Responder','Provider','Consumer'])
        ax.set_xticks(range(1,len(ticks)+1))
        ax.set_xlim(left=0)
        ax.set_ylim(bottom=0)
        ax.set_title(title)
        savePlot(fig, fname)

if __name__ == '__main__':
    directory = sys.argv[1]
    _folder = sys.argv[2]
    if _folder is not None and _folder != 'show':
        folder = _folder
        if len(sys.argv) > 3 and sys.argv[3] == 'show':
            showStuff = True
    elif _folder == 'show':
        showStuff = True
    params = getParams(directory)
    pyplot.ioff()
    handlers =  [DataTierStatusHandler(),MessageTierStatusHandler(), ClientTierStatusHandler(),\
                ClientTierTimingHandler(),ChannelBufferTimingHandler(),MessageTierTimingHandler() ,TracingLineHandler()]
    handlers.reverse()
    while handlers:
        th = handlers.pop()
        goThroughDirectory(directory, th)
        th.finish()
        del th
        gc.collect()
    finalize()
    showPlots()
