from testcase import CentralTestRunner
from tests import buildBattery
from random import shuffle


if __name__ == '__main__':
    
    dataTierHost = "ec2-54-194-34-48.eu-west-1.compute.amazonaws.com"
    messageTierHosts = ["ec2-54-194-47-128.eu-west-1.compute.amazonaws.com",\
                        ]

    user = "ec2-user"
    asldir = "/home/" + user + "/tmp/asl"
    libdir = asldir + "/lib"
    localLibDir = "."
    localLogDir = "/usr/tmp/ama4"
    
    cases = []
#     tests = dict(duration=[400],tracing=True,thinkTime=[0.05,0.01,0.005,0.001],nPairs=[8], nOneWays=[16],nServices=[1])
#     cases += buildBattery("singlevariance_thinktime", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,message=["hello","hello"*30,"hello"*300],nPairs=[8], nOneWays=[16],nServices=[1])
#     cases += buildBattery("singlevariance_message", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,nThreadsInPool=[1,16],nDatabaseConnections=[1,16],nOneWays=[16],nPairs=[8],nServices=[1])
#     cases += buildBattery("doublevariance_threads_connections", dataTierHost, messageTierHosts,**tests)
    tests = dict(duration=[400],tracing=True,nThreadsInPool=[2,4,6,8],nOneWays=[16],nPairs=[8],nServices=[1])
    cases += buildBattery("singlevariance_threads", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,nDatabaseConnections=[1,10,50,200],nOneWays=[16],nPairs=[8],nServices=[1])
#     cases += buildBattery("singlevariance_connections", dataTierHost, messageTierHosts,**tests)

#     tests = dict(duration=[400],tracing=True,nPairs=[3,6,24,64])
#     cases += buildBattery("singlevariance_pairs", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,thinkTime=[0.005,0.05],nPairs=[3,24])
#     cases += buildBattery("doublevariance_thinktime_pairs", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,nPairs=[3,24],nPairsQueues=[1,16])
#     cases += buildBattery("doublevariance_pairs_pairsqueues", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,nServices=[2],nProvidersPerService=[2,15],nConsumersPerService=[4,30])
#     cases += buildBattery("doublevariance_providers_consumers", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,nOneWays=[6,48],nOneWaysQueues=[1,16])
#     cases += buildBattery("doublevariance_oneways_onewaysqueues", dataTierHost, messageTierHosts,**tests)

#     tests = dict(duration=[400],tracing=True, nOneWays=[50,200,800,3500])
#     cases += buildBattery("singlevariance_oneways", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True, nServices=[5,20,50,200])
#     cases += buildBattery("singlevariance_services", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,mtimpl=['NON_BLOCKING','NAIVE'],nOneWays=[600],nPairs=[400],nServices=[50])
#     cases += buildBattery("singlevariance", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,dtimpl=['JDBC','JDBCv2'],nOneWays=[600],nPairs=[400],nServices=[50])
#     cases += buildBattery("singlevariance", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,nMessagesInDB=[1,1000,50000,1000000],nOneWays=[600],nPairs=[400],nServices=[50])
#     cases += buildBattery("singlevariance", dataTierHost, messageTierHosts,**tests)



#     tests = dict(duration=[400],tracing=True,nPairs=[8],nOneWays=[16],nServices=[1])
#     cases += buildBattery("speedup", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,nPairs=[8],nOneWays=[16],nServices=[1])
#     cases += buildBattery("speedup2", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,nPairs=[8],nOneWays=[16],nServices=[1])
#     cases += buildBattery("speedup3", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,nPairs=[8],nOneWays=[16],nServices=[1])
#     cases += buildBattery("speedup4", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,nPairs=[32],nOneWays=[64],nServices=[4])
#     cases += buildBattery("scaleup4", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,nPairs=[24],nOneWays=[48],nServices=[3])
#     cases += buildBattery("scaleup3", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,nPairs=[16],nOneWays=[32],nServices=[2])
#     cases += buildBattery("scaleup2", dataTierHost, messageTierHosts,**tests)

#     tests = dict(tracing=True,nPairs=[100], nOneWays=[100], nServices=[10],duration=400,mtimpl=['NAIVE'])
#     cases = buildBattery("naive", dataTierHost, messageTierHosts, **tests) + cases

#     tests = dict(tracing=True,nPairs=[5], nOneWays=[10], nServices=[1],duration=400)
#     cases = buildBattery("samemachine", dataTierHost, messageTierHosts, **tests) + cases

    
#     tests = dict(thinkTime=[0.01,0.1], nOneWays=[20,1000])
#     cases += buildBattery("doublevariance", dataTierHost, messageTierHosts,**tests)
#     tests = dict(duration=[400],tracing=True,message=["hello","hello"*200],nPairs=[10,500])
#     cases += buildBattery("doublevariance_msgsize_pairs", dataTierHost, messageTierHosts,**tests)
#     tests = dict(nPairs=[10,500],nOneWays=[20,1000])
#     cases += buildBattery("doublevariance", dataTierHost, messageTierHosts,**tests)

#     tests = dict(thinkTime=[0.01], message=["hello"], nPairs=[20], \
#                   nOneWays=[50], nServices=[2], nThreadsInPool=[8], nDatabaseConnections=[8],duration=[7500],tracing=True)
#     cases = buildBattery("trace", dataTierHost, messageTierHosts, **tests)
    
#     shuffle(cases)

#     tests = dict(tracing=True,nPairs=[8], nOneWays=[16], nServices=[1],duration=600)
#     cases = buildBattery("warmup", dataTierHost, messageTierHosts, **tests) + cases

    print cases
    
    runner = CentralTestRunner()
    runner.setTestCases(cases)
    runner.dataTierMachine = dict(host=dataTierHost, path=asldir + "/dt", lib=libdir, user=user)
    runner.messageTierMachines = [dict(host=mth, path=asldir + "/mt1", lib=libdir, user=user) for mth in messageTierHosts]
    runner.clientTierMachines = []
    for i in ["ec2-54-194-47-129.eu-west-1.compute.amazonaws.com",\
        "ec2-54-194-47-127.eu-west-1.compute.amazonaws.com",\
        "ec2-54-194-42-128.eu-west-1.compute.amazonaws.com",\
        ]:
        for j in range(1, 3):
            runner.clientTierMachines.append(dict(host="%s" % i, path=asldir + "/ct%d" % j, lib=libdir+"/ct%d"%j, user=user))
    runner.runTests(30, localLibDir, localLogDir,libToAll=True,startDelay=30,minilib=True)
