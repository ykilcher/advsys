import sched, time, os
from threading import Thread

def cleanup(cmd):
    os.system(cmd)

if __name__ == '__main__':
    for i in range(1,17):
            Thread(target=cleanup, args=('/usr/bin/ssh -o StrictHostKeyChecking=no %(user)s@%(host)s \'killall java; killall postgres; rm -rf %(dir)s/*\'' % dict(user="user02", host="dryad%02d" % (i,), dir="/home/user02/tmp/asl"),)).start()
