﻿--simple function to register a client with a certain uuid
CREATE OR REPLACE FUNCTION asl2.registerClient(client_id_ UUID) RETURNS VOID AS $$
BEGIN
	INSERT INTO asl2.client VALUES(client_id_);
END;
$$ LANGUAGE plpgsql;


--simple function to unregister a client with a certain uuid
CREATE OR REPLACE FUNCTION asl2.unregisterClient(client_id_ UUID) RETURNS VOID AS $$
BEGIN
	DELETE FROM asl2.client WHERE id = client_id_;
END;
$$ LANGUAGE plpgsql;


--simple function to add a queue
CREATE OR REPLACE FUNCTION asl2.createQueue(queue_id_ UUID) RETURNS VOID AS $$
BEGIN
	INSERT INTO asl2.queue VALUES(queue_id_);
END;
$$ LANGUAGE plpgsql;


--checks if a queue is empty
CREATE OR REPLACE FUNCTION asl2.checkIfQueueEmpty(queue_id_ UUID) RETURNS VOID AS $$
BEGIN
	PERFORM asl2.existsQueue(queue_id_);
	IF (SELECT COUNT(qid) FROM asl2.isIn WHERE qid = queue_id_) > 0 THEN
		RAISE EXCEPTION 'The queue (id: %) is not empty.', queue_id_;
	END IF;
END;
$$ LANGUAGE plpgsql;


--checks if a queue is empty
CREATE OR REPLACE FUNCTION asl2.checkIfQueueNotEmpty(queue_id_ UUID) RETURNS VOID AS $$
BEGIN
	PERFORM asl2.existsQueue(queue_id_);
	IF (SELECT COUNT(qid) FROM asl2.isIn WHERE qid = queue_id_) = 0 THEN
		RAISE EXCEPTION 'The queue (id: %) is empty.', queue_id_;
	END IF;
END;
$$ LANGUAGE plpgsql;



--simple function to remove a queue
CREATE OR REPLACE FUNCTION asl2.deleteQueue(queue_id_ UUID) RETURNS VOID AS $$
DECLARE
	state INTEGER;
BEGIN
	PERFORM asl2.checkIfQueueEmpty(queue_id_);
	DELETE FROM asl2.queue AS q WHERE q.id = queue_id_;
END;
$$LANGUAGE plpgsql;


--simple function to retrieve all the queues
CREATE OR REPLACE FUNCTION asl2.getAllQueues() RETURNS TABLE(id UUID) AS $$
BEGIN
	RETURN QUERY SELECT * FROM asl2.queue;
	RETURN;
END;
$$LANGUAGE plpgsql;


--simple function to retrieve all the queues which the client id_ can read
CREATE OR REPLACE FUNCTION asl2.getQueuesForClient(client_id_ UUID, includeCommon BOOLEAN) RETURNS TABLE(id UUID) AS $$
BEGIN
	IF includeCommon THEN
		RETURN QUERY SELECT qid FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE receiver = client_id_ OR receiver IS NULL;
	ELSE
		RETURN QUERY SELECT qid FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE receiver = client_id_;
	END IF;
	RETURN;
END;
$$LANGUAGE plpgsql;


--simple function to retrieve all the queues which the client id_ can read
CREATE OR REPLACE FUNCTION asl2.getQueuesForClientAndQueues(client_id_ UUID, includeCommon BOOLEAN, queue_ids_ UUID[]) RETURNS TABLE(id UUID) AS $$
BEGIN
	IF includeCommon THEN
		RETURN QUERY SELECT qid FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE queueId = ANY(queue_ids_) AND (receiver = client_id_ OR receiver IS NULL);
	ELSE
		RETURN QUERY SELECT qid FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE queueId = ANY(queue_ids_) AND receiver = client_id_;
	END IF;
	RETURN;
END;
$$LANGUAGE plpgsql;


--simple function to retrieve all the queues which the client client_id_ can read which are from sender sender_id_
CREATE OR REPLACE FUNCTION asl2.getQueuesForClientBySender(client_id_ UUID, sender_id_ UUID) RETURNS TABLE(id UUID) AS $$
BEGIN
	RETURN QUERY SELECT qid FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE sender = sender_id_ AND (receiver = client_id_ OR receiver IS NULL);
	RETURN;
END;
$$LANGUAGE plpgsql;


--simple function to retrieve all readable messages from sender_id_ to client_id_ ordered by orderByPriority
--CREATE OR REPLACE FUNCTION asl2.getQueuesForClient(client_id_ UUID, sender_id_ UUID, orderByPriority BOOLEAN) RETURNS TABLE(id UUID, sender UUID, receiver UUID, context VARCHAR, message VARCHAR, arrivalTime BIGINT, queueId UUID, priority INTEGER) AS $$
--BEGIN
--	IF orderByPrioriy THEN
--		RETURN QUERY SELECT * FROM asl2.message WHERE sender = sender_id AND (receiver = client_id OR receiver IS NULL) ORDER BY priority DESC;
--	ELSE
--		RETURN QUERY SELECT * FROM asl2.message WHERE sender = sender_id AND (receiver = client_id OR receiver IS NULL) ORDER BY arrivalTime ASC;
--	END IF;
--	RETURN;
--END;
--$$LANGUAGE plpgsql;


--simple function to retrieve the first message in queue_id_ for client_id_ ordered by orderByPriority
CREATE OR REPLACE FUNCTION asl2.read(queue_id_ UUID, client_id_ UUID, orderByPriority BOOLEAN, pop BOOLEAN,includeCommon BOOLEAN) RETURNS TABLE(_id UUID, _sender UUID, _receiver UUID, _context VARCHAR, _message VARCHAR, _arrivalTime TIMESTAMP, _queueId UUID, _priority INTEGER) AS $$
BEGIN
	PERFORM asl2.checkIfQueueNotEmpty(queue_id_);
	IF pop THEN
		IF orderByPriority THEN
		IF includeCommon THEN
			RETURN QUERY SELECT id, sender, receiver, context, message, arrivalTime, qid, priority FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE qid = queue_id_ AND (receiver = client_id_  OR receiver IS NULL) ORDER BY priority DESC LIMIT 1;
			DELETE FROM asl2.message WHERE id IN (SELECT id as mid FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE qid = queue_id_ AND (receiver = client_id_  OR receiver IS NULL) ORDER BY priority DESC LIMIT 1);
		ELSE
			RETURN QUERY SELECT id, sender, receiver, context, message, arrivalTime, qid, priority FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE qid = queue_id_ AND (receiver = client_id_ ) ORDER BY priority DESC LIMIT 1;
			DELETE FROM asl2.message WHERE id IN (SELECT id as mid FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE qid = queue_id_ AND (receiver = client_id_ ) ORDER BY priority DESC LIMIT 1);
		END IF;
			ELSE
			IF includeCommon THEN
			RETURN QUERY SELECT id, sender, receiver, context, message, arrivalTime, qid, priority FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE qid = queue_id_ AND (receiver = client_id_  OR receiver IS NULL) ORDER BY arrivalTime ASC LIMIT 1;
			DELETE FROM asl2.message WHERE id IN (SELECT id as mid FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE qid = queue_id_ AND (receiver = client_id_  OR receiver IS NULL) ORDER BY arrivalTime ASC LIMIT 1);
		ELSE
			RETURN QUERY SELECT id, sender, receiver, context, message, arrivalTime, qid, priority FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE qid = queue_id_ AND (receiver = client_id_ ) ORDER BY arrivalTime ASC LIMIT 1;
			DELETE FROM asl2.message WHERE id IN (SELECT id as mid FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE qid = queue_id_ AND (receiver = client_id_ ) ORDER BY arrivalTime ASC LIMIT 1);
		END IF;
			END IF;
	ELSE
		IF orderByPriority THEN
		IF includeCommon THEN
			RETURN QUERY SELECT id, sender, receiver, context, message, arrivalTime, qid, priority FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE qid = queue_id_ AND (receiver = client_id_  OR receiver IS NULL) ORDER BY priority DESC LIMIT 1;
		ELSE
			RETURN QUERY SELECT id, sender, receiver, context, message, arrivalTime, qid, priority FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE qid = queue_id_ AND (receiver = client_id_ ) ORDER BY priority DESC LIMIT 1;
		END IF;
			ELSE
			IF includeCommon THEN
			RETURN QUERY SELECT id, sender, receiver, context, message, arrivalTime, qid, priority FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE qid = queue_id_ AND (receiver = client_id_  OR receiver IS NULL) ORDER BY arrivalTime ASC LIMIT 1;
		ELSE
			RETURN QUERY SELECT id, sender, receiver, context, message, arrivalTime, qid, priority FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE qid = queue_id_ AND (receiver = client_id_ ) ORDER BY arrivalTime ASC LIMIT 1;
		END IF;
			END IF;
	END IF;
	RETURN;
END;
$$LANGUAGE plpgsql;


--simple function to retrieve the first messages from sender_id_ to client_id_ ordered by orderByPriority
CREATE OR REPLACE FUNCTION asl2.getMessageForClientBySender(sender_id_ UUID, client_id_ UUID, orderByPriority BOOLEAN, pop BOOLEAN) RETURNS TABLE(_id UUID, _sender UUID, _receiver UUID, _context VARCHAR, _message VARCHAR, _arrivalTime TIMESTAMP, _queueId UUID, _priority INTEGER) AS $$
BEGIN
	IF pop THEN
		IF orderByPriority THEN
			RETURN QUERY SELECT id, sender, receiver, context, message, arrivalTime, qid, priority FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE sender = sender_id_ AND (receiver = client_id_  OR receiver IS NULL) ORDER BY priority DESC LIMIT 1;
			DELETE FROM asl2.message WHERE id IN (SELECT id as mid FROM asl2.message WHERE sender = sender_id_ AND (receiver = client_id_  OR receiver IS NULL) ORDER BY priority DESC LIMIT 1);
		ELSE
			RETURN QUERY SELECT id, sender, receiver, context, message, arrivalTime, qid, priority FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE sender = sender_id_ AND (receiver = client_id_  OR receiver IS NULL) ORDER BY arrivalTime ASC LIMIT 1;
			DELETE FROM asl2.message WHERE id IN (SELECT id as mid FROM asl2.message WHERE sender = sender_id_ AND (receiver = client_id_  OR receiver IS NULL) ORDER BY arrivalTime ASC LIMIT 1);
		END IF;
	ELSE
		IF orderByPriority THEN
			RETURN QUERY SELECT id, sender, receiver, context, message, arrivalTime, qid, priority FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE sender = sender_id_ AND (receiver = client_id_  OR receiver IS NULL) ORDER BY priority DESC LIMIT 1;
		ELSE
			RETURN QUERY SELECT id, sender, receiver, context, message, arrivalTime, qid, priority FROM asl2.isIn INNER JOIN asl2.message ON (asl2.isIn.mid = asl2.message.id) WHERE sender = sender_id_ AND (receiver = client_id_  OR receiver IS NULL) ORDER BY arrivalTime ASC LIMIT 1;
		END IF;
	END IF;
	RETURN;
END;
$$LANGUAGE plpgsql;


--simple function which checks if a queue exists
CREATE OR REPLACE FUNCTION asl2.existsQueue(queue_id_ UUID) RETURNS VOID AS $$
BEGIN
	IF (SELECT COUNT(id) FROM asl2.queue WHERE id = queue_id_) = 0 THEN
		RAISE EXCEPTION 'The queue (id: %) does not exist.', queue_id_;
	END IF;
END;
$$LANGUAGE plpgsql;


--simple function which puts a message into all given queues
CREATE OR REPLACE FUNCTION asl2.sendMessageToQueues(message_id_ UUID, sender_id_ UUID, receiver_id_ UUID, context_ VARCHAR, message_ VARCHAR, queue_ids_ UUID[], priority_ INTEGER) RETURNS TIMESTAMP AS $$
DECLARE
	queue_id_ UUID;
	arrivalTime_ TIMESTAMP;
	i INTEGER;
BEGIN
	FOREACH queue_id_ IN ARRAY queue_ids_ LOOP
		PERFORM asl2.existsQueue(queue_id_);
	END LOOP;
	arrivalTime_ := 'now';

	INSERT INTO asl2.message VALUES(message_id_, sender_id_, receiver_id_, context_, message_, arrivalTime_, priority_);

	FOREACH queue_id_ IN ARRAY queue_ids_ LOOP
		INSERT INTO asl2.isIn VALUES(message_id_, queue_id_);
	END LOOP;
	RETURN arrivalTime_;
END;
$$LANGUAGE plpgsql;

--simple function to check whether a message exists or not
CREATE OR REPLACE FUNCTION asl2.existsMessage(message_id_ UUID) RETURNS VOID AS $$
BEGIN
	IF (SELECT COUNT(id) FROM asl2.message WHERE id = message_id_) = 0 THEN
		RAISE EXCEPTION 'The message (id: %) does not exist.', message_id_;
	END IF;
END;
$$LANGUAGE plpgsql;


--simple function to delete a message
CREATE OR REPLACE FUNCTION asl2.deleteMessage(message_id_ UUID) RETURNS VOID AS $$
BEGIN
	DELETE FROM asl2.message WHERE id = message_id_;
END;
$$LANGUAGE plpgsql;


--retrieves the id of all clients
CREATE OR REPLACE FUNCTION asl2.getAllClients() RETURNS TABLE(id UUID) AS $$
BEGIN
	RETURN QUERY SELECT * FROM asl2.client;
	RETURN;
END;
$$LANGUAGE plpgsql;


--returns the number of clients in the database
CREATE OR REPLACE FUNCTION asl2.getNumClients() RETURNS INTEGER AS $$
DECLARE 
	num INTEGER;
BEGIN
	SELECT COUNT(id) INTO num FROM asl2.client;
	RETURN num;
END;
$$LANGUAGE plpgsql;


--returns the number of queues in the database
CREATE OR REPLACE FUNCTION asl2.getNumQueues() RETURNS INTEGER AS $$
DECLARE 
	num INTEGER;
BEGIN
	SELECT COUNT(id) INTO num FROM asl2.queue;
	RETURN num;
END;
$$LANGUAGE plpgsql;


--returns the number of messages in the database
CREATE OR REPLACE FUNCTION asl2.getNumMessages() RETURNS INTEGER AS $$
DECLARE 
	num INTEGER;
BEGIN
	SELECT COUNT(id) INTO num FROM asl2.message;
	RETURN num;
END;
$$LANGUAGE plpgsql;


--returns the number of messages in each queue in the database
CREATE OR REPLACE FUNCTION asl2.getNumMessagesInAllQueues() RETURNS TABLE(id_ UUID, num_ BIGINT) AS $$
BEGIN
	RETURN QUERY SELECT q.id, COUNT(i.mid) FROM asl2.isIn as i INNER JOIN asl2.queue as q ON (q.id = i.qid) GROUP BY q.id;
	RETURN;
END;
$$LANGUAGE plpgsql;


--returns the number of messages in the the queue
CREATE OR REPLACE FUNCTION asl2.getNumMessagesInQueue(queue_id_ UUID) RETURNS INTEGER AS $$
DECLARE 
	num INTEGER;
BEGIN
	PERFORM asl2.existsQueue(queue_id_);
	SELECT COUNT(mid) INTO num FROM asl2.isIn WHERE qid = queue_id_;
	RETURN num;
END;
$$LANGUAGE plpgsql;


