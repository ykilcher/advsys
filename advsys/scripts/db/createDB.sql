﻿---- CLEAR ALL EXISTING DATA ----

DROP SCHEMA IF EXISTS asl CASCADE;

---- CREATE SCHEMA AND TABLES ----

CREATE SCHEMA asl;

CREATE TABLE asl.queue(
	id UUID PRIMARY KEY
);

CREATE TABLE asl.client(
	id UUID PRIMARY KEY
);

CREATE TABLE asl.message(
	id UUID PRIMARY KEY,
	sender UUID,
	receiver UUID,
	context VARCHAR,
	message VARCHAR(2000),
	arrivalTime TIMESTAMP,
	queueId UUID NOT NULL,
	priority INTEGER,
	FOREIGN KEY (sender) REFERENCES asl.client(id) ON UPDATE CASCADE,
	-- FOREIGN KEY (receiver) REFERENCES asl.client(id) ON UPDATE CASCADE,
	FOREIGN KEY (queueId) REFERENCES asl.queue(id) ON UPDATE CASCADE,
	CHECK (priority >= 1 AND priority <= 10)
);

---- CREATE INDEXES ----

CREATE INDEX aslMessageSender
	ON asl.message
	USING btree
	(sender);

CREATE INDEX aslMessageReceiver
	ON asl.message
	USING btree
	(receiver);

CREATE INDEX aslMessageSenderReceiver
	ON asl.message
	USING btree
	(sender, receiver);

CREATE INDEX aslMessagePriority
	ON asl.message
	USING btree
	(priority);

CREATE INDEX aslMessageArrivalTime
	ON asl.message
	USING btree
	(arrivalTime);

CREATE INDEX aslMessageQueueId
	ON asl.message
	USING btree
	(queueId);
