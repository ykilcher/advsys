﻿---- CLEAR ALL EXISTING DATA ----

DROP SCHEMA IF EXISTS asl2 CASCADE;

---- CREATE SCHEMA AND TABLES ----

CREATE SCHEMA asl2;

CREATE TABLE asl2.queue(
	id UUID PRIMARY KEY
);

CREATE TABLE asl2.client(
	id UUID PRIMARY KEY
);

CREATE TABLE asl2.message(
	id UUID PRIMARY KEY,
	sender UUID,
	receiver UUID,
	context VARCHAR,
	message VARCHAR(2000),
	arrivalTime TIMESTAMP,
	priority INTEGER,
	FOREIGN KEY (sender) REFERENCES asl2.client(id) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (receiver) REFERENCES asl2.client(id) ON UPDATE CASCADE ON DELETE CASCADE,
	CHECK (priority >= 1 AND priority <= 10)
);

CREATE TABLE asl2.isIn(
	mid UUID NOT NULL,
	qid UUID NOT NULL,
	FOREIGN KEY (qid) REFERENCES asl2.queue(id) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (mid) REFERENCES asl2.message(id) ON UPDATE CASCADE ON DELETE CASCADE
);

---- CREATE INDEXES ----

CREATE INDEX asl2MessageSender
	ON asl2.message
	USING btree
	(sender);

CREATE INDEX asl2MessageReceiver
	ON asl2.message
	USING btree
	(receiver);

CREATE INDEX asl2MessagePriority
	ON asl2.message
	USING btree
	(priority);

CREATE INDEX asl2MessageSenderReceiver
	ON asl2.message
	USING btree
	(sender, receiver);

CREATE INDEX asl2MessageArrivalTime
	ON asl2.message
	USING btree
	(arrivalTime);

CREATE INDEX asl2isInQueueID
	ON asl2.isIn
	USING btree
	(qid);

CREATE INDEX asl2isInMessageID
	ON asl2.isIn
	USING btree
	(mid);
