psql -d postgres -a -f ~/git/advsys/advsys/scripts/db/createDB.sql
psql -d postgres -a -f ~/git/advsys/advsys/scripts/db/createFunctions.sql
psql -d postgres -a -f ~/git/advsys/advsys/scripts/db/createDBv2.sql
psql -d postgres -a -f ~/git/advsys/advsys/scripts/db/createFunctionsv2.sql
