#!/bin/bash

mkdir ~/postgres/
cd ~/postgres/
~/pg/postgresql-9.3.1/configure --prefix=/home/user02/postgres/pgsql
#--with-libraries=<directories> for external libraries which aren't installed at the standard locations, multiple directories are separated by ':'
make world
make check
make install-world

PATH=~/postgres/pgsql/bin:$PATH
export PATH
MANPATH=~/postgres/pgsql/man:$MANPATH
export MANPATH

mkdir ~/postgres/pgsql/data
initdb -D ~/postgres/pgsql/data

echo "host all all 0.0.0.0/0 trust" >> ~/postgres/pgsql/data/pg_hba.conf
echo "listen_addresses = '*'" >> ~/postgres/pgsql/data/postgresql.conf

postgres -D ~/postgres/pgsql/data >logfile 2>&1 &
#createdb postgres	#is not needed because it creates a database postgres automatically
#psql postgres		#starts up the a psql console which is connected to the db postgres
