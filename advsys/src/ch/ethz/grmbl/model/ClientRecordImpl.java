package ch.ethz.grmbl.model;

import java.util.Calendar;
import java.util.UUID;

public class ClientRecordImpl implements ClientRecord {
	
	private static final long serialVersionUID = -2388279494497471895L;
	private UUID id;
	private UUID messageTierId;
	private boolean sender = true;
	private boolean reciever = true;
	private long version = Calendar.getInstance().getTimeInMillis();

	public ClientRecordImpl(UUID id) {
		super();
		this.id = id;
	}

	@Override
	public UUID getId() {
		return this.id;
	}
	
	@Override
	public boolean isSender() {
		return this.sender;
	}
	@Override
	public boolean isReciever() {
		return this.reciever;
	}

	@Override
	public UUID getMessageTierId() {
		return messageTierId;
	}
	
	public void setMessageTierId(UUID messageTierId) {
		this.messageTierId = messageTierId;
	}
	@Override
	public long getVersion() {
		return version;
	}
	
	public long updateVersion(){
		this.version = Calendar.getInstance().getTimeInMillis();
		return this.version;
	}

	@Override
	public boolean equals(Object c) {
		return c instanceof ClientRecordImpl && ((ClientRecordImpl)c).getId().equals(id);
	}
	

}
