package ch.ethz.grmbl.model;

import java.io.Serializable;
import java.util.UUID;

public interface Record extends Serializable,Cacheable {
	UUID getId();
}
