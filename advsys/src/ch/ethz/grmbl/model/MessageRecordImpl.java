package ch.ethz.grmbl.model;

import java.util.Calendar;
import java.util.UUID;

public class MessageRecordImpl implements MessageRecord {

	private static final long serialVersionUID = -2151706152161594405L;
	private UUID id;
	private UUID senderId;
	private UUID receiverId;
	private UUID queueId;
	private String context;
	private Integer priority;
	private Long arrivalTime;
	private String message;
	private long version = Calendar.getInstance().getTimeInMillis();
	
	

	public MessageRecordImpl(UUID id) {
		super();
		this.id = id;
	}

	@Override
	public UUID getId() {
		return this.id;
	}

	@Override
	public UUID getSenderId() {
		return senderId;
	}

	@Override
	public UUID getReceiverId() {
		return receiverId;
	}

	@Override
	public UUID getQueueId() {
		return queueId;
	}

	@Override
	public String getContext() {
		return context;
	}

	@Override
	public Integer getPriority() {
		return this.priority;
	}

	@Override
	public Long getArrivalTime() {
		return this.arrivalTime;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

	@Override
	public boolean isOneWay() {
		return !this.isTwoWay();
	}

	@Override
	public boolean isTwoWay() {
		return this.receiverId != null && this.context != null;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public void setSenderId(UUID senderId) {
		this.senderId = senderId;
	}

	public void setReceiverId(UUID receiverId) {
		this.receiverId = receiverId;
	}

	public void setQueueId(UUID queueId) {
		this.queueId = queueId;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public void setArrivalTime(Long arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public long getVersion() {
		return version;
	}
	
	public long updateVersion(){
		this.version = Calendar.getInstance().getTimeInMillis();
		return this.version;
	}



}
