package ch.ethz.grmbl.model;

import java.util.UUID;

/**A message.
 * @author yk
 *
 */
public interface MessageRecord extends Record{
	UUID getSenderId();
	UUID getReceiverId();
	UUID getQueueId();
	String getContext();
	Integer getPriority();
	Long getArrivalTime();
	String getMessage();
	
	boolean isOneWay();
	boolean isTwoWay();
}
