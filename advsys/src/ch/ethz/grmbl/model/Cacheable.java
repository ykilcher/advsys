package ch.ethz.grmbl.model;

public interface Cacheable {
	long getVersion();
}
