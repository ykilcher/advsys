package ch.ethz.grmbl.model;

import java.util.UUID;

/**Represents a client record in the database.
 * @author yk
 *
 */
public interface ClientRecord extends Record{
	UUID getMessageTierId();
	boolean isSender();
	boolean isReciever();
	boolean equals(Object c);
}
