package ch.ethz.grmbl.model;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class QueueRecordImpl implements QueueRecord {

	private static final long serialVersionUID = -3735579621821864990L;

	private UUID id;
	private long version = Calendar.getInstance().getTimeInMillis();

	private Set<MessageRecordImpl> messages = Collections
			.<MessageRecordImpl> synchronizedSet(new HashSet<MessageRecordImpl>());

	public QueueRecordImpl(UUID id) {
		this.id = id;
	}

	@Override
	public UUID getId() {
		return this.id;
	}

	public Set<MessageRecordImpl> getMessages() {
		return messages;
	}
	
	@Override
	public long getVersion() {
		return version;
	}
	
	public long updateVersion(){
		this.version = Calendar.getInstance().getTimeInMillis();
		return this.version;
	}

}
