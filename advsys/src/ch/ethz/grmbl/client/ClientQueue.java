package ch.ethz.grmbl.client;

import java.util.UUID;

import ch.ethz.grmbl.exception.QueueDeletionException;
import ch.ethz.grmbl.exception.QueueEmptyException;
import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.model.QueueReadOrder;

public interface ClientQueue {
	UUID getId();
	void delete() throws QueueDeletionException;
	ClientMessage peek() throws QueueEmptyException, QueueNonExistantException;
	ClientMessage pop() throws QueueEmptyException, QueueNonExistantException;
	ClientMessage peek(QueueReadOrder order) throws QueueEmptyException, QueueNonExistantException;
	ClientMessage pop(QueueReadOrder order) throws QueueEmptyException, QueueNonExistantException;
	ClientMessage peekSpecific() throws QueueEmptyException, QueueNonExistantException;
	ClientMessage popSpecific() throws QueueEmptyException, QueueNonExistantException;
	ClientMessage peekSpecific(QueueReadOrder order) throws QueueEmptyException, QueueNonExistantException;
	ClientMessage popSpecific(QueueReadOrder order) throws QueueEmptyException, QueueNonExistantException;
	ClientMessage sendMessage(String message) throws QueueNonExistantException;
	ClientMessage sendMessage(String message,int priority) throws QueueNonExistantException;
	ClientMessage sendMessage(String message,UUID receiverId,int priority) throws QueueNonExistantException;
	ClientMessage sendMessage(String message, UUID receiverId, String context,int priority) throws QueueNonExistantException;
	
	boolean isEmpty() throws QueueNonExistantException;
	int getSize() throws QueueNonExistantException;
}
