package ch.ethz.grmbl.client;

import java.util.UUID;

import ch.ethz.grmbl.exception.QueueDeletionException;
import ch.ethz.grmbl.exception.QueueEmptyException;
import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.model.MessageRecord;
import ch.ethz.grmbl.model.QueueReadOrder;
import ch.ethz.grmbl.model.QueueRecord;

public interface ClientQueueManager {

	MessageRecord readQueue(QueueRecord queue, QueueReadOrder order,boolean pop, boolean includeCommon)
			throws QueueEmptyException, QueueNonExistantException;

	MessageRecord sendMessageToQueue(QueueRecord queue, String message,
			UUID receiverId, String context, Integer priority) throws QueueNonExistantException;

	Boolean checkIfQueueEmpty(QueueRecord queue)
			throws QueueNonExistantException;

	void deleteQueue(QueueRecord queue) throws QueueDeletionException;

	int getQueueSize(QueueRecord record) throws QueueNonExistantException;
}
