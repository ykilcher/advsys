package ch.ethz.grmbl.client;

import java.util.Collection;
import java.util.Set;
import java.util.UUID;

import ch.ethz.grmbl.exception.QueueCreationException;
import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.model.QueueReadOrder;

public interface ClientAPI {
	UUID getClientId();

	void unregister();

	ClientQueue createQueue(UUID id) throws QueueCreationException;

	ClientMessage queryBySender(UUID senderId);
	ClientMessage queryBySender(UUID senderId, QueueReadOrder order
			);
	ClientMessage queryBySender(UUID senderId, QueueReadOrder order,
			Boolean pop);

	Set<ClientQueue> queryForQueuesWithWaitingMessages();
	Set<ClientQueue> queryForQueuesWithSpecificWaitingMessages();
	Set<ClientQueue> queryForQueuesWithWaitingMessagesFromQueues(Collection<UUID> queueIds);
	Set<ClientQueue> queryForQueuesWithSpecificWaitingMessagesFromQueues(Collection<UUID> queueIds);

	Set<ClientQueue> getAllQueues();

	ClientQueue getQueueById(UUID queueId) throws QueueNonExistantException;

	Collection<ClientMessage> sendMessageToMultipleQueues(String message,
			Collection<ClientQueue> queues, int priority) throws QueueNonExistantException;
	
	Set<UUID> getAllOtherClients();
}
