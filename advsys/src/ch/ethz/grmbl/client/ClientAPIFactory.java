package ch.ethz.grmbl.client;

import java.util.UUID;

import ch.ethz.grmbl.msg.MessageTier;
import ch.ethz.grmbl.msg.MessageTierFactory;

public class ClientAPIFactory {
	public static ClientAPI createAPI(UUID clientId, String messageTierAddress){
		MessageTier messageTier = MessageTierFactory.getMessageTier(messageTierAddress);
		messageTier.registerClient(clientId, "asdf");
		return new ClientAPIImpl(clientId,messageTier);
	}
}
