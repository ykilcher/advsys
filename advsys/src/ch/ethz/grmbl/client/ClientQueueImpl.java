package ch.ethz.grmbl.client;

import java.util.UUID;

import ch.ethz.grmbl.exception.QueueDeletionException;
import ch.ethz.grmbl.exception.QueueEmptyException;
import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.model.MessageRecord;
import ch.ethz.grmbl.model.QueueReadOrder;
import ch.ethz.grmbl.model.QueueRecord;

public class ClientQueueImpl implements ClientQueue {

	private QueueRecord record;
	private ClientQueueManager queueManager;
	private ClientMessageManager messageManager;

	public ClientQueueImpl(ClientQueueManager queueManager, ClientMessageManager messageManager, QueueRecord record) {
		this.queueManager = queueManager;
		this.messageManager = messageManager;
		this.record = record;
	}

	@Override
	public UUID getId() {
		return record.getId();
	}

	@Override
	public ClientMessage peek() throws QueueEmptyException, QueueNonExistantException {
		return this.peek(null);
	}

	@Override
	public ClientMessage pop() throws QueueEmptyException, QueueNonExistantException {
		return this.pop(null);
	}

	@Override
	public ClientMessage peek(QueueReadOrder order) throws QueueEmptyException, QueueNonExistantException {
		MessageRecord mr = queueManager.readQueue(record, order,false,true);
		return new ClientMessageImpl(messageManager, mr);
	}

	@Override
	public ClientMessage pop(QueueReadOrder order) throws QueueEmptyException, QueueNonExistantException {
		MessageRecord mr = queueManager.readQueue(record, order,true,true);
		return new ClientMessageImpl(messageManager, mr);
	}

	@Override
	public ClientMessage sendMessage(String message) throws QueueNonExistantException {
		return this.sendMessage(message, null, null,1);
	}

	@Override
	public ClientMessage sendMessage(String message,int priority) throws QueueNonExistantException {
		return this.sendMessage(message, null, null,priority);
	}

	@Override
	public ClientMessage sendMessage(String message,UUID receiverId, int priority) throws QueueNonExistantException {
		return this.sendMessage(message, receiverId, null,priority);
	}

	@Override
	public ClientMessage sendMessage(String message, UUID receiverId,
			String context,int priority) throws QueueNonExistantException {
		MessageRecord mr = queueManager.sendMessageToQueue(record, message, receiverId,
				context,priority);
		return new ClientMessageImpl(messageManager, mr);
	}

	@Override
	public boolean isEmpty() throws QueueNonExistantException {
		return queueManager.checkIfQueueEmpty(record);
	}
	
	@Override
	public int getSize() throws QueueNonExistantException {
		return queueManager.getQueueSize(record);
	}

	@Override
	public void delete() throws QueueDeletionException {
		queueManager.deleteQueue(record);
	}

	@Override
	public ClientMessage peekSpecific() throws QueueEmptyException,
			QueueNonExistantException {
		return this.peekSpecific(null);
	}

	@Override
	public ClientMessage popSpecific() throws QueueEmptyException,
			QueueNonExistantException {
		return this.popSpecific(null);
	}

	@Override
	public ClientMessage peekSpecific(QueueReadOrder order)
			throws QueueEmptyException, QueueNonExistantException {
		MessageRecord mr = queueManager.readQueue(record, order,false,false);
		return new ClientMessageImpl(messageManager, mr);
	}

	@Override
	public ClientMessage popSpecific(QueueReadOrder order)
			throws QueueEmptyException, QueueNonExistantException {
		MessageRecord mr = queueManager.readQueue(record, order,true,false);
		return new ClientMessageImpl(messageManager, mr);
	}

}
