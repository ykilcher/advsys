package ch.ethz.grmbl.client;

import java.util.UUID;

import ch.ethz.grmbl.exception.QueueNonExistantException;

/**A message as viewed by the client.
 * @author yk
 *
 */
public interface ClientMessage{
	UUID getSenderId();
	UUID getReceiverId();
	UUID getQueueId();
	String getContext();
	Integer getPriority();
	Long getArrivalTime();
	String getMessage();
	
	boolean isOneWay();
	boolean isTwoWay();
	
	ClientMessage reply(String message, UUID queueId, String context) throws QueueNonExistantException;
}
