package ch.ethz.grmbl.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;

import ch.ethz.grmbl.exception.QueueCreationException;
import ch.ethz.grmbl.exception.QueueDeletionException;
import ch.ethz.grmbl.exception.QueueEmptyException;
import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.model.ClientRecord;
import ch.ethz.grmbl.model.MessageRecord;
import ch.ethz.grmbl.model.QueueReadOrder;
import ch.ethz.grmbl.model.QueueRecord;
import ch.ethz.grmbl.msg.MessageTier;
import ch.ethz.grmbl.util.Logging;
import ch.ethz.grmbl.util.Logging.TimingLogger;

public class ClientAPIImpl implements ClientAPI, ClientMessageManager,
		ClientQueueManager {

	private final String LOG_PREFIX;

	private UUID clientId;
	private MessageTier messageTier;

	public ClientAPIImpl(UUID clientId, MessageTier messageTier) {
		this.clientId = clientId;
		this.messageTier = messageTier;
		LOG_PREFIX = "ClientAPI";
	}

	@Override
	public MessageRecord readQueue(QueueRecord queue, QueueReadOrder order, boolean pop, boolean includeCommon)
			throws QueueEmptyException, QueueNonExistantException {
		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " readQueue");
		MessageRecord mr = messageTier.read(queue.getId(), clientId, order,
				pop,includeCommon);
		log.end();
		return mr;
	}

	@Override
	public MessageRecord sendMessageToQueue(QueueRecord queue, String message,
			UUID receiverId, String context, Integer priority)
			throws QueueNonExistantException {
		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " sendMessageToQueue");
		MessageRecord mr;
		try {
			mr = messageTier.sendMessageToQueue(queue.getId(),
					message, clientId, receiverId, context, priority);
		} catch (QueueNonExistantException e) {
			log.end("fail");
			throw e;
		}
		log.end("success");
		return mr;
	}

	@Override
	public Boolean checkIfQueueEmpty(QueueRecord queue)
			throws QueueNonExistantException {

		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " checkIfQueueEmpty");
		boolean b = messageTier.checkIfQueueEmpty(queue.getId());
		log.end(Boolean.toString(b));
		return b;
	}

	@Override
	public int getQueueSize(QueueRecord queue) throws QueueNonExistantException {

		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getQueueSize");
		int s = messageTier.getQueueSize(queue.getId());
		log.end(Integer.toString(s));
		return s;
	}

	@Override
	public void deleteQueue(QueueRecord queue) throws QueueDeletionException {
		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX + " deleteQueue");
		messageTier.deleteQueue(queue.getId());
		log.end();
	}

	@Override
	public MessageRecord replyToMessage(MessageRecord original, String message,
			UUID queueId, String context) throws QueueNonExistantException {
		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " replyToMessage");
		MessageRecord r;
		try {
			r = messageTier.sendMessageToQueue(queueId, message,
					clientId, original.getSenderId(), context,
					original.getPriority());
		} catch (QueueNonExistantException e) {
			log.end("fail");
			throw e;
		}
		log.end("success");
		return r;
	}

	@Override
	public UUID getClientId() {
		return this.clientId;
	}

	@Override
	public void unregister() {

		messageTier.unregisterClient(clientId);
	}
	
	@Override
	public ClientQueue createQueue(UUID id) throws QueueCreationException {

		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " createQueue");
		QueueRecord qr = messageTier.createQueue(id);
		ClientQueue cq = createClientQueueFromRecord(qr);
		log.end();
		return cq;
	}

	@Override
	public ClientMessage queryBySender(UUID senderId) {
		return this.queryBySender(senderId, QueueReadOrder.PRIORITY);
	}

	@Override
	public ClientMessage queryBySender(UUID senderId, QueueReadOrder order) {
		return this.queryBySender(senderId, order, true);
	}

	@Override
	public ClientMessage queryBySender(UUID senderId, QueueReadOrder order,
			Boolean pop) {

		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " queryBySender");
		MessageRecord mr = messageTier.queryForClientBySender(clientId,
				senderId, order, pop);
		if (mr != null) {
			log.end("success");
			return new ClientMessageImpl(this, mr);
		} else {
			log.end("fail");
			return null;
		}
	}
	
	private Set<ClientQueue> queryForQueues(Collection<UUID> queueIds, boolean includeCommon){
		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " queryForQueues");
		Set<ClientQueue> cqs = new HashSet<ClientQueue>();
		Collection<QueueRecord> qrs = messageTier.queryForClient(clientId,queueIds,includeCommon);
		for (QueueRecord qr : qrs) {
			ClientQueueImpl cq = createClientQueueFromRecord(qr);
			cqs.add(cq);
		}
		log.end(Integer.toString(cqs.size()));
		return cqs;
		
	}

	@Override
	public Set<ClientQueue> queryForQueuesWithWaitingMessages() {
		return this.queryForQueues(null, true);
	}

	@Override
	public Set<ClientQueue> queryForQueuesWithSpecificWaitingMessages() {
		return this.queryForQueues(null, false);
	}

	@Override
	public Set<ClientQueue> queryForQueuesWithWaitingMessagesFromQueues(
			Collection<UUID> queueIds) {
		return this.queryForQueues(queueIds, true);
	}

	@Override
	public Set<ClientQueue> queryForQueuesWithSpecificWaitingMessagesFromQueues(
			Collection<UUID> queueIds) {
		return this.queryForQueues(queueIds, false);
	}

	@Override
	public Set<ClientQueue> getAllQueues() {
		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getAllQueues");
		Set<ClientQueue> cqs = new HashSet<ClientQueue>();
		Collection<QueueRecord> qrs = messageTier.getAllQueues();
		for (QueueRecord qr : qrs) {
			ClientQueueImpl cq = createClientQueueFromRecord(qr);
			cqs.add(cq);
		}
		log.end(Integer.toString(cqs.size()));
		return cqs;
	}

	@Override
	public ClientQueue getQueueById(UUID queueId)
			throws QueueNonExistantException {

		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getQueueById");
		QueueRecord qr = messageTier.getQueueById(queueId);
		ClientQueue cq = createClientQueueFromRecord(qr);
		log.end();
		return cq;
	}

	private ClientQueueImpl createClientQueueFromRecord(QueueRecord qr) {
		return new ClientQueueImpl(this, this, qr);
	}

	@Override
	public Collection<ClientMessage> sendMessageToMultipleQueues(
			String message, Collection<ClientQueue> queues, int priority)
			throws QueueNonExistantException {
		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " sendMessageToMultipleQueues");
		Collection<UUID> qids = new ArrayList<UUID>();
		for (ClientQueue cq : queues) {
			qids.add(cq.getId());
		}
		Set<ClientMessage> cms = new HashSet<ClientMessage>();
		try{
		Collection<MessageRecord> msgs = messageTier
				.sendMessageToMultipleQueues(qids, message, clientId, priority);
		for (MessageRecord mr : msgs) {
			cms.add(new ClientMessageImpl(this, mr));
		}
		}catch(QueueNonExistantException e){
		log.end(Integer.toString(qids.size()) + " fail");
			throw e;
		}
		log.end(Integer.toString(qids.size()) + " success");
		return cms;
	}

	@Override
	public Set<UUID> getAllOtherClients() {

		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getAllOtherClients");
		Set<UUID> ss = new HashSet<UUID>();
		Collection<ClientRecord> clients = messageTier.getAllClients();
		for (ClientRecord c : clients) {
			UUID id = c.getId();
			if (!id.equals(clientId)) {
				ss.add(id);
			}
		}
		log.end(Integer.toString(clients.size()));
		return ss;
	}


}
