package ch.ethz.grmbl.client;

import java.util.UUID;

import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.model.MessageRecord;

public class ClientMessageImpl implements ClientMessage {

	private MessageRecord record;

	private ClientMessageManager messageManager;

	public ClientMessageImpl(ClientMessageManager messageManager,
			MessageRecord record) {
		if(record == null){
			throw new RuntimeException("Record cannot be null");
		}
		this.messageManager = messageManager;
		this.record = record;
	}

	@Override
	public Long getArrivalTime() {
		return record.getArrivalTime();
	}

	@Override
	public String getMessage() {
		return record.getMessage();
	}

	@Override
	public Integer getPriority() {
		return record.getPriority();
	}

	@Override
	public UUID getQueueId() {
		return record.getQueueId();
	}

	@Override
	public UUID getReceiverId() {
		return record.getReceiverId();
	}

	@Override
	public String getContext() {
		return record.getContext();
	}

	@Override
	public UUID getSenderId() {
		return record.getSenderId();
	}

	@Override
	public boolean isOneWay() {
		return !this.isTwoWay();
	}

	@Override
	public boolean isTwoWay() {
		return record.isTwoWay();
	}

	@Override
	public ClientMessage reply(String message, UUID queueId, String context) throws QueueNonExistantException {
		MessageRecord reply = messageManager.replyToMessage(this.record,
				message, queueId, context);
		return new ClientMessageImpl(messageManager, reply);
	}

	public MessageRecord getRecord() {
		return record;
	}

}
