package ch.ethz.grmbl.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import ch.ethz.grmbl.exception.QueueNonExistantException;

public class JavaOneWayClient {
	private ClientAPI api;
	private UUID clientId;
	private String messageTierAddress;
	private List<UUID> queueIds;
	private List<UUID> otherIds;
	private double thinkTime;
	private double pollPause;
	private String message;
	private UUID sentToId = null;
	private String sentContext = null;
	public boolean running = true;

	public JavaOneWayClient(UUID clientId, String messageTierAddress,
			List<UUID> queueIds, List<UUID> otherIds, double thinkTime,
			double pollPause, String message) {
		this.clientId = clientId;
		this.messageTierAddress = messageTierAddress;
		this.queueIds = queueIds;
		this.otherIds = otherIds;
		this.thinkTime = thinkTime;
		this.pollPause = pollPause;
		this.message = message;
	}

	public void init() {
		this.api = ClientAPIFactory.createAPI(this.clientId,
				this.messageTierAddress);
	}

	private void sendToRandom() throws QueueNonExistantException {
		Random r = new Random();
		this.sentToId = this.otherIds.get(r.nextInt(this.otherIds.size()));
		this.sentContext = Integer.toString(r.nextInt(2 << 30));
		ClientQueue q = this.api.getQueueById(this.queueIds.get(r
				.nextInt(this.queueIds.size())));
		q.sendMessage(this.message, this.sentToId, this.sentContext,
				r.nextInt(9) + 1);
	}

	public void onewaying() throws QueueNonExistantException, InterruptedException{
		while(this.running){
			if(this.sentToId != null){
				 ClientMessage m = this.api.queryBySender(this.sentToId);
						if(m != null && this.sentContext.equals(m.getContext())){
		                    System.out.println("received " + m.getMessage());
		                    Thread.sleep((long)(this.thinkTime*1000));
		                    this.sendToRandom();
						}
			}else{
				this.sendToRandom();
			}
			List<ClientQueue> qs = new ArrayList<ClientQueue>(this.api.queryForQueuesWithWaitingMessages());
			if(!qs.isEmpty()){
                try{
                	Random r = new Random();
                	ClientMessage m = qs.get(r.nextInt(qs.size())).pop();
                	if(m != null){
                		if(this.sentToId.equals(m.getSenderId()) && this.sentContext.equals(m.getContext())){
                            System.out.println("received "+m.getMessage());
                            Thread.sleep((long)(this.thinkTime*1000));
                            this.sendToRandom();
                		}else{
                			String message = this.message;
                            Thread.sleep((long)(this.thinkTime));
                            m.reply(message,m.getQueueId(),m.getContext());
                            System.out.println("responded");
                		}
                	}
                }catch(Exception e){
                	//no problemo
                }
			}

                Thread.sleep((long)(this.pollPause));
		}
	}
}
