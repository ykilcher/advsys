package ch.ethz.grmbl.client;

import java.util.UUID;

import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.model.MessageRecord;

public interface ClientMessageManager {

	MessageRecord replyToMessage(MessageRecord original, String message,
			UUID queueId, String context) throws QueueNonExistantException;

}
