package ch.ethz.grmbl.monitoring;

import java.util.logging.Level;

import ch.ethz.grmbl.exception.DBConnectionException;
import ch.ethz.grmbl.util.Config;
import ch.ethz.grmbl.util.Logging;

public class DataMonitorThread extends Thread
{
	DataCollector c = null;
	
	public DataMonitorThread(boolean withGUI) throws DBConnectionException
	{
		super();
		Config config = Config.get();
		if(withGUI)
		{
			DataMonitor m = new DataMonitor(config.dataMonitorWidth, config.dataMonitorHeight, config.dataCollectorUpdateTime, config.dataCollectorNumDataPoints, withGUI);
			c = m.getDataCollector();
		}
		else
			c = new DataCollector(config.dataCollectorUpdateTime, config.dataCollectorNumDataPoints, null);
	}

	@Override
	public void run() {
		long time = System.currentTimeMillis();
		while(true)
		{
			long newTime = System.currentTimeMillis();
			double dt = (newTime-time)/1000.0;
			time = newTime;
			c.update(dt);
			try {
				Thread.sleep((int)(Config.get().dataCollectorUpdateTime*100));
			} catch (InterruptedException e) {
				Logging.error().log(Level.FINE, "InterruptionException in the monitoring thread.", e);
			}
		}
	}
}