package ch.ethz.grmbl.monitoring;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.UUID;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ch.ethz.grmbl.exception.DBConnectionException;
import ch.ethz.grmbl.util.Config;

public class DataMonitor extends JFrame implements ListSelectionListener
{
	private static final long serialVersionUID = 6846167724305628412L;
	
	private DataCollector data;
	
	private Chart clients, messages, queues;
	private SingleQueueChart messagesInQueue;
	private JScrollPane scrollPaneQueueIds;
	private JList<UUID> queueIds;
	
	DataMonitor(int width, int height, double dataUpdateTimer, int numDataPoints, boolean withGUI) throws DBConnectionException
	{
		super("Data monitor");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		data = new DataCollector(dataUpdateTimer, numDataPoints, this);
		
		GridLayout gl = new GridLayout(0, 2);
		gl.setHgap(5);
		gl.setVgap(5);
		setLayout(gl);
		
		clients = new Chart(data.getNumClients(), "#Clients");
		clients.setPreferredSize(new Dimension(width/2, height/2));
		add(clients);
		
		messages = new Chart(data.getNumMessages(), "#Messages");
		messages.setPreferredSize(new Dimension(width/2, height/2));
		add(messages);
		
		queues = new Chart(data.getNumQueues(), "#Queues");
		queues.setPreferredSize(new Dimension(width/2, height/2));
		add(queues);
		
		messagesInQueue = new SingleQueueChart("<id>", "#MessagesInQueue", this);
		messagesInQueue.setPreferredSize(new Dimension(width/2, height/2));
		add(messagesInQueue);
		
		add(new JLabel(""));
		
		queueIds = new JList<UUID>(new Vector<UUID>());
		queueIds.setVisible(true);
		queueIds.setDragEnabled(false);
		queueIds.setFocusable(false);
		queueIds.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		queueIds.getSelectionModel().addListSelectionListener(this);

		scrollPaneQueueIds = new JScrollPane(queueIds, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPaneQueueIds.setPreferredSize(new Dimension(width/2, height/2));
		add(scrollPaneQueueIds);
		
		pack();
		setVisible(true);
		
		validate();
	}

	DataCollector getDataCollector() {
		return data;
	}
	
	void setQueueId(UUID id)
	{
		data.setQueueId(id);
	}
	
	void updateGraphs()
	{
		clients.update(data.getNumClients());
		messages.update(data.getNumMessages());
		queues.update(data.getNumQueues());
		messagesInQueue.update(data.getNumMessagesInQueue());
		queueIds.setListData(data.getQueueIds());
		
		clients.repaint();
		messages.repaint();
		queues.repaint();
		messagesInQueue.repaint();
		queueIds.repaint();
	}
	
	public static void start(boolean withGUI) throws DBConnectionException, InterruptedException
	{
		Config.get().dataCollectSingleQueues = withGUI;
		Thread t = new DataMonitorThread(withGUI);
		t.start();
	}
	
	public static void main(String[] args) throws DBConnectionException, InterruptedException
	{
		start(true);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) 
	{
		UUID id = queueIds.getSelectedValue();
		if(id != null)
			messagesInQueue.setInput(id.toString());
	}
}