package ch.ethz.grmbl.monitoring;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.Vector;
import java.util.logging.Level;

import ch.ethz.grmbl.data.DataTier;
import ch.ethz.grmbl.data.DataTierFactory;
import ch.ethz.grmbl.exception.DBConnectionException;
import ch.ethz.grmbl.model.QueueRecord;
import ch.ethz.grmbl.util.Config;
import ch.ethz.grmbl.util.Logging;

public class DataCollector
{
	private double updateTime;
	private double dt;
	private DataMonitor anchor;
	private DataTier dataTier;
	
	private LinkedList<Integer> numMessages = new LinkedList<Integer>();
	private LinkedList<Integer> numQueues = new LinkedList<Integer>();
	private LinkedList<Integer> numClients = new LinkedList<Integer>();
	private HashMap<UUID, LinkedList<Integer>> numMessagesInQueue = new HashMap<UUID, LinkedList<Integer>>();
	private Vector<UUID> queueIds = new Vector<UUID>();
	private UUID queueId = null;
	
	public DataCollector(double updateTime, int numPoints, DataMonitor anchor) throws DBConnectionException
	{
		this.anchor = anchor;
		this.updateTime = updateTime;
		
		Config config = Config.get();
		
		//TODO: make it better - not threadsafe!
		int maxConnections = config.dbMaxConnections;
		config.dbMaxConnections = 1;
		this.dataTier = DataTierFactory.getDataTier();
		config.dbMaxConnections = maxConnections;
		
		dt = updateTime;
		for(int i = 0; i < numPoints; ++i)
		{
			numClients.add(0);
			numQueues.add(0);
			numMessages.add(0);
		}
	}


	/**
	 * 
	 */
	public synchronized void update(double dt_)
	{
		dt -= dt_;
		if(dt < 0)
		{
			dt = (dt%updateTime)+updateTime;
			
			numClients.removeFirst();
			int nclients = dataTier.getNumClients();
			numClients.addLast(nclients);
			Logging.status().log(Level.FINE,"DataCollector numClients "+nclients);
			
			numMessages.removeFirst();
			int nmsgs = dataTier.getNumMessages();
			numMessages.addLast(nmsgs);
			Logging.status().log(Level.FINE,"DataCollector numMessages "+nmsgs);

			if(Config.get().dataCollectSingleQueues){
			HashSet<UUID> ids = new HashSet<UUID>(numMessagesInQueue.keySet());
			if(Logging.status().isLoggable(Level.FINEST))
			{
				Map<UUID,Integer> results = dataTier.getNumMessagesInAllQueues();
				for(Entry<UUID, Integer> e: results.entrySet())
				{
					LinkedList<Integer> l = numMessagesInQueue.get(e.getKey());
					if(l != null)
					{
						l.removeFirst();
						l.addLast(e.getValue());
						ids.remove(e.getKey());
					}
					else
					{
						l = new LinkedList<Integer>();
						for(int i = 0; i < numMessages.size() - 1; ++i){
							l.addLast(0);
						}
						l.addLast(e.getValue());
						numMessagesInQueue.put(e.getKey(), l);
					}
					Logging.status().log(Level.FINEST, "DataCollector numMessagesInQueue "+e.getValue()+" "+e.getKey());
				}
				for(UUID id: ids){
					numMessagesInQueue.remove(id);
				}
				
				queueIds = new Vector<UUID>(numMessagesInQueue.keySet());
			}
			else
			{
				if(queueId != null)
				{
					try
					{
						int i = dataTier.getNumMessagesInQueue(queueId);
						LinkedList<Integer> l = numMessagesInQueue.get(queueId);
						l.removeFirst();
						l.addLast(i);
					}
					catch(Exception e) {
						//e.printStackTrace();
					}
				}

				Collection<QueueRecord> c = dataTier.getAllQueues();
				queueIds.clear();
				for(QueueRecord qr : c)
				{
					queueIds.add(qr.getId());
				}
			}

			Collections.sort(queueIds);
			}
			
			numQueues.removeFirst();
			int nqueues = dataTier.getNumQueues();
			numQueues.addLast(nqueues);
			Logging.status().log(Level.FINE,"DataCollector numQueues "+nqueues);
			
			if(anchor != null)
				anchor.updateGraphs();
		}
	}
	
	public LinkedList<Integer> getNumClients()
	{
		return numClients;
	}
	
	public LinkedList<Integer> getNumQueues()
	{
		return numQueues;
	}
	
	public LinkedList<Integer> getNumMessages()
	{
		return numMessages;
	}
	
	public LinkedList<Integer> getNumMessagesInQueue()
	{
		if(queueId != null)
			return numMessagesInQueue.get(queueId);
		else
			return null;
	}
	
	public Vector<UUID> getQueueIds()
	{
		return queueIds;
	}
	
	public void setQueueId(UUID id)
	{
		if(!Logging.status().isLoggable(Level.FINEST))
		{
			numMessagesInQueue.clear();
			if(id != null)
			{
				LinkedList<Integer> dataPoints = new LinkedList<Integer>();
				for(int i = 0; i < numMessages.size(); ++i)
					dataPoints.add(0);
				numMessagesInQueue.put(id, dataPoints);
			}
		}
		queueId = id;
	}
}
