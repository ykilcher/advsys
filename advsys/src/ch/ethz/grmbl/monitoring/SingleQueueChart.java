package ch.ethz.grmbl.monitoring;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.LinkedList;
import java.util.UUID;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class SingleQueueChart extends JPanel implements DocumentListener, ComponentListener
{
	private static final long serialVersionUID = 5953259562129030016L;
	private Chart chart;
	private JTextField input;
	private DataMonitor monitor;
	
	public SingleQueueChart(String defaultTextInInputLine, String chartDescription, DataMonitor dataMonitor) {
		this.monitor = dataMonitor;
		input = new JTextField(defaultTextInInputLine);
		input.getDocument().addDocumentListener(this);
		input.setVisible(true);
		add(input, BorderLayout.NORTH);
		
		chart = new Chart(null, chartDescription);
		add(chart, BorderLayout.CENTER);
		
		setVisible(true);
		
		addComponentListener(this);
	}
	
	public void setPreferredSize(Dimension dim)
	{
		super.setPreferredSize(dim);
		input.setPreferredSize(new Dimension(dim.width, 24));
		chart.setPreferredSize(new Dimension(dim.width, dim.height-32));
	}
	
	/*public void repaint()
	{
		super.repaint();
		if(input != null)
			input.repaint();
		if(chart != null)
			chart.repaint();
	}*/

	public void update(LinkedList<Integer> numMessagesInQueue) 
	{
		chart.update(numMessagesInQueue);
	}
	
	private void updatedInput()
	{
		String i = input.getText();
		try
		{
			UUID queueId = UUID.fromString(i);
			monitor.setQueueId(queueId);
		}
		catch(IllegalArgumentException e) {}
	}

	@Override
	public void changedUpdate(DocumentEvent arg0) {
		updatedInput();
	}

	@Override
	public void insertUpdate(DocumentEvent arg0) {
		updatedInput();
	}

	@Override
	public void removeUpdate(DocumentEvent arg0) {
		updatedInput();
	}

	@Override
	public void componentHidden(ComponentEvent arg0) {}

	@Override
	public void componentMoved(ComponentEvent arg0) {}

	@Override
	public void componentResized(ComponentEvent arg0) {
		input.setPreferredSize(new Dimension(getWidth(), 24));
		chart.setPreferredSize(new Dimension(getWidth(), getHeight()-32));
	}

	@Override
	public void componentShown(ComponentEvent arg0) {}

	public void setInput(String inputText) 
	{
		input.setText(inputText);
	}
}
