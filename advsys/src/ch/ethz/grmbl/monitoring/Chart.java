package ch.ethz.grmbl.monitoring;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;

import javax.swing.JPanel;

public class Chart extends JPanel 
{
	private static final long serialVersionUID = 4898768315816558540L;
	
	private LinkedList<Integer> dataPoints = new LinkedList<Integer>();
	private String label;
	
	public Chart(LinkedList<Integer> dataPoints, String label)
	{
		this.label = label;
		if(dataPoints != null && dataPoints.size() > 0)
			update(dataPoints);
		else
		{
			this.dataPoints.add(0);
		}
		setVisible(true);
	}
	
	public void update(LinkedList<Integer> dataPoints)
	{
		if(dataPoints == null || dataPoints.size() == 0)
			return;
		synchronized (this.dataPoints) 
		{
			this.dataPoints.clear();
			this.dataPoints.addAll(dataPoints);
		}
	}	
	
	protected void paintComponent(Graphics g)
	{
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getWidth(), getHeight());

		g.setColor(Color.BLACK);
		synchronized (dataPoints) 
		{
			int max = 1;
			int height = getHeight()-5;
			for(Integer i : dataPoints)
				if(i > max)
					max = i;
			
			g.drawString(label + ": " + dataPoints.getLast() + " (" + max + ")", 5, getHeight()-20);
			
			int num = dataPoints.size();
			int dx = getWidth()/num;
			int x = dx/-2;
			
			
			/*for(Integer i : dataPoints) //block chart
			{
				g.drawLine(x, getHeight()-i*height/max, x, getHeight());
				x += dx;
			}*/
			
			Integer i = dataPoints.getFirst();
			for(Integer j : dataPoints) //line chart
			{
				g.drawLine(x, getHeight()-i*height/max, x+dx, getHeight()-j*height/max);
				x += dx;
				i = j;
			}
		}
	}
}
