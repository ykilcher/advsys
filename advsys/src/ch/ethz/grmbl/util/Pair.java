package ch.ethz.grmbl.util;

import java.io.Serializable;

public class Pair<L,R> implements Serializable {
	private static final long serialVersionUID = 5648251355353682192L;
	
	public static <L,R> Pair<L,R> of(L left,R right){
		return new Pair<L,R>(left, right);
	}
	
	private final L left;
	private final R right;
	
	public Pair(L left, R right) {
		this.left = left;
		this.right = right;
	}
	
	public L getLeft() {
		return left;
	}
	public R getRight() {
		return right;
	}

}
