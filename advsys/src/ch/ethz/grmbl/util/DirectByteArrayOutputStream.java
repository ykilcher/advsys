package ch.ethz.grmbl.util;

import java.io.ByteArrayOutputStream;

public class DirectByteArrayOutputStream extends ByteArrayOutputStream {
	public DirectByteArrayOutputStream() {
	}

	public DirectByteArrayOutputStream(int size) {
		super(size);
	}
	
	public byte[] getBuf() {
		return this.buf;
	}

	public int getCount() {
		return this.count;
	}
	
	public void setCount(int count){
		this.count = count;
	}

}
