package ch.ethz.grmbl.util;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class Logging {

	private static final Logger timingLogger = Logger.getLogger("grmbl.timing");
	private static final Logger statusLogger = Logger.getLogger("grmbl.status");
	private static final Logger errorLogger = Logger.getLogger("grmbl.error");
	private static final Logger traceLogger = Logger.getLogger("grmbl.trace");
	private static final Random random = new Random();
	
	public static class Formatter extends java.util.logging.Formatter{
		@Override
		public String format(LogRecord record) {
			StringBuffer sb= new StringBuffer();
			sb.append("LOG ");
			sb.append(record.getLoggerName());
			sb.append(' ');
			sb.append(record.getMillis());
			sb.append(' ');
			sb.append(record.getMessage().replace('\n', '\t'));
			if(record.getThrown() != null){
				sb.append(' ');
				sb.append(record.getThrown().toString());
				if(record.getThrown().getMessage() != null){
				sb.append(' ');
				sb.append(record.getThrown().getMessage().replace('\n', '\t'));
			}
				sb.append(' ');
				sb.append(printStackTrace(record.getThrown()));
				if(record.getThrown().getCause() != null){
					sb.append(' ');
					sb.append(record.getThrown().getCause().toString());
					if(record.getThrown().getCause().getMessage() != null){
					sb.append(' ');
					sb.append(record.getThrown().getCause().getMessage().replace('\n', '\t'));
				}
					sb.append(' ');
					sb.append(printStackTrace(record.getThrown().getCause()));
				}
			}
			sb.append('\n');
			return sb.toString();
		}
		
	}
	
	public static StringBuffer printStackTrace(Throwable thrown) {
		if(thrown == null){
			return new StringBuffer();
		}
		StackTraceElement[] el = thrown.getStackTrace();
		StringBuffer sb = new StringBuffer();
		for(StackTraceElement e : el){
			sb.append(e.getClassName());
			sb.append(':');
			sb.append(e.getMethodName());
			sb.append(':');
			sb.append(e.getLineNumber());
			sb.append(' ');
		}
		return sb;
	}

	public static class TimingLogger {
		private String event;
		private Level level;
		private int id;
		private long start;

		public TimingLogger(String event, Level level) {
			this.event = event;
			this.level = level;
			this.id = Logging.random.nextInt(Integer.MAX_VALUE);
			start = System.currentTimeMillis();
//			if (timingLogger.isLoggable(level)) {
//				Logging.timingLogger.log(level, "timing event " + id
//						+ " thread " + Thread.currentThread().getId()
//						+ " start " + event);
//			}
		}

		public void end(String info) {
			if (timingLogger.isLoggable(level)) {
//				Logging.timingLogger.log(level, "timing event " + id
//						+ " thread " + Thread.currentThread().getId() + " end "
//						+ event + info);
				long end = System.currentTimeMillis();
				StringBuffer sb = new StringBuffer();
				sb.append(end-start);
				sb.append(' ');
				sb.append(event);
				if(info != null){
				sb.append(' ');
				sb.append(info);
				}
				Logging.timingLogger.log(level,sb.toString());
			}
		}
		
		public void end(){
			this.end(null);
		}
	}

	public static Logger error() {
		return errorLogger;
	}

	public static Logger status() {
		return statusLogger;
	}
	
	public static Logger trace(){
		return traceLogger;
	}

	public static TimingLogger timingStart(Level level, String event) {
		return new TimingLogger(event, level);
	}
}
