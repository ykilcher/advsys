package ch.ethz.grmbl.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

public class Config {

	private static Config cfg;

	public static Config get() {
		if (cfg == null) {
			cfg = new Config();
		}
		return cfg;
	}

	public static Config set(Config config) {
		Config c = cfg;
		cfg = config;
		return c;
	}

	public static enum DataTierImplementation {
		IN_MEMORY, JDBC, JDBCv2
	}

	public static enum MessageTierRemotingImplementation {
		NON_BLOCKING, NAIVE
	}

	public static Config loadFromResources(String... resourceNames) {
		List<Properties> props = new ArrayList<Properties>();
		for (String name : resourceNames) {
			Properties p = new Properties();
			try {
				p.load(new FileInputStream(name));
			} catch (IOException ex) {
				ex.printStackTrace();
				throw new RuntimeException(ex);
			}
			props.add(p);
		}
		return Config.loadFromProperties(props
				.toArray(new Properties[resourceNames.length]));
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Config loadFromProperties(Properties... properties) {
		Config cfg = new Config();
		for (Properties props : properties) {

			Enumeration<String> names = (Enumeration<String>) props
					.propertyNames();
			while (names.hasMoreElements()) {
				String e = names.nextElement();
				String p = props.getProperty(e);
				try {
					Field f = cfg.getClass().getDeclaredField(e);
					f.setAccessible(true);
					if (f.getType().equals(String.class)) {
						f.set(cfg, p);
					} else if (f.getType().equals(Boolean.class)) {
						f.set(cfg, Boolean.valueOf(p));
					} else if (f.getType().isEnum()) {
						f.set(cfg,
								Enum.valueOf(
										(Class<? extends Enum>) f.getType(), p));
					} else if (f.getType().equals(int.class)) {
						f.set(cfg, Integer.valueOf(p).intValue());
					} else if (f.getType().equals(double.class)) {
						f.set(cfg, Double.valueOf(p).doubleValue());
					}
				} catch (SecurityException ex) {
					System.err.println("ERROR SETTING PROPERTY " + e);
					ex.printStackTrace();
					throw new RuntimeException(ex);
				} catch (IllegalArgumentException ex) {
					System.err.println("ERROR SETTING PROPERTY " + e);
					ex.printStackTrace();
					throw new RuntimeException(ex);
				} catch (IllegalAccessException ex) {
					System.err.println("ERROR SETTING PROPERTY " + e);
					ex.printStackTrace();
					throw new RuntimeException(ex);
				} catch (NoSuchFieldException ex) {
					System.err.println("ERROR SETTING PROPERTY " + e);
					ex.printStackTrace();
					throw new RuntimeException(ex);
				}
			}
		}
		Config.cfg = cfg;
		return get();
	}

	public DataTierImplementation dataTierImplementation = DataTierImplementation.JDBC;
	public String dbServerName = "localhost";
	public int dbPort = 5477;
	public String dbUsername = "user02";
	public String dbPassword = null;
	public String dbName = "postgres";
	public int dbMaxConnections = 10;

	public Boolean dataTierCaching = false;

	public Boolean remoteMessageTier = true;
	public MessageTierRemotingImplementation messageTierRemotingImplementation = MessageTierRemotingImplementation.NON_BLOCKING;
	public String messageTierAddress = "";
	public int nThreadsInPool = 10;

	public double dataCollectorUpdateTime = 1;
	public int dataCollectorNumDataPoints = 100;
	public int dataMonitorWidth = 800;
	public int dataMonitorHeight = 600;
	public boolean dataCollectSingleQueues = false;

}
