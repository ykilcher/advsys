package ch.ethz.grmbl.msg;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.UUID;
import java.util.logging.Level;

import ch.ethz.grmbl.data.DataTier;
import ch.ethz.grmbl.exception.QueueCreationException;
import ch.ethz.grmbl.exception.QueueDeletionException;
import ch.ethz.grmbl.exception.QueueEmptyException;
import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.model.ClientRecord;
import ch.ethz.grmbl.model.ClientRecordImpl;
import ch.ethz.grmbl.model.MessageRecord;
import ch.ethz.grmbl.model.MessageRecordImpl;
import ch.ethz.grmbl.model.QueueReadOrder;
import ch.ethz.grmbl.model.QueueRecord;
import ch.ethz.grmbl.model.QueueRecordImpl;
import ch.ethz.grmbl.util.Logging;
import ch.ethz.grmbl.util.Logging.TimingLogger;

public class MessageTierImpl implements MessageTier {

	private static final String LOG_PREFIX = "MessageTier";
	private DataTier dataTier;
	private UUID id;

	public MessageTierImpl(UUID id, DataTier dataTier) {
		this.id = id;
		this.dataTier = dataTier;
	}

	@Override
	public UUID getId() {
		return id;
	}

	@Override
	public ClientRecord registerClient(UUID clientId, String address) {
		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " registerClient");
		ClientRecord cr = new ClientRecordImpl(clientId);
		cr = dataTier.registerClient(cr);
		log.end();
		return cr;
	}

	@Override
	public void unregisterClient(UUID clientId) {
		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " unregisterClient");
		dataTier.unregisterClient(clientId);
		log.end();
	}

	@Override
	public QueueRecord createQueue(UUID id) throws QueueCreationException {

		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " createQueue");
		QueueRecord q = new QueueRecordImpl(id);
		q = dataTier.createQueue(q);
		log.end();
		return q;
	}

	@Override
	public Collection<QueueRecord> queryForClient(UUID clientId,Collection<UUID> queueIds, Boolean includeCommon) {
		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " queryForClient");
		Collection<QueueRecord> qrs = dataTier.getQueuesForClient(clientId,queueIds,includeCommon);
		log.end(Integer.toString(qrs.size()));
		return qrs;
	}

	@Override
	public MessageRecord queryForClientBySender(UUID clientId, UUID senderId,
			QueueReadOrder order, Boolean pop) {

		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " queryForClientBySender");
		MessageRecord mr = dataTier.getMessageForClientBySender(clientId,
				senderId, order, pop);
		log.end(mr == null? "fail":"success");
		return mr;
	}

	@Override
	public MessageRecord read(UUID queueId, UUID clientId,
			QueueReadOrder order, Boolean pop,Boolean includeCommon) throws QueueEmptyException,
			QueueNonExistantException {
		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " read");
		MessageRecord mr;
		try {
			mr = dataTier.read(queueId, clientId, order, pop,includeCommon);
		} catch (QueueEmptyException e) {
			log.end("fail empty");
			throw e;
		}catch(QueueNonExistantException e){
			log.end("fail nonexistant");
			throw e;
		}
		log.end(mr==null?"fail":"success");
		return mr;
	}

	@Override
	public MessageRecord sendMessageToQueue(UUID queueId, String message,
			UUID senderId, UUID receiverId, String context, Integer priority)
			throws QueueNonExistantException {

		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " sendMessageToQueue");
		MessageRecordImpl msg = createMessageRecord(null, message, senderId,
				receiverId, context, priority);
		UUID[] qids = new UUID[] { queueId };
		Collection<MessageRecord> ms = dataTier.sendMessageToQueues(msg,
				Arrays.asList(qids));
		MessageRecord mr = ms.iterator().next();
		log.end();
		return mr;
	}

	private MessageRecordImpl createMessageRecord(UUID queueId, String message,
			UUID senderId, UUID receiverId, String context, Integer priority) {

		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " createMessageRecord");
		MessageRecordImpl msg = new MessageRecordImpl(UUID.randomUUID());
		msg.setQueueId(queueId);
		msg.setMessage(message);
		msg.setSenderId(senderId);
		msg.setReceiverId(receiverId);
		msg.setContext(context);
		msg.setArrivalTime(Calendar.getInstance().getTimeInMillis());
		msg.setPriority(priority);

		log.end();
		return msg;
	}

	@Override
	public Boolean checkIfQueueEmpty(UUID queueId)
			throws QueueNonExistantException {

		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " checkIfQueueEmpty");
		boolean b = dataTier.checkIfQueueEmpty(queueId);
		log.end(Boolean.toString(b));
		return b;
	}

	@Override
	public Integer getQueueSize(UUID id) throws QueueNonExistantException {
		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getQueueSize");
		int s = dataTier.getNumMessagesInQueue(id);
		log.end(Integer.toString(s));
		return s;
	}

	@Override
	public void deleteQueue(UUID id) throws QueueDeletionException {
		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " deleteQueue");
		dataTier.deleteQueue(id);
		log.end();
	}

	@Override
	public QueueRecord getQueueById(UUID id) throws QueueNonExistantException {
		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getQueueById");
		QueueRecord qr = dataTier.getQueueById(id);
		log.end();
		return qr;
	}

	@Override
	public Collection<QueueRecord> getAllQueues() {
		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getAllQueues");
		Collection<QueueRecord> qs = dataTier.getAllQueues();
		log.end(Integer.toString(qs.size()));
		return qs;
	}

	@Override
	public Collection<MessageRecord> sendMessageToMultipleQueues(
			Collection<UUID> qids, String message, UUID senderId,
			Integer priority) throws QueueNonExistantException {
		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " sendMessageToMultipleQueues");
		MessageRecordImpl mr = this.createMessageRecord(null, message,
				senderId, null, null, priority);
		Collection<MessageRecord> msgs = dataTier.sendMessageToQueues(mr, qids);
		log.end(Integer.toString(qids.size()));
		return msgs;
	}

	@Override
	public Collection<ClientRecord> getAllClients() {
		TimingLogger log = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getAllClients");
		Collection<ClientRecord> qr = dataTier.getAllClients();
		log.end(Integer.toString(qr.size()));
		return qr;
	}

}
