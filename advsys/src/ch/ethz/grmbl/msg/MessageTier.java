package ch.ethz.grmbl.msg;

import java.util.Collection;
import java.util.UUID;

import ch.ethz.grmbl.exception.QueueCreationException;
import ch.ethz.grmbl.exception.QueueDeletionException;
import ch.ethz.grmbl.exception.QueueEmptyException;
import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.model.ClientRecord;
import ch.ethz.grmbl.model.MessageRecord;
import ch.ethz.grmbl.model.QueueReadOrder;
import ch.ethz.grmbl.model.QueueRecord;

public interface MessageTier {
	UUID getId();

	ClientRecord registerClient(UUID clientId, String address);

	void unregisterClient(UUID clientId);

	QueueRecord createQueue(UUID id) throws QueueCreationException;

	Collection<QueueRecord> queryForClient(UUID clientId,Collection<UUID> queueIds, Boolean includeCommon);

	MessageRecord queryForClientBySender(UUID clientId, UUID senderId,
			QueueReadOrder order, Boolean pop);

	MessageRecord read(UUID queueId, UUID clientId, QueueReadOrder order,
			Boolean pop, Boolean includeCommon) throws QueueEmptyException, QueueNonExistantException;

	MessageRecord sendMessageToQueue(UUID queueId, String message,
			UUID senderId, UUID receiverId, String context, Integer priority)
			throws QueueNonExistantException;

	Boolean checkIfQueueEmpty(UUID queueId) throws QueueNonExistantException;

	void deleteQueue(UUID id) throws QueueDeletionException;

	QueueRecord getQueueById(UUID queueId) throws QueueNonExistantException;

	Collection<QueueRecord> getAllQueues();

	Collection<MessageRecord> sendMessageToMultipleQueues(
			Collection<UUID> qids,String message,UUID senderId, Integer priority) throws QueueNonExistantException;
	
	Collection<ClientRecord> getAllClients();

	Integer getQueueSize(UUID id) throws QueueNonExistantException;

}
