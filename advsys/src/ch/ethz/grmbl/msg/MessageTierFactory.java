package ch.ethz.grmbl.msg;

import java.io.IOException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import ch.ethz.grmbl.data.DataTier;
import ch.ethz.grmbl.data.DataTierFactory;
import ch.ethz.grmbl.exception.DBConnectionException;
import ch.ethz.grmbl.remote.RemotingFactory;
import ch.ethz.grmbl.remote.RemotingServer;
import ch.ethz.grmbl.util.Config;
import ch.ethz.grmbl.util.Logging;

public class MessageTierFactory {
	private static final Logger log = Logger.getLogger(MessageTierFactory.class
			.getName());

	public static MessageTier getMessageTier() {
		return getMessageTier(Config.get().messageTierAddress);
	}

	public static synchronized MessageTier getMessageTier(String address) {
		if (!Config.get().remoteMessageTier) {
			try {
				return getLocalMessageTier();
			} catch (DBConnectionException e) {
				throw new RuntimeException(e);
			}
		} else {
			return getRemoteMessageTier(address);
		}
	}

	public static synchronized MessageTier getRemoteMessageTier(String address) {
		log.fine("getting remote message tier " + address);
		String hostname;
		int port;
		if (!address.contains(":")) {
			port = 80;
			hostname = address;
		} else {
			String[] split = address.split(":");
			hostname = split[0];
			port = Integer.parseInt(split[1]);
		}
		MessageTier mt;
		try {
			mt = RemotingFactory.createRemotingClient(MessageTier.class,
					hostname, port);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return mt;
	}

	public static synchronized MessageTier getLocalMessageTier()
			throws DBConnectionException {
		log.fine("getting local message tier");
//		Logging.error().log(Level.INFO,"Datatieraddress " + Config.get().dbServerName);
		DataTier dt = DataTierFactory.getDataTier();
		MessageTierImpl mt = new MessageTierImpl(UUID.randomUUID(), dt);
		return mt;
	}

	public static synchronized RemotingServer<MessageTier> createMessageTierServer(
			int port) throws IOException, DBConnectionException {
		MessageTier impl = getLocalMessageTier();
		return RemotingFactory.createRemotingServer(impl, port);
	}

}
