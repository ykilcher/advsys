package ch.ethz.grmbl.exception;

import java.util.UUID;

public class AbstractQueueException extends Exception{

	private static final long serialVersionUID = 1002305724190195947L;

	private UUID queueId;

	public AbstractQueueException(UUID queueId, String message, Throwable cause) {
		super(message, cause);
		this.queueId = queueId;
	}

	public AbstractQueueException(UUID queueId, String message) {
		super(message);
		this.queueId = queueId;
	}
	
	public UUID getQueueId() {
		return queueId;
	}

}
