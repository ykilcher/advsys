package ch.ethz.grmbl.exception;

public class DBConnectionException extends Exception 
{
	private static final long serialVersionUID = -3807089310913276947L;
	
	private Throwable ex;
	
	public DBConnectionException(String message, Throwable e) {
		super(message);
		ex = e;
	}
	
	public void printStackTrace()
	{
		super.printStackTrace();
		ex.printStackTrace();
	}
}
