package ch.ethz.grmbl.exception;

import java.util.UUID;

public class QueueCreationException extends AbstractQueueException{

	private static final long serialVersionUID = 8098868589178878478L;

	public QueueCreationException(UUID queueId, String message,
			Throwable cause) {
		super(queueId, message, cause);
	}

	public QueueCreationException(UUID queueId, String message) {
		super(queueId, message);
	}
	
	
	
}
