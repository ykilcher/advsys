package ch.ethz.grmbl.exception;

import java.util.UUID;

public class QueueNonExistantException extends AbstractQueueException {

	private static final long serialVersionUID = 1346767675391370455L;

	public QueueNonExistantException(UUID queueId, String message,
			Throwable cause) {
		super(queueId, message, cause);
	}

	public QueueNonExistantException(UUID queueId, String message) {
		super(queueId, message);
	}
	

}
