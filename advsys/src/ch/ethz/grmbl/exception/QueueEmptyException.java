package ch.ethz.grmbl.exception;

import java.util.UUID;

public class QueueEmptyException extends AbstractQueueException {

	private static final long serialVersionUID = 8324720847894269728L;

	public QueueEmptyException(UUID queueId, String message, Throwable cause) {
		super(queueId,message, cause);
	}

	public QueueEmptyException(UUID queueId, String message) {
		super(queueId,message);
	}
	

}
