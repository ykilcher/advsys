package ch.ethz.grmbl.exception;

import java.util.UUID;

public class QueueDeletionException extends AbstractQueueException{

	private static final long serialVersionUID = -9211384467479093778L;

	public QueueDeletionException(UUID queueId, String message, Throwable cause) {
		super(queueId,message, cause);
	}

	public QueueDeletionException(UUID queueId, String message) {
		super(queueId,message);
	}
	
}
