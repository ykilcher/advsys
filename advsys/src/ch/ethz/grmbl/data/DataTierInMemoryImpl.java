package ch.ethz.grmbl.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import ch.ethz.grmbl.exception.QueueEmptyException;
import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.model.ClientRecord;
import ch.ethz.grmbl.model.ClientRecordImpl;
import ch.ethz.grmbl.model.MessageRecord;
import ch.ethz.grmbl.model.MessageRecordImpl;
import ch.ethz.grmbl.model.QueueReadOrder;
import ch.ethz.grmbl.model.QueueRecord;
import ch.ethz.grmbl.model.QueueRecordImpl;

public class DataTierInMemoryImpl implements DataTier {
	private Map<UUID, ClientRecordImpl> clients = new ConcurrentHashMap<UUID,ClientRecordImpl>();
	private Map<UUID, QueueRecordImpl> queues = new ConcurrentHashMap<UUID,QueueRecordImpl>();

	@Override
	public QueueRecord createQueue(QueueRecord queue) {
		QueueRecordImpl q = new QueueRecordImpl(queue.getId());
		queues.put(q.getId(), q);
		return q;
	}

	@Override
	public void deleteQueue(UUID id) {
		queues.remove(id);
	}

	@Override
	public Collection<QueueRecord> getAllQueues() {
		Set<QueueRecord> rs = new HashSet<QueueRecord>();
		rs.addAll(queues.values());
		return rs;
	}

	@Override
	public Collection<QueueRecord> getQueuesForClientBySender(UUID clientId,
			UUID senderId) {
		Set<QueueRecord> qset = new HashSet<QueueRecord>();
		Collection<QueueRecordImpl> qs = this.queues.values();
		for (QueueRecordImpl q : qs) {
			Set<MessageRecordImpl> msgs = q.getMessages();
			for (MessageRecordImpl msg : msgs) {
				if (msg.getSenderId().equals(senderId)
						&& (msg.getReceiverId() == null || msg.getReceiverId()
								.equals(clientId))) {
					qset.add(q);
					break;
				}
			}
		}
		return qset;
	}

	private MessageRecordImpl createMessageRecordImpl(MessageRecord message) {
		MessageRecordImpl m = new MessageRecordImpl(message.getId());
		m.setArrivalTime(message.getArrivalTime());
		m.setMessage(message.getMessage());
		m.setPriority(message.getPriority());
		m.setReceiverId(message.getReceiverId());
		m.setSenderId(message.getSenderId());
		m.setContext(message.getContext());
		m.setQueueId(message.getQueueId());
		return m;
	}

	@Override
	public MessageRecord read(UUID queueId, UUID clientId,
			QueueReadOrder order, boolean pop, boolean includeCommon) throws QueueEmptyException {
		QueueRecordImpl q = queues.get(queueId);
		MessageRecord m = getFirstForClient(q, clientId, order,pop); //TODO
		if(m == null){
			throw new QueueEmptyException(queueId,"Queue Empty");
		}
		return m;
	}

	@Override
	public MessageRecord getMessageForClientBySender(UUID clientId,
			UUID senderId,QueueReadOrder order, boolean pop) {
		//TODO: implement order and pop
		for (QueueRecordImpl qr : queues.values()) {
			for (MessageRecord msg : qr.getMessages()) {
				if (msg.getSenderId().equals(senderId)
						&& clientId.equals(msg.getReceiverId())) {
					return msg;
				}
			}
		}
		return null;
	}
	
	private MessageRecord getFirstForClient(QueueRecordImpl queue,
			UUID clientId, QueueReadOrder order) {
		return this.getFirstForClient(queue, clientId, order,false);
	}

	private MessageRecord getFirstForClient(QueueRecordImpl queue,
			UUID clientId, QueueReadOrder order,boolean remove) {
		Set<MessageRecordImpl> msgs = queue.getMessages();
		MessageRecord msg = null;
		for (MessageRecordImpl m : msgs) {
			if (m == null || m.getReceiverId() != null
					&& !m.getReceiverId().equals(clientId)) {
				continue;
			}
			if (msg == null) {
				msg = m;
				continue;
			}
			if (QueueReadOrder.TIME.equals(order)) {
				if (msg.getArrivalTime() > m.getArrivalTime()) {
					msg = m;
				}
			} else {
				if (msg.getPriority() > m.getPriority()) {
					msg = m;
				}
			}
		}
		if(remove){
			queue.getMessages().remove(msg);
		}
		return msg;
	}

	@Override
	public ClientRecord registerClient(ClientRecord client) {
		ClientRecordImpl c = new ClientRecordImpl(client.getId());
		clients.put(c.getId(), c);
		return c;
	}

	@Override
	public void unregisterClient(UUID clientId) {
		clients.remove(clientId);
	}

	Map<UUID, ClientRecordImpl> getClients() {
		return clients;
	}

	Map<UUID, QueueRecordImpl> getQueues() {
		return queues;
	}

	@Override
	public boolean checkIfQueueEmpty(UUID queueId) {
		return this.queues.get(queueId).getMessages().isEmpty();
	}
	
	@Override
	public int getNumMessagesInQueue(UUID id) throws QueueNonExistantException {
		return this.queues.get(id).getMessages().size();
	}

	@Override
	public QueueRecord getQueueById(UUID id) {
		return queues.get(id);
	}
	
	@Override
	public Collection<MessageRecord> sendMessageToQueues(
			MessageRecord message, Collection<UUID> qids)
			throws QueueNonExistantException {
		Set<MessageRecord> msgs = new HashSet<MessageRecord>();
		for (UUID qid : qids) {
			MessageRecordImpl mr = createMessageRecordImpl(message);
			mr.setQueueId(qid);
			QueueRecordImpl qri = this.queues.get(qid);
			qri.getMessages().add(mr);
			msgs.add(mr);
		}
		return msgs;
	}

	@Override
	public void deleteMessageById(UUID id) {
		for (QueueRecordImpl q : this.queues.values()) {
			for (MessageRecordImpl m : q.getMessages()) {
				if(m.getId().equals(id)){
					q.getMessages().remove(m);
					return;
				}
			}
		}
	}

	@Override
	public Collection<ClientRecord> getAllClients() {
		Set<ClientRecord> crs = new HashSet<ClientRecord>();
		crs.addAll(this.clients.values());
		return crs;
	}

	@Override
	public int getNumClients() {
		return clients.size();
	}

	@Override
	public int getNumQueues() {
		return queues.size();
	}

	@Override
	public int getNumMessages() {
		int sum = 0;
		for (QueueRecordImpl q : this.queues.values()) 
		{
			sum += q.getMessages().size();
		}
		return sum;
	}

	@Override
	public Map<UUID, Integer> getNumMessagesInAllQueues() {
		HashMap<UUID, Integer> result = new HashMap<UUID, Integer>();
		for(Entry<UUID, QueueRecordImpl> entry: queues.entrySet())
			result.put(entry.getKey(), entry.getValue().getMessages().size());
		return null;
	}

	@Override
	public void reset() {
		clients.clear();
		queues.clear();
		
	}

	@Override
	public Collection<QueueRecord> getQueuesForClient(UUID clientId,
			Collection<UUID> queueIds, boolean includeCommon) {
		Set<QueueRecord> qset = new HashSet<QueueRecord>();
		Collection<QueueRecordImpl> qs = queues.values();
		for (QueueRecordImpl q : qs) {
			if(queueIds != null && !queueIds.contains(q.getId())){
				continue;
			}
			Set<MessageRecordImpl> msgs = q.getMessages();
			for (MessageRecordImpl msg : msgs) {
				if ((includeCommon && msg.getReceiverId() == null)
						|| msg.getReceiverId().equals(clientId)) {
					qset.add(q);
					break;
				}
			}
		}
		return qset;
	}

}
