package ch.ethz.grmbl.data;

import java.util.Calendar;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import ch.ethz.grmbl.exception.QueueCreationException;
import ch.ethz.grmbl.exception.QueueDeletionException;
import ch.ethz.grmbl.exception.QueueEmptyException;
import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.model.ClientRecord;
import ch.ethz.grmbl.model.MessageRecord;
import ch.ethz.grmbl.model.MessageRecordImpl;
import ch.ethz.grmbl.model.QueueReadOrder;
import ch.ethz.grmbl.model.QueueRecord;
import ch.ethz.grmbl.util.Pair;

public class DataTierCache implements DataTier {
	
	
	private static final Logger log = Logger.getLogger(DataTierCache.class
			.getName());

	private static final long QUEUE_CACHE_LIMIT = 1000;

	private DataTier impl;

	private Map<UUID, Pair<Long, CachedQueueImpl>> queues = new ConcurrentHashMap<UUID,Pair<Long,CachedQueueImpl>>();

	private Calendar calendar = Calendar.getInstance();

	private MessageRecordImpl createMessageImplFromMessage(MessageRecord msg) {
		MessageRecordImpl mri = new MessageRecordImpl(msg.getId());
		mri.setArrivalTime(msg.getArrivalTime());
		mri.setMessage(msg.getMessage());
		mri.setPriority(msg.getPriority());
		mri.setQueueId(msg.getQueueId());
		mri.setReceiverId(msg.getReceiverId());
		mri.setSenderId(msg.getSenderId());
		mri.setContext(msg.getContext());
		return mri;
	}

	private CachedQueueImpl createCachedQueueFromQueue(QueueRecord queue) {
		CachedQueueImpl qri = new CachedQueueImpl(queue.getId());
		if (queue instanceof CachedQueueImpl) {
			CachedQueueImpl q = (CachedQueueImpl) queue;
			for (MessageRecordImpl mri : q.getMessages()) {
				MessageRecordImpl m = this.createMessageImplFromMessage(mri);
				qri.addMessage(m);
			}
		}
		return qri;
	}

	private MessageRecordImpl putMessageToCache(MessageRecord msg) {
		MessageRecordImpl mri = this.createMessageImplFromMessage(msg);
		UUID qid = mri.getQueueId();
		CachedQueueImpl qri = this.getCachedQueue(msg.getQueueId());
		if (qri == null) {
			qri = this.createCachedQueue(qid);
		}
		qri.getMessages().add(mri);
		return mri;
	}

	private CachedQueueImpl createCachedQueue(UUID id) {
		CachedQueueImpl qri = new CachedQueueImpl(id);
		this.putQueueToCache(qri);
		return qri;
	}

	private CachedQueueImpl putQueueToCache(QueueRecord queue) {
		CachedQueueImpl qri = this.createCachedQueueFromQueue(queue);
		this.queues
				.put(queue.getId(),
						new Pair<Long, CachedQueueImpl>(calendar
								.getTimeInMillis(), qri));
		return qri;
	}

	private CachedQueueImpl mergeQueueToCache(QueueRecord queue) {
		CachedQueueImpl qri = this.getCachedQueue(queue.getId());
		if (qri != null) {
			if (queue instanceof CachedQueueImpl) {
				CachedQueueImpl q = (CachedQueueImpl) queue;
				// FIXME: this is really bad. overload hash & equals in MRI and
				// QRI to use our ids
				Set<MessageRecordImpl> msgs = qri.getMessages();
				Set<MessageRecordImpl> nmsgs = q.getMessages();
				for (MessageRecordImpl nm : nmsgs) {
					boolean has = false;
					for (MessageRecordImpl m : msgs) {
						if (m.getId().equals(nm)) {
							has = true;
							break;
						}
					}
					if (!has) {
						msgs.add(nm);
					}
				}
			}
			return qri;
		} else {
			return putQueueToCache(queue);
		}
	}

	private CachedQueueImpl getCachedQueue(UUID queueId) {
		Pair<Long, CachedQueueImpl> p = this.queues.get(queueId);
		if (p == null) {
			return null;

		}
		long limit = calendar.getTimeInMillis() - QUEUE_CACHE_LIMIT;
		if (p.getLeft() < limit) {
			this.queues.remove(queueId);
			return null;
		}
		return p.getRight();
	}

	public DataTierCache(DataTier dataTier) {
		this.impl = dataTier;
	}

	@Override
	public QueueRecord createQueue(QueueRecord queue)
			throws QueueCreationException {
		QueueRecord q = impl.createQueue(queue);
		this.putQueueToCache(q);
		return q;
	}

	@Override
	public void deleteQueue(UUID id) throws QueueDeletionException {
		this.queues.remove(id);
		impl.deleteQueue(id);
	}

	@Override
	public Collection<QueueRecord> getAllQueues() {
		Collection<QueueRecord> qs = impl.getAllQueues();
		Map<UUID, Pair<Long, CachedQueueImpl>> qm = new ConcurrentHashMap<UUID,Pair<Long,CachedQueueImpl>>();
		long now = calendar.getTimeInMillis();
		for (QueueRecord qr : qs) {
			qm.put(qr.getId(),
					new Pair<Long,CachedQueueImpl>(now, this.createCachedQueueFromQueue(qr)));
		}
		this.queues = qm;
		return qs;
	}

	@Override
	public Collection<QueueRecord> getQueuesForClientBySender(UUID clientId,
			UUID senderId) {
		Collection<QueueRecord> qs = impl.getQueuesForClientBySender(clientId,
				senderId);
		for (QueueRecord qr : qs) {
			this.mergeQueueToCache(qr);
		}
		return qs;
	}

	@Override
	public MessageRecord getMessageForClientBySender(UUID clientId,
			UUID senderId,QueueReadOrder order, boolean pop) {
		MessageRecord mr = impl.getMessageForClientBySender(clientId, senderId,order,pop);
		this.putMessageToCache(mr);
		return mr;
	}

	@Override
	public MessageRecord read(UUID queueId, UUID clientId,
			QueueReadOrder order, boolean pop,boolean includeCommon) throws QueueEmptyException,
			QueueNonExistantException {
		MessageRecord mr = impl.read(queueId, clientId, order, pop,includeCommon);
		this.putMessageToCache(mr);
		return mr;
	}

	@Override
	public ClientRecord registerClient(ClientRecord client) {
		return impl.registerClient(client);
	}

	@Override
	public void unregisterClient(UUID clientId) {
		impl.unregisterClient(clientId);
	}

	@Override
	public boolean checkIfQueueEmpty(UUID queueId)
			throws QueueNonExistantException {
		CachedQueueImpl qri = this.getCachedQueue(queueId);
		if (qri != null && !qri.getMessages().isEmpty()) {
			return false;
		}
		return impl.checkIfQueueEmpty(queueId);
	}

	@Override
	public QueueRecord getQueueById(UUID id) throws QueueNonExistantException {
		CachedQueueImpl cq = this.getCachedQueue(id);
		if (cq != null) {
			return cq;
		}
		return impl.getQueueById(id);
	}
	
	@Override
	public Collection<MessageRecord> sendMessageToQueues(
			MessageRecord message, Collection<UUID> qids)
			throws QueueNonExistantException {
		Collection<MessageRecord> msgs = impl.sendMessageToQueues(message, qids);
		for (MessageRecord msg : msgs) {
			this.putMessageToCache(msg);
		}
		return msgs;
	}

	@Override
	public void deleteMessageById(UUID id) {
		this.impl.deleteMessageById(id);
	}

	@Override
	public Collection<ClientRecord> getAllClients() {
		return impl.getAllClients();
	}

	@Override
	public int getNumClients() {
		return impl.getNumClients();
	}

	@Override
	public int getNumQueues() {
		return impl.getNumQueues();
	}

	@Override
	public int getNumMessages() {
		return impl.getNumMessages();
	}

	@Override
	public Map<UUID, Integer> getNumMessagesInAllQueues() {
		return impl.getNumMessagesInAllQueues();
	}
	
	public int getNumMessagesInQueue(UUID id) throws QueueNonExistantException {
		return impl.getNumMessagesInQueue(id);
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Collection<QueueRecord> getQueuesForClient(UUID clientId,
			Collection<UUID> queueIds, boolean includeCommon) {
		Collection<QueueRecord> qs = impl.getQueuesForClient(clientId,queueIds,includeCommon);
		for (QueueRecord qr : qs) {
			this.mergeQueueToCache(qr);
		}
		return qs;
	}
}
