package ch.ethz.grmbl.data;

import ch.ethz.grmbl.exception.DBConnectionException;
import ch.ethz.grmbl.util.Config;

public class DataTierFactory {
	private static DataTier dataTier;

	public static synchronized DataTier getDataTier() throws DBConnectionException{
		if(dataTier != null){
			return dataTier;
		}
		DataTier dt = null;
		switch(Config.get().dataTierImplementation){
		case IN_MEMORY:
			dt = new DataTierInMemoryImpl();
			break;
		case JDBC:
			dt = new DataTierInDBImpl();
			break;
		case JDBCv2:
			dt = new DataTierInDBV2Impl();
			break;
		default:
			break;
		}
		if(dt == null){
			throw new RuntimeException("Data Tier instantiation failed");
		}
		if(Config.get().dataTierCaching){
			dt = new DataTierCache(dt);
		}
		dataTier = dt;
		return dataTier;
	}

}
