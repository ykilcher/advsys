package ch.ethz.grmbl.data;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.UUID;

import ch.ethz.grmbl.model.MessageRecordImpl;
import ch.ethz.grmbl.model.QueueReadOrder;
import ch.ethz.grmbl.model.QueueRecordImpl;
import ch.ethz.grmbl.util.Pair;

public class CachedQueueImpl extends QueueRecordImpl {

	private static final long serialVersionUID = -2855592408944447757L;

	private Map<UUID, MessageRecordImpl> messageMap = new HashMap<UUID,MessageRecordImpl>();

	private Queue<Pair<UUID, Integer>> priorityQueue = new PriorityQueue<Pair<UUID,Integer>>(0,
			new Comparator<Pair<UUID, Integer>>() {
				@Override
				public int compare(Pair<UUID, Integer> o1,
						Pair<UUID, Integer> o2) {
					return o1.getRight().compareTo(o2.getRight());
				}

			});

	private Queue<Pair<UUID, Long>> timeQueue = new PriorityQueue<Pair<UUID,Long>>(0,
			new Comparator<Pair<UUID, Long>>() {
				@Override
				public int compare(Pair<UUID, Long> o1, Pair<UUID, Long> o2) {
					return o1.getRight().compareTo(o2.getRight());
				}
			});

	public CachedQueueImpl(UUID id) {
		super(id);
	}

	@Override
	public Set<MessageRecordImpl> getMessages() {
		return new HashSet<MessageRecordImpl>(this.messageMap.values());
	}

	public synchronized void addMessage(MessageRecordImpl m) {
		this.messageMap.put(m.getId(), m);
		this.priorityQueue.add(new Pair<UUID, Integer>(m.getId(), m
				.getPriority()));
		this.timeQueue
				.add(new Pair<UUID, Long>(m.getId(), m.getArrivalTime()));
	}

	public MessageRecordImpl read(QueueReadOrder order, boolean pop) {
		Pair<UUID, ?> p = null;
		switch (order) {
		case PRIORITY:
			if (pop) {
				p = this.priorityQueue.poll();
			} else {
				p = this.priorityQueue.peek();
			}
			break;
		case TIME:
			if(pop){
				p = this.timeQueue.poll();
			}else{
				p=this.timeQueue.peek();
			}
			break;
		}
		if(p == null){
			return null;
		}
		UUID id = p.getLeft();
		return this.messageMap.get(id);
	}

}
