package ch.ethz.grmbl.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

import org.postgresql.ds.PGPoolingDataSource;

import ch.ethz.grmbl.exception.DBConnectionException;
import ch.ethz.grmbl.exception.QueueCreationException;
import ch.ethz.grmbl.exception.QueueDeletionException;
import ch.ethz.grmbl.exception.QueueEmptyException;
import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.model.ClientRecord;
import ch.ethz.grmbl.model.ClientRecordImpl;
import ch.ethz.grmbl.model.MessageRecord;
import ch.ethz.grmbl.model.MessageRecordImpl;
import ch.ethz.grmbl.model.QueueReadOrder;
import ch.ethz.grmbl.model.QueueRecord;
import ch.ethz.grmbl.model.QueueRecordImpl;
import ch.ethz.grmbl.util.Config;
import ch.ethz.grmbl.util.Logging;
import ch.ethz.grmbl.util.Logging.TimingLogger;

public class DataTierInDBImpl implements DataTier {

	protected PGPoolingDataSource connectionPool;
	protected String schema = "asl";
	private static final String LOG_PREFIX = "DataTierInDBImpl";

	public DataTierInDBImpl() throws DBConnectionException {
		try {
			init();
		} catch (SQLException e) {
			throw new DBConnectionException(
					"Couldn't connect to the database.", e);
		}
	}

	private void init() throws SQLException {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " init");
		Config config = Config.get();
		connectionPool = new PGPoolingDataSource();
		connectionPool.setDataSourceName("aslPool");
		connectionPool.setServerName(config.dbServerName);
		connectionPool.setDatabaseName(config.dbName);
		connectionPool.setUser(config.dbUsername);
		connectionPool.setPassword(config.dbPassword);
		connectionPool.setPortNumber(config.dbPort);
		System.out.println("allocating " + config.dbMaxConnections + " connections to db");
		connectionPool.setMaxConnections(config.dbMaxConnections);
		connectionPool.setSsl(false);
		tlog.end();
	}

	private String trimQuotes(String s) {
		if (s == null || s.length() < 1) {
			return s;
		}
		int beg = 0;
		int end = s.length();
		while (beg < end && s.charAt(beg) == '"') {
			beg++;
		}
		if (beg == end) {
			return "";
		}
		while (s.charAt(end - 1) == '"') {
			end--;
		}
		return s.substring(beg, end);
	}

	protected void logUnknwonException(SQLException e) {
		Logging.error().log(Level.SEVERE, LOG_PREFIX + " Unknown SQLException",
				e);
	}

	protected void logCloseConnectionException(SQLException e) {
		Logging.error()
				.log(Level.SEVERE,
						LOG_PREFIX
								+ " Unknown exception while closing the connection to the db",
						e);
	}

	@Override
	public QueueRecord createQueue(QueueRecord queue)
			throws QueueCreationException {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " createQueue");
		QueueRecordImpl q = new QueueRecordImpl(queue.getId());
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".createQueue(?)");
			stmt.setObject(1, queue.getId());
			stmt.execute();
		} catch (SQLException e) {
			throw new QueueCreationException(queue.getId(),
					"Couldn't create a new queue.", e);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		tlog.end();
		return q;
	}

	@Override
	public void deleteQueue(UUID id) throws QueueDeletionException {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " deleteQueue");
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".deleteQueue(?)");
			stmt.setObject(1, id);
			stmt.execute();
		} catch (SQLException e) {
			throw new QueueDeletionException(id,
					"Couldn't delete the queue with the id " + id + ".", e);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		tlog.end();
	}

	@Override
	public void reset() {
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			con.setAutoCommit(false);
			Statement stmt = con.createStatement();
			stmt.addBatch("DELETE FROM asl.message"); // DB v1
			stmt.addBatch("DELETE FROM asl.queue");
			stmt.addBatch("DELETE FROM asl.client");
			stmt.addBatch("DELETE FROM asl2.isIn"); // DBv2
			stmt.addBatch("DELETE FROM asl2.message");
			stmt.addBatch("DELETE FROM asl2.queue");
			stmt.addBatch("DELETE FROM asl2.client");
			stmt.executeBatch();
			con.commit();
		} catch (SQLException e) {
			throw new RuntimeException("Could not reset DB");
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
	}

	@Override
	public Collection<QueueRecord> getAllQueues() {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getAllQueues");
		HashSet<QueueRecord> result = new HashSet<QueueRecord>();
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".getAllQueues()");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				UUID id = (UUID) rs.getObject(1);
				result.add(new QueueRecordImpl(id));
			}
		} catch (SQLException e) {
			logUnknwonException(e);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		tlog.end(Integer.toString(result.size()));
		return result;
	}

	@Override
	public Collection<QueueRecord> getQueuesForClientBySender(UUID clientId,
			UUID senderId) {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getQueuesForClientBySender");
		HashSet<QueueRecord> result = new HashSet<QueueRecord>();
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".getQueuesForClientBySender(?, ?)");
			stmt.setObject(1, clientId);
			stmt.setObject(2, senderId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				result.add(new QueueRecordImpl((UUID) rs.getObject(1)));
			}
		} catch (SQLException e) {
			logUnknwonException(e);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		tlog.end(Integer.toString(result.size()));
		return result;
	}

	private MessageRecord parseMessageRecord(ResultSet rs) throws SQLException {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " parseMessageRecord");
		String[] values = rs.getString(1).split(",");
		MessageRecordImpl record = new MessageRecordImpl(
				UUID.fromString(values[0].substring(1)));
		record.setSenderId(UUID.fromString(values[1]));
		if (values[2].length() > 0)
			record.setReceiverId(UUID.fromString(values[2]));
		record.setContext(trimQuotes(values[3]));
		record.setMessage(trimQuotes(values[4]));
		try {
			values[5] = trimQuotes(values[5]);
			values[5] += "000";
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			long time = df.parse(values[5].substring(1, 24)).getTime();
			record.setArrivalTime(time);
		} catch (ParseException e) {
			Logging.error().log(Level.SEVERE, "Unknown timestamp format", e);
		}
		record.setQueueId(UUID.fromString(values[6]));
		record.setPriority(Integer.parseInt(values[7].substring(0,
				values[7].length() - 1)));
		tlog.end();
		return record;
	}

	@Override
	public MessageRecord getMessageForClientBySender(UUID clientId,
			UUID senderId, QueueReadOrder order, boolean pop) {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getMessageForClientBySender");
		MessageRecord result = null;
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".getMessageForClientBySender(?, ?, ?, ?)");
			stmt.setObject(1, senderId);
			stmt.setObject(2, clientId);
			if (order == QueueReadOrder.PRIORITY)
				stmt.setBoolean(3, true);
			else
				stmt.setBoolean(3, false);
			stmt.setBoolean(4, pop);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				result = parseMessageRecord(rs);
			}
		} catch (SQLException e) {
			logUnknwonException(e);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		tlog.end(result == null ? "fail" : "success");
		return result;
	}

	@Override
	public MessageRecord read(UUID queueId, UUID clientId,
			QueueReadOrder order, boolean pop, boolean includeCommon) throws QueueEmptyException,
			QueueNonExistantException {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " read");
		MessageRecord result = null;
		Connection con = null;
		// System.out.println("Test0");
		try {
			// System.out.println("Test1");
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".read(?, ?, ?, ?, ?)");
			stmt.setObject(1, queueId);
			stmt.setObject(2, clientId);
			if (order == QueueReadOrder.PRIORITY){
				stmt.setBoolean(3, true);
			}
			else{
				stmt.setBoolean(3, false);
			}
			stmt.setBoolean(4, pop);
			stmt.setBoolean(5, includeCommon);
			// System.out.println("Test2");
			ResultSet rs = stmt.executeQuery();
			// System.out.println("Test3");
			if (rs.next()) {
				result = parseMessageRecord(rs);
			}
			// System.out.println("Test4");
		} catch (SQLException e) {
			if (e.getMessage().indexOf(") does not exist") > 0)
				throw new QueueNonExistantException(queueId, clientId
						+ " wasn't able to read any message from the queue "
						+ queueId, e);
			throw new QueueEmptyException(queueId, clientId
					+ " wasn't able to read any message from the queue "
					+ queueId, e);
		} finally {
			// System.out.println("Test6");
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		tlog.end();
		return result;
	}

	@Override
	public ClientRecord registerClient(ClientRecord client) {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " registerClient");
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".registerClient(?)");
			stmt.setObject(1, client.getId());
			stmt.execute();
		} catch (SQLException e) {
			logUnknwonException(e);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		tlog.end();
		return client;
	}

	@Override
	public void unregisterClient(UUID clientId) {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " unregisterClient");
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".unregisterClient(?)");
			stmt.setObject(1, clientId);
			stmt.execute();
		} catch (SQLException e) {
			logUnknwonException(e);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		tlog.end();
	}

	@Override
	public boolean checkIfQueueEmpty(UUID queueId)
			throws QueueNonExistantException {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " checkIfQueueEmpty");
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".checkIfQueueEmpty(?)");
			stmt.setObject(1, queueId);
			stmt.execute();
			tlog.end("true");
			return true;
		} catch (SQLException e) {/*
								 * queue isn't empty - might also be a different
								 * exception
								 */
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		tlog.end("false");
		return false;
	}

	@Override
	public QueueRecord getQueueById(UUID id) throws QueueNonExistantException {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getQueueById");
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".existsQueue(?)");
			stmt.setObject(1, id);
			stmt.execute();

			QueueRecordImpl qr = new QueueRecordImpl(id);
			tlog.end();
			return qr;
		} catch (SQLException e) {
			throw new QueueNonExistantException(id, "The queue (id: "
					+ id.toString() + ") doesn't exist.", e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
			}
		}
	}

	@Override
	public Collection<MessageRecord> sendMessageToQueues(MessageRecord message,
			Collection<UUID> qids) throws QueueNonExistantException {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " sendMessageToQueues");
		Connection con = null;
		HashSet<MessageRecord> entries = new HashSet<MessageRecord>();
		ArrayList<UUID> mids = new ArrayList<UUID>();
		for (int i = 0; i < qids.size(); ++i)
			mids.add(UUID.randomUUID());

		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".sendMessageToQueues(?,?,?,?,?,?,?)");
			stmt.setArray(1, con.createArrayOf("uuid", mids.toArray()));
			stmt.setObject(2, message.getSenderId());
			stmt.setObject(3, message.getReceiverId());
			stmt.setString(4, message.getContext());
			stmt.setString(5, message.getMessage());
			stmt.setObject(6, con.createArrayOf("uuid", qids.toArray()));
			stmt.setInt(7, message.getPriority());

			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				Iterator<UUID> qit = qids.iterator();
				Iterator<UUID> mit = mids.iterator();

				while (qit.hasNext() && mit.hasNext()) {
					MessageRecordImpl m = new MessageRecordImpl(mit.next());
					m.setMessage(message.getMessage());
					m.setArrivalTime(message.getArrivalTime());
					m.setContext(message.getContext());
					m.setPriority(message.getPriority());
					m.setQueueId(qit.next());
					m.setReceiverId(message.getReceiverId());
					m.setSenderId(message.getSenderId());
					entries.add(m);
				}
			}
		} catch (SQLException e) {
			throw new QueueNonExistantException(null,
					"One of the queues doesn't exist.", e);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		tlog.end(Integer.toString(qids.size()));
		return entries;
	}

	@Override
	public void deleteMessageById(UUID id) {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " deleteMessageById");
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".deleteMessage(?)");
			stmt.setObject(1, id);
			stmt.execute();
		} catch (SQLException e) {
			logUnknwonException(e);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		tlog.end();
	}

	@Override
	public Collection<ClientRecord> getAllClients() {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getAllClients");
		HashSet<ClientRecord> result = new HashSet<ClientRecord>();
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".getAllClients()");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				result.add(new ClientRecordImpl((UUID) rs.getObject(1)));
			}
		} catch (SQLException e) {
			logUnknwonException(e);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		tlog.end(Integer.toString(result.size()));
		return result;
	}

	public boolean existsMessage(UUID id) {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " existsMessage");
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".existsMessage(?)");
			stmt.setObject(1, id);
			stmt.execute();
			tlog.end("true");
			return true;
		} catch (SQLException e) {/*
								 * The message doesn't exist.
								 */
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		tlog.end("false");
		return false;
	}

	@Override
	public int getNumClients() {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getNumClients");
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".getNumClients()");
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				int res = rs.getInt(1);
				tlog.end(Integer.toString(res));
				return res;
			}
		} catch (SQLException e) {
			logUnknwonException(e);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
			}
		}
		tlog.end(Integer.toString(0));
		return 0;
	}

	@Override
	public int getNumQueues() {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getNumQueues");
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".getNumQueues()");
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				int res = rs.getInt(1);
				tlog.end(Integer.toString(res));
				return res;
			}
		} catch (SQLException e) {
			logUnknwonException(e);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		tlog.end(Integer.toString(0));
		return 0;
	}

	@Override
	public int getNumMessages() {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getNumMessages");
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".getNumMessages()");
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				int res = rs.getInt(1);
				tlog.end(Integer.toString(res));
				return res;
			}
		} catch (SQLException e) {
			logUnknwonException(e);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		tlog.end(Integer.toString(0));
		return 0;
	}

	@Override
	public Map<UUID, Integer> getNumMessagesInAllQueues() {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getNumMessagesInAllQueues");
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".getNumMessagesInAllQueues()");
			ResultSet rs = stmt.executeQuery();
			HashMap<UUID, Integer> result = new HashMap<UUID, Integer>();
			while (rs.next()) {
				String[] entry = rs.getString(1).split(",");
				result.put(
						UUID.fromString(entry[0].substring(1)),
						Integer.parseInt(entry[1].substring(0,
								entry[1].length() - 1)));
			}
			tlog.end(Integer.toString(result.size()));
			return result;
		} catch (SQLException e) {
			logUnknwonException(e);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		return null;
	}

	public int getNumMessagesInQueue(UUID id) throws QueueNonExistantException {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getNumMessagesInQueue");
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT " + schema
					+ ".getNumMessagesInQueue(?)");
			stmt.setObject(1, id);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				int res = rs.getInt(1);
				tlog.end(Integer.toString(res));
				return res;
			}
		} catch (SQLException e) {
			throw new QueueNonExistantException(id, "The queue doesn't exist.",
					e);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		tlog.end(Integer.toString(0));
		return 0;
	}

	@Override
	public Collection<QueueRecord> getQueuesForClient(UUID clientId,
			Collection<UUID> queueIds, boolean includeCommon) {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " getQueuesForClient");
		HashSet<QueueRecord> result = new HashSet<QueueRecord>();
		Connection con = null;
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt;
			if (queueIds == null) {
				stmt = con.prepareStatement("SELECT " + schema
						+ ".getQueuesForClient(?,?)");
				stmt.setObject(1, clientId);
				stmt.setBoolean(2, includeCommon);
			} else {
				stmt = con.prepareStatement("SELECT " + schema
						+ ".getQueuesForClientAndQueues(?,?,?)");
				stmt.setObject(1, clientId);
				stmt.setBoolean(2, includeCommon);
				stmt.setArray(3, con.createArrayOf("uuid", queueIds.toArray()));
			}
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				result.add(new QueueRecordImpl((UUID) rs.getObject(1)));
			}
		} catch (SQLException e) {
			logUnknwonException(e);
		} finally {
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
		}
		tlog.end(Integer.toString(result.size()));
		return result;
	}

	/*
	 * public static void main(String args[]) throws Exception {
	 * DataTierInDBImpl db = new DataTierInDBImpl(); UUID id =
	 * UUID.randomUUID(); //db.createQueue(new QueueRecordImpl(id)); db.read(id,
	 * UUID.randomUUID(), QueueReadOrder.PRIORITY, true); }
	 */
}
