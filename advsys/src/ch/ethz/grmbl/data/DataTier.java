package ch.ethz.grmbl.data;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import ch.ethz.grmbl.exception.QueueCreationException;
import ch.ethz.grmbl.exception.QueueDeletionException;
import ch.ethz.grmbl.exception.QueueEmptyException;
import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.model.ClientRecord;
import ch.ethz.grmbl.model.MessageRecord;
import ch.ethz.grmbl.model.QueueReadOrder;
import ch.ethz.grmbl.model.QueueRecord;

public interface DataTier {
	/**
	 * Creates a new Queue.
	 * 
	 * @param queue
	 *            the queue to be created
	 * @return a queue with the same id and other parameters as the queue
	 *         parameter
	 * @throws QueueCreationException
	 *             if creating this queue failed
	 */
	QueueRecord createQueue(QueueRecord queue) throws QueueCreationException;

	/**
	 * Deletes the specified queue.
	 * 
	 * @param id
	 *            the id of the queue to be deleted
	 * @throws QueueDeletionException
	 *             if deleting this queue failed
	 */
	void deleteQueue(UUID id) throws QueueDeletionException;

	/**
	 * Retrieves all active queues in the database.
	 * 
	 * @return a collection containing all active queues
	 */
	Collection<QueueRecord> getAllQueues();

	/**
	 * Retrieves all queues with messages for this client.
	 * 
	 * @param clientId
	 *            the client wanting to read the queues
	 * @param queueIds
	 *            a set of queues to query, or null for querying all queues
	 * @param includeCommon
	 *            whether or not to include messages that have no receiver
	 * @return a collection containing all queues with messages that this client
	 *         can read, thus, he might be the receiver or no receiver has been
	 *         specified.
	 */

	Collection<QueueRecord> getQueuesForClient(UUID clientId,Collection<UUID> queueIds,boolean includeCommon);

	/**
	 * Retrieves all queues with messages for this client from a particular
	 * sender
	 * 
	 * @param clientId
	 *            the client wanting to read
	 * @param senderId
	 *            the sender from whom the messages should come
	 * @return a collection containing all queues with messages that this client
	 *         can read and that have been authored by the specified sender.
	 */
	Collection<QueueRecord> getQueuesForClientBySender(UUID clientId,
			UUID senderId);


	/**
	 * Retrieves the next message for the client from a specific sender.
	 * 
	 * @param clientId
	 *            the client wanting to read
	 * @param senderId
	 *            the sender from whom the message should come
	 * @param order
	 *            the order by which the message should be selected
	 * @param pop
	 *            if true, the message is removed from the queue on return
	 * @return the next message for this client by the specified sender or null
	 *         if there is none
	 */
	MessageRecord getMessageForClientBySender(UUID clientId, UUID senderId,
			QueueReadOrder order, boolean pop);

	/**Reads the next message from a queue.
	 * @param queueId the queue to read from
	 * @param clientId the client wanting to read
	 * @param order the ordering of the messages
	 * @param pop if true, the message is removed from the queue upon read
	 * @return the next message for this client from this queue
	 * @throws QueueEmptyException if the queue is empty
	 * @throws QueueNonExistantException if the queue does not exist
	 */
	MessageRecord read(UUID queueId, UUID clientId, QueueReadOrder order,
			boolean pop, boolean includeCommon) throws QueueEmptyException, QueueNonExistantException;

	/**Registers a client with the system.
	 * @param client the client to be registered
	 * @return the registered client
	 */
	ClientRecord registerClient(ClientRecord client);

	/**Unregisters a client from the system.
	 * @param clientId the client to be unregistered
	 */
	void unregisterClient(UUID clientId);

	/**Checks if a queue is empty.
	 * @param queueId the queue to be checked
	 * @return true, if the queue is empty, else false
	 * @throws QueueNonExistantException if the queue does not exist
	 */
	boolean checkIfQueueEmpty(UUID queueId) throws QueueNonExistantException;

	/**Retrieve a queue by its id.
	 * @param id the id of the queue to retrieve
	 * @return the queue that has the specified id
	 * @throws QueueNonExistantException if the queue does not exist
	 */
	QueueRecord getQueueById(UUID id) throws QueueNonExistantException;

	/**Send a message to a number of queues.
	 * @param message the message to be sent
	 * @param qids the queues that the message should be sent to
	 * @return a collection with the newly created messages
	 * @throws QueueNonExistantException if the queue does not exist
	 */
	Collection<MessageRecord> sendMessageToQueues(MessageRecord message,
			Collection<UUID> qids) throws QueueNonExistantException;
	
	/**Delete a message by its id.
	 * @param id the id of the message to be deleted
	 */
	void deleteMessageById(UUID id);
	
	/**Retrieve all registered clients.
	 * @return a collection containing all currently registered clients
	 */
	Collection<ClientRecord> getAllClients();
	
	int getNumClients();

	int getNumQueues();

	int getNumMessages();

	Map<UUID, Integer> getNumMessagesInAllQueues();

	int getNumMessagesInQueue(UUID id) throws QueueNonExistantException;

	void reset();
}
