package ch.ethz.grmbl.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.UUID;
import java.util.logging.Level;

import ch.ethz.grmbl.exception.DBConnectionException;
import ch.ethz.grmbl.exception.QueueNonExistantException;
import ch.ethz.grmbl.model.MessageRecord;
import ch.ethz.grmbl.model.MessageRecordImpl;
import ch.ethz.grmbl.util.Logging;
import ch.ethz.grmbl.util.Logging.TimingLogger;

public class DataTierInDBV2Impl extends DataTierInDBImpl {
	
	private static final String LOG_PREFIX = "DataTierInDBV2Impl";
	public DataTierInDBV2Impl() throws DBConnectionException
	{
		super();
		schema = "asl2";
	}

	@Override
	public Collection<MessageRecord> sendMessageToQueues(MessageRecord message,
			Collection<UUID> qids) throws QueueNonExistantException {
		TimingLogger tlog = Logging.timingStart(Level.FINER,LOG_PREFIX + " sendMessageToQueues");
		Connection con = null;
		HashSet<MessageRecord> entries = new HashSet<MessageRecord>();
		UUID mid = UUID.randomUUID();
		
		try {
			con = connectionPool.getConnection();
			PreparedStatement stmt = con.prepareStatement("SELECT asl2.sendMessageToQueues(?,?,?,?,?,?,?)");
			stmt.setObject(1, mid);
			stmt.setObject(2, message.getSenderId());
			stmt.setObject(3, message.getReceiverId());
			stmt.setString(4, message.getContext());
			stmt.setString(5, message.getMessage());
			stmt.setObject(6, con.createArrayOf("uuid", qids.toArray()));
			stmt.setInt(7, message.getPriority());
			
			ResultSet rs = stmt.executeQuery();
			if(rs.next())
			{
				Iterator<UUID> qit = qids.iterator();
				// long timestamp = rs.getTimestamp(1).getTime();
				
				while(qit.hasNext())
				{
					MessageRecordImpl m = new MessageRecordImpl(mid);
					m.setMessage(message.getMessage());
					m.setArrivalTime(message.getArrivalTime());
					m.setContext(message.getContext());
					m.setPriority(message.getPriority());
					m.setQueueId(qit.next());
					m.setReceiverId(message.getReceiverId());
					m.setSenderId(message.getSenderId());
					entries.add(m);
				}

			}
		} catch (SQLException e) {
			throw new QueueNonExistantException(null, "One of the queues doesn't exist.", e);
		}
		finally
		{
			if(con != null)
				try {
					con.close();
				} catch (SQLException e) {
					logCloseConnectionException(e);
				}
			tlog.end();
		}
		return entries;
	}

	@Override
	public int getNumMessagesInQueue(UUID id) throws QueueNonExistantException {
		// TODO Auto-generated method stub
		return 0;
	}
}
