package ch.ethz.grmbl.remote;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.util.logging.Level;

import ch.ethz.grmbl.exception.ConnectionClosedException;
import ch.ethz.grmbl.util.DirectByteArrayOutputStream;
import ch.ethz.grmbl.util.Logging;
import ch.ethz.grmbl.util.Logging.TimingLogger;

public class RemotingClient {

	private static final String LOG_PREFIX = "RemotingClient";

	private SocketChannel channel;

	public void connect(String address, int port) throws IOException {
		SocketChannel channel = SocketChannel.open();
		channel.configureBlocking(true);
		channel.connect(new InetSocketAddress(address, port));
		this.channel = channel;
	}

	public RemoteResponse<?> doRequest(RemoteRequest request)
			throws IOException, ClassNotFoundException, ConnectionClosedException {
		TimingLogger tl = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " doRequest " + request.getId() + " " + request.getMethodName());
		TimingLogger tlwrite = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " writeRequest " + request.getId() + " " + request.getMethodName());
		DirectByteArrayOutputStream baos = new DirectByteArrayOutputStream();
		baos.setCount(4);
		ObjectOutputStream oos = null;
		int s = 0;
		try {
			oos = new ObjectOutputStream(baos);
			oos.writeObject(request);
			s = baos.getCount() - Integer.SIZE/Byte.SIZE;
			for (int i = Integer.SIZE - Byte.SIZE, j=0; i >= 0; i -= Byte.SIZE, j++) {
				baos.getBuf()[j] = (byte) (s >> i);
			}
//			this.channel.socket().getOutputStream().write(bs);
			baos.writeTo(this.channel.socket().getOutputStream());
		} finally {
			if (oos != null) {
				oos.close();
			}
		}
		tlwrite.end(Integer.toString(s));
		TimingLogger tlread = Logging.timingStart(Level.FINER, LOG_PREFIX
				+ " readResponse " + request.getId() + " " + request.getMethodName());
		InputStream is = this.channel.socket().getInputStream();
		int[] bsi = new int[Integer.SIZE/Byte.SIZE];
		for (int i = 0; i < Integer.SIZE / Byte.SIZE; i++) {
			int read = is.read();
			if(read == -1){
				throw new ConnectionClosedException();
			}
			bsi[i] = read;
		}
		
		s = 0;
		for (int i = 0; i < Integer.SIZE / Byte.SIZE; i++) {
			s |= (bsi[i] << i * Byte.SIZE);
		}
		byte b[] = new byte[s];
		for (int i = 0; i < s; i++) {
			int read = is.read();
			if(read == -1){
				throw new ConnectionClosedException();
			}
			b[i] = (byte) read;
		}
		RemoteResponse<?> resp;
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new ByteArrayInputStream(b));
			resp = (RemoteResponse<?>) ois.readObject();
		} finally {
			if (ois != null) {
				ois.close();
			}
		}
		tlread.end(Integer.toString(s));
		RemoteResponse<?> rresp = (RemoteResponse<?>) resp;
		tl.end(rresp.isSuccess() ? "success" : "fail");
		return rresp;
	}

	public void close() throws IOException {
		channel.close();
	}
}
