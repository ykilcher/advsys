package ch.ethz.grmbl.remote;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;

import ch.ethz.grmbl.util.Logging;
import ch.ethz.grmbl.util.Pair;
import ch.ethz.grmbl.util.Logging.TimingLogger;

abstract class ChannelBuffer {
	private static final String LOG_PREFIX = "ChannelBuffer";
	public int dataSize;
	public ByteBuffer dataBuffer;
	public int sizeSize;
	public ByteBuffer sizeBuffer;
	public Queue<Pair<ByteBuffer, TimingLogger>> outQueue = new ConcurrentLinkedQueue<Pair<ByteBuffer,TimingLogger>>();
	public SelectionKey key;
	private TimingLogger readTimeLog = null;
	private TimingLogger writeTimeLog = null;

	protected abstract void doneReading(final SelectionKey key,
			final ByteBuffer inBuf);

	public void read() throws IOException {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX + " readBytes");
		SocketChannel sc = (SocketChannel) key.channel();
		if (this.sizeBuffer == null) {
			this.sizeSize = Integer.SIZE / Byte.SIZE;
			this.sizeBuffer = ByteBuffer.allocate(this.sizeSize);
			this.sizeBuffer.clear();
		}
		if(this.sizeBuffer.position() == 0){
			this.readTimeLog = Logging.timingStart(Level.FINE, LOG_PREFIX + " readSizeBuffer");
		}
		int readBytes = 0;
		if (this.sizeBuffer.position() < this.sizeBuffer.capacity()) {
			int read = 1;
			while (read > 0) {
				read = sc.read(this.sizeBuffer);
				readBytes += read;
			}
			if (this.sizeBuffer.position() == this.sizeBuffer.capacity()) {
				this.readTimeLog.end(Integer.toString(this.sizeSize));
				this.sizeBuffer.flip();
				this.dataSize = this.sizeBuffer.getInt();
				if(this.dataBuffer == null || this.dataBuffer.capacity() < this.dataSize){
					this.dataBuffer = ByteBuffer.allocate(this.dataSize);
					this.dataBuffer.clear();
				}else{
					this.dataBuffer.limit(this.dataSize);
				}
				this.readTimeLog = Logging.timingStart(Level.FINE, LOG_PREFIX + " readDataBuffer");
			}
		} 
		if(this.sizeBuffer.position() == this.sizeBuffer.capacity()){
			int read = 1;
			while (read > 0) {
				read = sc.read(this.dataBuffer);
				readBytes += read;
			}
			if (this.dataBuffer.position() == this.dataBuffer.limit()) {
				this.readTimeLog.end(Integer.toString(this.dataSize));
				doneReading(key, this.dataBuffer);
				this.dataBuffer.clear();
				this.sizeBuffer.clear();
			}
		}
		tlog.end(Integer.toString(readBytes));
	}

	public void write() throws IOException {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX + " writeBytes");
		int writtenBytes = 0;
		if (!this.outQueue.isEmpty()) {
			while (true) {
				if (this.outQueue.size() > 0) {
					ByteBuffer bb = this.outQueue.peek().getLeft();
					if(this.writeTimeLog == null){
						writeTimeLog = Logging.timingStart(Level.FINE, LOG_PREFIX + " writeBuffer");
					}
					SocketChannel sc = (SocketChannel) key.channel();
					int write = 1;
					while (write > 0) {
						write = sc.write(bb);
						writtenBytes += write;
					}
					if (bb.remaining() == 0) {
						this.writeTimeLog.end(Integer.toString(bb.capacity()));
						this.writeTimeLog = null;
						this.outQueue.poll().getRight().end();
					} else {
						synchronized (this) {
							key.interestOps(key.interestOps()
									| SelectionKey.OP_WRITE);
						}
						break;
					}
				} else {
					synchronized (this) {
						key.interestOps(key.interestOps()
								& ~SelectionKey.OP_WRITE);
					}
					break;
				}
			}
		} else {
			synchronized (this) {
				key.interestOps(key.interestOps() & ~SelectionKey.OP_WRITE);
			}
		}
		tlog.end(Integer.toString(writtenBytes));
	}

	public void enqueue(byte[] buf, int count) throws IOException {
		TimingLogger tlog = Logging.timingStart(Level.FINER, LOG_PREFIX + " enqueue");
		byte b[] = new byte[this.sizeSize + count];
		int s = count;
		for (int i = Integer.SIZE - Byte.SIZE; i >= 0; i -= Byte.SIZE) {
			b[i / Byte.SIZE] = (byte) (s >> i);
		}
		System.arraycopy(buf, 0, b, this.sizeSize, count);
		synchronized (this) {
//			outQueue.add(Pair.of(ByteBuffer.wrap(b),Logging.timingStart(Level.FINE, LOG_PREFIX + " enqueueSize")));
//			outQueue.add(Pair.of(outb,Logging.timingStart(Level.FINE, LOG_PREFIX + " enqueueOutbuf")));
			outQueue.add(Pair.of(ByteBuffer.wrap(b),Logging.timingStart(Level.FINE, LOG_PREFIX + " enqueueOutbuf")));
		}
		tlog.end(Integer.toString(count));
		write();
	}
}