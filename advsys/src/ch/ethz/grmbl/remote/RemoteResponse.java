package ch.ethz.grmbl.remote;

import java.io.Serializable;
import java.util.UUID;

public class RemoteResponse<T> implements Serializable {

	private static final long serialVersionUID = 4576002149688316367L;

	private String id;
	private String requestId;
	private T response;
	private Throwable exception;

	public RemoteResponse(String requestId, T obj) {
		this.id = UUID.randomUUID().toString();
		this.requestId = requestId;
		if (obj instanceof Throwable) {
			exception = (Throwable) obj;
		} else {
			response = obj;
		}
	}

	public T getResponse() {
		return response;
	}

	public Throwable getException() {
		return exception;
	}

	public boolean isSuccess() {
		return exception == null;
	}
	
	public String getId() {
		return id;
	}
	public String getRequestId() {
		return requestId;
	}

}
