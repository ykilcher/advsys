package ch.ethz.grmbl.remote;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.UUID;

public class RemoteRequest implements Serializable {

	private static final long serialVersionUID = 7895022293499024853L;

	private String id;
	private String methodName;
	private Object[] args;
	private String[] parameterTypeNames;

	public RemoteRequest(Method method, Object[] args) {
		this.id = UUID.randomUUID().toString();
		this.methodName = method.getName();
		this.args = args;
		Class<?>[] pts = method.getParameterTypes();
		this.parameterTypeNames = new String[pts.length];
		for (int i = 0; i < pts.length; i++) {
			this.parameterTypeNames[i] = pts[i].getName();
		}
	}

	public Object[] getArgs() {
		return args;
	}
	
	public String[] getParameterTypeNames() {
		return parameterTypeNames;
	}

	public String getMethodName() {
		return methodName;
	}
	
	public String getId() {
		return id;
	}

}
