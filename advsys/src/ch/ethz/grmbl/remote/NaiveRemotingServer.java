package ch.ethz.grmbl.remote;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

import ch.ethz.grmbl.util.Logging;
import ch.ethz.grmbl.util.Logging.TimingLogger;

public class NaiveRemotingServer<T> {

	private ServerSocketChannel channel;
	private T impl;
	private boolean running = true;
	private Set<Thread> clients = new HashSet<Thread>();

	public void setup(int port, T impl) throws IOException {
		ServerSocketChannel channel = ServerSocketChannel.open();
		channel.socket().bind(new InetSocketAddress(port));
		this.channel = channel;
		this.impl = impl;
	}

	public void run() throws IOException {
		while (this.running) {
			final SocketChannel client = this.channel.accept();
			Thread ct = new Thread(new Runnable() {
				private static final String LOG_PREFIX = "SocketToClient";
				@Override
				public void run() {
					try {
						while (true) {
							TimingLogger tlincoming = Logging.timingStart(Level.FINER, LOG_PREFIX + " readStream");
							InputStream is = client.socket().getInputStream();
							int s = 0;
							for (int i = 0; i < Integer.SIZE / Byte.SIZE; i++) {
								s |= (is.read() << (Integer.SIZE / Byte.SIZE
										- 1 - i)
										* Byte.SIZE);
							}
							byte b[] = new byte[s];
							for (int i = 0; i < s; i++) {
								b[i] = (byte) is.read();
							}
							tlincoming.end();
							TimingLogger tlread = Logging.timingStart(Level.FINER, LOG_PREFIX + " deserializeRequest");
							RemoteRequest req;
							ObjectInputStream ois = null;
							try {ois = new ObjectInputStream(
									new ByteArrayInputStream(b));
								req = (RemoteRequest) ois.readObject();
							}finally{
								if(ois != null){
									ois.close();
								}
							}
							tlread.end();
							TimingLogger tlfind = Logging.timingStart(Level.FINER, LOG_PREFIX + " findMethod");
							Class<? extends Object> c = impl.getClass();
							String methodName = req.getMethodName();
										String[] ptns = req
												.getParameterTypeNames();
										Class<?>[] paramTypes = new Class<?>[ptns.length];
										for (int i = 0; i < ptns.length; i++) {
											paramTypes[i] = Class
													.forName(ptns[i]);
										}
							Object[] args = req.getArgs();
							Method method = c.getMethod(methodName, paramTypes);
							tlfind.end();
							TimingLogger tlinvoke = Logging.timingStart(Level.FINER, LOG_PREFIX + " invokeMethod");
							Object ret;
							try {
								ret = method.invoke(impl, args);
							} catch (InvocationTargetException e) {
								ret = e.getTargetException();
							}
							tlinvoke.end();
							TimingLogger tlwrite = Logging.timingStart(Level.FINER, LOG_PREFIX + " serializeResponse");
							RemoteResponse<?> resp = new RemoteResponse<Object>(req.getId(),ret);
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							ObjectOutputStream oos = null;
							try {oos = new ObjectOutputStream(
									baos);
								oos.writeObject(resp);
							}finally{
								if(oos != null){
									oos.close();
								}
							}
							tlwrite.end();
							TimingLogger tlout = Logging.timingStart(Level.FINER, LOG_PREFIX + " writeStream");
							s = baos.size();
							for (int i = 0; i < Integer.SIZE / Byte.SIZE; i++) {
								client.socket().getOutputStream()
										.write(s >> (i * Byte.SIZE));
							}
							baos.writeTo(client.socket().getOutputStream());
							tlout.end();
						}
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}catch (ClassNotFoundException e) {
						e.printStackTrace();
					}catch(IllegalArgumentException e){
						e.printStackTrace();
					}catch(IOException e){
						e.printStackTrace();
					} catch (NoSuchMethodException e) {
						e.printStackTrace();
					} catch (SecurityException e) {
						e.printStackTrace();
					}
					try {
						client.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});
			ct.start();
			clients.add(ct);

		}
		this.channel.close();
	}

	public void close() throws IOException {
		for (Thread t : this.clients) {
			t.interrupt();
		}
		this.running = false;
		channel.close();
	}
}
