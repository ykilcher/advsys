package ch.ethz.grmbl.remote;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;

import ch.ethz.grmbl.util.Config;
import ch.ethz.grmbl.util.DirectByteArrayOutputStream;
import ch.ethz.grmbl.util.Logging;
import ch.ethz.grmbl.util.Logging.TimingLogger;

public class RemotingServer<T> {

	private class DataCollector implements Runnable {
		private static final String LOG_PREFIX = "MessageTierDataCollector";
		private static final int logPause = 1000;

		@Override
		public void run() {
			try {
				while (running) {
					if (selector != null && selector.isOpen()) {
						int keys = selector.keys().size();
						Logging.status().log(Level.FINER,
								LOG_PREFIX + " keys " + keys);
					}
					if (pendingTasks != null) {
						int tasks = pendingTasks.size();
						Logging.status().log(Level.FINER,
								LOG_PREFIX + " pendingTasks " + tasks);
					}
					try {
						Thread.sleep(logPause);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			} catch (ClosedSelectorException ex) {
				// selector closed
			}

		}

	}

	private ServerSocketChannel channel;
	private T impl;
	private ExecutorService executor;
	private Selector selector;
	private boolean running = true;
	final Queue<Runnable> pendingTasks = new LinkedList<Runnable>();
	private DataCollector dataCollector = null;

	public void setup(int port, T impl) throws IOException {
		ServerSocketChannel channel = ServerSocketChannel.open();
		channel.configureBlocking(false);
		channel.socket().bind(new InetSocketAddress(port));
		this.channel = channel;
		this.impl = impl;
//		ExecutorService tp = Executors.newCachedThreadPool();
		System.out.println("allocating " + Config.get().nThreadsInPool + " threads in pool");
		ExecutorService tp = Executors.newFixedThreadPool(Config.get().nThreadsInPool);
		this.executor = tp;
		this.dataCollector = new DataCollector();
	}

	public void run() throws IOException {
		Thread dct = new Thread(this.dataCollector);
		dct.setDaemon(true);
		dct.start();
		selector = Selector.open();
		channel.register(selector, SelectionKey.OP_ACCEPT);
		while (this.running) {
			for (Runnable task : pendingTasks) {
				executor.execute(task);
			}
			pendingTasks.clear();
			int rdyChannels = selector.select();
			if (rdyChannels == 0) {
				continue;
			}

			Set<SelectionKey> selectedKeys = selector.selectedKeys();

			Iterator<SelectionKey> keyIterator = selectedKeys.iterator();

			while (keyIterator.hasNext()) {
				SelectionKey key = keyIterator.next();
				if ((key.interestOps() & SelectionKey.OP_ACCEPT) != 0
						&& key.isAcceptable()) {
					ServerSocketChannel c = (ServerSocketChannel) key.channel();
					SocketChannel client = c.accept();
					client.configureBlocking(false);
					ChannelBuffer buf = new ChannelBuffer() {
						private static final String LOG_PREFIX = "ChannelBuffer";

						@Override
						protected void doneReading(final SelectionKey key,
								final ByteBuffer inBuf) {
							TimingLogger tl = Logging.timingStart(Level.FINER,
									LOG_PREFIX + " doneReadingStream");
							final ChannelBuffer buf = this;
//							inBuf.rewind();
							pendingTasks.add(new Runnable() {
								private static final String LOG_PREFIX = "PendingTask";
								
								private TimingLogger inQueueLogger = Logging.timingStart(Level.FINE, LOG_PREFIX + " inQueue");

								@Override
								public void run() {
									this.inQueueLogger.end();
									TimingLogger tl = Logging.timingStart(
											Level.FINER, LOG_PREFIX + " run");
									try {
										TimingLogger tlread = Logging
												.timingStart(
														Level.FINER,
														LOG_PREFIX
																+ " deserializeRequest");
										RemoteRequest req;
										ObjectInputStream ois = null;
										try {
											ois = new ObjectInputStream(
													new ByteArrayInputStream(
															inBuf.array(),0,inBuf.limit()));
											req = (RemoteRequest) ois
													.readObject();
										} finally {
											if (ois != null) {
												ois.close();
											}
										}
										tlread.end();
										TimingLogger tlfindmethod = Logging
												.timingStart(Level.FINER,
														LOG_PREFIX
																+ " findMethod " + req.getMethodName());
										Class<? extends Object> c = impl
												.getClass();
										String methodName = req.getMethodName();
										String[] ptns = req
												.getParameterTypeNames();
										Class<?>[] paramTypes = new Class<?>[ptns.length];
										for (int i = 0; i < ptns.length; i++) {
											paramTypes[i] = Class
													.forName(ptns[i]);
										}
										Object[] args = req.getArgs();
										Method method = c.getMethod(methodName,
												paramTypes);
										tlfindmethod.end();
										TimingLogger tlinvoke = Logging
												.timingStart(
														Level.FINER,
														LOG_PREFIX
																+ " invokeMethod " + req.getMethodName());
										Object ret;
										try {
											ret = method.invoke(impl, args);
										} catch (InvocationTargetException e) {
//											Logging.error().log(Level.SEVERE,"InvocationTargetException",e.getCause());
											ret = e.getTargetException();
										}
										tlinvoke.end();
										TimingLogger tlwrite = Logging
												.timingStart(
														Level.FINER,
														LOG_PREFIX
																+ " serializeResponse " + req.getMethodName());
										RemoteResponse<?> resp = new RemoteResponse<Object>(
												req.getId(), ret);
										DirectByteArrayOutputStream baos = new DirectByteArrayOutputStream();
										ObjectOutputStream oos = null;
										try {
											oos = new ObjectOutputStream(baos);
											oos.writeObject(resp);
										} finally {
											if (oos != null) {
												oos.close();
											}
										}
//										ByteBuffer outb = ByteBuffer.wrap(baos
//												.toByteArray());
//										outb.rewind();
										tlwrite.end();
										buf.enqueue(baos.getBuf(),baos.getCount());
									} catch (IllegalAccessException e) {
										e.printStackTrace();
									} catch (IllegalArgumentException e) {
										e.printStackTrace();
									} catch (ClassNotFoundException e) {
										e.printStackTrace();
									} catch (IOException e) {
										e.printStackTrace();
									} catch (NoSuchMethodException e) {
										e.printStackTrace();
									} catch (SecurityException e) {
										e.printStackTrace();
									}
									tl.end();
								}
							});
							tl.end();
						}
					};
					SelectionKey selKey = client.register(selector,
							SelectionKey.OP_READ, buf);
					buf.key = selKey;
				} else if ((key.interestOps() & SelectionKey.OP_READ) != 0
						&& key.isReadable()) {
					ChannelBuffer buf = (ChannelBuffer) key.attachment();
					buf.read();
				} else if ((key.interestOps() & SelectionKey.OP_WRITE) != 0
						&& key.isWritable()) {
					ChannelBuffer buf = (ChannelBuffer) key.attachment();
					buf.write();
				}

				keyIterator.remove();
			}
		}
		Set<SelectionKey> keys = this.selector.keys();
		for (SelectionKey k : keys) {
			k.channel().close();
		}
		this.channel.close();
		this.executor.shutdown();
	}

	public void close() throws IOException {
		this.running = false;
		if (this.selector != null) {
			selector.wakeup();
		}
	}
}
