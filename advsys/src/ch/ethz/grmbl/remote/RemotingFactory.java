package ch.ethz.grmbl.remote;

import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.logging.Level;
import java.util.logging.Logger;

import ch.ethz.grmbl.exception.ConnectionClosedException;
import ch.ethz.grmbl.util.Logging;
import ch.ethz.grmbl.util.Logging.TimingLogger;

public class RemotingFactory {

	private static final Logger log = Logger.getLogger(RemotingFactory.class
			.getName());

	public static <T> T createRemotingClient(Class<T> c, String address,
			int port) throws IOException {
		return createRemotingClient(c, address, port, new RemotingClient());
	}

	@SuppressWarnings("unchecked")
	public static <T> T createRemotingClient(Class<T> c, String address,
			int port, final RemotingClient client) throws IOException {
		log.fine("creating remoting client for " + c.getName() + " at "
				+ address + ":" + port);
		client.connect(address, port);
		return (T) Proxy.newProxyInstance(c.getClassLoader(),
				new Class<?>[] { c }, new InvocationHandler() {
					private static final String LOG_PREFIX = "RemotingProxy";

					@Override
					public Object invoke(Object proxy, Method method,
							Object[] args) throws Throwable {
						TimingLogger tl = Logging.timingStart(Level.FINER,
								LOG_PREFIX + " invoke " + method.getName());
						if (method.getName().equals("close")) {
							// TODO
						}
						log.finer("invoking method " + method.getName()
								+ " on proxy");
						RemoteRequest req = new RemoteRequest(method, args);
						RemoteResponse<?> resp;
						try {
							resp = client.doRequest(req);
							// } catch (IOException e) {
							// if(e.getMessage().equals("Broken pipe") ||
							// e.getMessage().equals("Connection reset by peer")){
							// throw new ConnectionClosedException();
							// }
							// throw e;
							// }catch(NegativeArraySizeException e){
							// throw new ConnectionClosedException();
							// }catch(ArrayIndexOutOfBoundsException e){
							// throw new ConnectionClosedException();
						} catch (Exception e) {
							Logging.error().log(Level.FINE,
									LOG_PREFIX + " connectionClosed", e);
							throw new ConnectionClosedException();
						}
						tl.end(resp.isSuccess()?"success":"fail");
						if (resp.isSuccess()) {
							return resp.getResponse();
						}
						// Logging.error().log(Level.SEVERE,LOG_PREFIX +
						// " Error while invoking remotely",resp.getException());
						throw resp.getException();
					}
				});
	}

	public static <T> NaiveRemotingServer<T> createNaiveRemotingServer(T impl,
			int port) throws IOException {
		return createNaiveRemotingServer(impl, port,
				new NaiveRemotingServer<T>());
	}

	public static <T> NaiveRemotingServer<T> createNaiveRemotingServer(T impl,
			int port, NaiveRemotingServer<T> server) throws IOException {
		server.setup(port, impl);
		return server;
	}

	public static <T> RemotingServer<T> createRemotingServer(T impl, int port)
			throws IOException {
		return createRemotingServer(impl, port, new RemotingServer<T>());
	}

	public static <T> RemotingServer<T> createRemotingServer(T impl, int port,
			RemotingServer<T> server) throws IOException {
		server.setup(port, impl);
		return server;
	}

}
