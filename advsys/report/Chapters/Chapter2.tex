\chapter{Experimental analysis}

In this chapter, we will describe how we staged, planned and performed our experiments and we will show the results we obtained from these.

\section{Experimentation setup}

By far the largest part of this project was about getting the testing setup right and finding good parameters to test.
During the short period of this project, we have set about extracting as much \emph{useful} information about our system as possible.
In the following, we will describe in detail how we have planned and setup our testing environment.

\section{Sets of tests}
During our work on this project, we have designed and conducted a number of experiments. Because we were relatively clueless about how we should configure and parametrize the testing parameters, we simply started with some values and then explored our possibilities.
Up until the day of the milestone presentation, we were constantly working with a couple of hundred clients connected to our system, which gave us some good results, but also some less understandable ones.
But on the milestone presentation (yesterday, at the time of this writing), we noticed that all other groups had done their testing with significantly less clients than we did and so we performed all our experiments again over night with only a couple of dozen clients and got much more understandable numbers out of this:
Our response time went down, throughput went up and the behaviour was much more predictable, which is why we must assume that for some parts, we might have saturated some capacity of the system or infrastructure on our first set of tests.

\subsection{Hardware}

We have done our initial testing on the provided dryad cluster, but we soon reached its limitations:
On one hand, it was only available every so often and on the other hand, we have discovered that four machines are not enough to accurately test our system.
It is for these reasons that we have moved to Amazon's EC2, for which we were thankfully provided a voucher.
For our testing, we used nine medium tier general-purpose AMIs with Amazon Linux (2 ECUs, 1 vCPU, 3.7GiB memory, moderate network performance), one of which, for high load testing, served as host for the database, two served as middleware servers and the remaining six were dedicated client tiers.
For low load testing we used one machine for data tier, one machine as middleware and three machines for running clients.
On each of the client tiers, we were able to run multiple processes, each running multiple threads of which each one simulated the behaviour of a single client.

\subsection{The use of Jython}

For specifying the behaviour of the simulated clients, we used the python language, which greatly simplified many things.
We also used python for all of the evaluation and analysis, as well as some other small tasks, but specifically for simulating clients, we used a special python implementation, called \emph{jython}, which is compiled to bytecode at runtime and runs on the JVM.
Jython code is therefore capable of interacting with all other constructs living in the JVM, for example all Java classes, but also has access to all of the python standard library as well as any add-on packages.
And since it is compiled to bytecode, it does not suffer the performance drawbacks of an interpreted language.
We have conducted some very simple performance testing in the form of unit tests to ensure that our system suffers no performance decrease when running on python.
We have let a jython client and an equivalent java client perform a number of operations on our system, and it turned out that the jython client's runtime was around 1.3 to 1.4 to that of the java client, so we can use jython with good consience for our experiments.
Since we programmed our system to be a library on the client side, we used jython to make use of that library and infuse our system with life.
We first specified the concept of a \emph{node}, which represents a single agent (e.g. a client, middleware or data tier) in our system.
A node can be given a predefined behaviour, it can be started and stopped and it has an internal timer to coordinate its different behaviours.
During experimentation, a node will be distributed to a specific \emph{machine}, but this is decided at runtime.
While coding the tests, a node is independent of the machine it is going to run on and its specified behaviour should only concern its interaction with other nodes.
A node will, besides running a part of our system, also collect all logs and also create hardware statistics, such as CPU utilization or memory consumption.
We subclassed the node to yield \emph{ClientTierNode}, \emph{MessageTierNode} and \emph{DataTierNode}.

A ClientTierNode represents a client using our system. It will connect to a middleware (here also called \emph{message tier}), which it is assigned at runtime, at the beginning of the experiment by calling a factory method to obtain an instance of \emph{ClientAPI}.
This, as well as the instances returned from calls to it, can then be used to interact with the system.
The exact behaviour of the clients during the system is defined by further subclasses.

A MessageTierNode represents a middleware server.
The rather minimal role of our scripts here is simply to start up a server instance, also via calling a factory method.
The server's behaviour is already fully defined in the Java code:
Upon starting, it will create a pool of database connections, a pool of threads for handling requests and it will open its listening sockets for clients to register themselves with the system.
There is, however, in every experiment one special MessageTierNode (since there can be multiple), the job of which is also to fill the database with queues and messages needed for the following experiments.

DataTierNode is a bit of an exception, since it has only a passive part in the system.
The active part of the data tier consists of the database server itself, to which the middleware connects directly via the JDBC driver.
The DataTierNode jython script will simply be staged on the data tier machine in order to collect logs as well as hardware statistics for us to analyse later.
It will also periodically query the database for status updates, such as the number of messages currently held in the system.

\subsection{Types of clients}
\label{ClientTypes}

As previously said, we created different types of clients by subclassing ClientTierNode, each having its own behaviour of sending, receiving and querying for messages.
By creating these different clients, we were able to test the different functionalities as well as cover the various use cases of our system.
In the end, we had created the following types:

\emph{SendingClient} and \emph{ReceivingClient} are the most basic clients, as one of them only sends an the other one only receives. We ultimately did not use these for testing, as they do not keep the number of messages present in the system at a constant level and can therefore not be measured properly.

\emph{OneWayClient} is a client, which randomly sends a message to another OneWayClient, which, upon receiving the message, increments a counter on the message and sends it back.
By having such counters in the message bodies, we are able to keep track of how many times the messages are passed around and also we can check our system for correctness and stability, as if our system were not stable and clients would crash, we would expect there to be messages (the ones from and to that client), which are essentially being stopped in the system and would therefore be passed around a significantly less amount than all the other messages of the same type of client. (We do, of course, expect messages of different client types to be passed around a different amount of time, as their behaviour is different.)

\emph{RequestingClient} and \emph{RespondingClient} act as a pair.
Each of them has the other one as a dedicated partner in the system.
At first, RequestingClient will start sending a message to RespondingClient, which will then increment the counter on the message and send it back.
After that, the two clients will send back and forth the same message between themselves, each time incrementing the counter on it.

\emph{ServiceProvidingClient} and \emph{ServiceConsumingClient} represent a client-server architecture.
ServiceConsumingClient will send messages ("requests") to a dedicated set of queues in the system.
These queues are listened to by all ServiceProvidingClients.
One of them - the first one to read the message - will remove it from the queue, increment its counter and respond via a second set of queues, to which all ServiceConsumingClients listen.
Other than the requests, these responses are directly addressed to the ServiceConsumingClient that originally sent it and only the original sender can retrieve a corresponding response from a queue.
After receiving a response, the ServiceConsumingClient will, upon incrementing the counter, send the message again to the queues where the ServiceProvidingClients are listening.
This way, the message goes back and forth, altering between ServiceProvidingClients and ServiceConsumingClients.

Note that all of the clients (except SendingClient) are, whenever they are not sending a message or are in their \emph{thinking pause}, which happens basically after every action done, they are polling the system for new messages. We expect these polls to make up a large part of the request volume to the system.

\subsection{Automation}

We have built an extensive system of automation for distributing and running our experiments, as well as collecting all log files and shutting down the whole system after running the experiments.
All of the automation is implemented using python/jython scripts, which coordinate carefully.
As a preparation step, all Java classes - the classes of our system as well as all of the postgresql jdbc driver and the jython library - are packed into a single jar via an ant script.
This jar, as well as the tar'd postgresql database binaries, which we have compiled once for each test environment, are distributed at the very beginning of a whole test and cleaned up only after running all tests in a test set.
By not copying these files around for every test we save ourselves a lot of time.
We also keep the database running over a whole test set.
We have read some opinions online as well as talked to other students, some of which indicated that postgresql can have a significant warmup time of multiple minutes.
We did not do any steps to verify or counteract this, but avoided it altogether by simply keeping the database running for the whole test set and running a ten minute warmup dummy test case in the beginning of each test set.
In the end, after analysing the logs of these warmup tests, we discovered that the warmup time for the database was much shorter than the warmup time of our system (around 200 seconds at most), which rendered the warmup tests unnecessary.

Creating tests on our system can be quite specific.
The experimenter has to pass a list of node instances, each of which describing the behaviour of a single node (e.g. a client), to the test runner, which will then distribute and run them.
Note that we do not simply pass parameters to the remote test runners, but whole \emph{objects} in serialized form.
The remote test runner will then have to load the passed instances from their serialized form and simply run each of them at the desired starting time.
This method guarantees the most flexibility when testing.
So, in theory, one would need to specifically instantiate each node of the experiment in the central script and then pass them on to the remote locations. Luckily, we have also created scripts, which relieve us of this work and instantiate a number of nodes with a desired set of parameters.

On top of that, we have created yet other functions, which create whole \emph{sets} of tests.
One can simply give the function rages of parameters and it will combinatorially iterate through combinations of these and create a single test case for each possible configuration. This way, a lot of test cases can be created in a very simple way.

So far, we have seen how the creation of tests and the contained nodes works, but how do these nodes find their way to the remote machines on which they should be run?
For this, the experimenter can specify, separate from the definition of the nodes, which machines are available for the different tiers.
There can be one data tier machine, to which the database will be deployed.
Each middleware machine will receive one middleware node, since we always want to have dedicated machines for the middlewares.
The client machines are obviously going to be fewer in numbers than the number of client nodes.
The central test runner will allocate each client machine a random subset of all client nodes, such that the client nodes are distributed equally on the client tier machines.
Note that a \emph{machine} is specified by a tuple of host, user and folder on host.
This enables us to host multiple logical machines on a single physical machine (run in different processes), which was especially useful for local testing and again makes the testing more flexible.

After the nodes for a single test have been distributed, the next task is to schedule the starting of the test.
This should happen at roughly the same time on all locations, which is why we should try to synchronize the clocks of the different machines as much as possible. Note that we will never use these clocks in comparison to each other when calculating response times and such measures, but a rough synchronization is very useful for starting and stopping tests.
For this, we record the time when we copy the serialized node instances from the central test runner to the remote machine. Specifically, we record the time when the scp command returns, which should happen with a very short delay after the remote file has been written.
We then issue a second ssh command to the remote test runner to pass it the recorded time.
The remote test runner will then compare the passed in time with the modification time of the created file and adjusts its internal scheduling clock to the time difference it calculated from this process.
This enables us to instruct all remote test runners to start at a specific point in time.

After a test has been run, the remote test runners shut down automatically, but just to be sure, a couple of seconds after this should have happened, we instruct a kill script to kill the remote processes (we recorded their PIDs when we started them).
This way, we will not have any overlap between tests.

After a test is done, the last task before cleaning up is to collect the logs.
We produce an immense amount of logs using the java.logging package, which is why we compress them first on the remote machines before scp'ing them to the local machine, where they are extracted again and put into a folder with a combined name made from the timestamp and the test name.
Logs of our system are collected and cleaned up after every test, otherwise we would easily overflow the disk limits on both the dryad cluster and EC2.
It also enables us to continuously analyse the collected logs, which takes quite a while, while other tests are still running.

\subsection{Logging}

For logging, we have used the java.util.logging package where we attached file handlers to redirect all logging traffic to disk.
We have divided our logging between different logging namespaces, each of which serves a specific purpose.

The \emph{timing} namespace is by far the one with the most logging volume.
Here, we log the timing of each important method call throughout the system.
We take the time at the entry into a method and at the end of the method, we calculate the difference to the recorded time and log the difference as the time it took for completing this method.
In this way, we have logged all methods on the client API, the middleware and the data tier to get a complete understanding of what is happening in our system.
With this method, we can also log the time a request spent waiting in some specific queue to be processed and from all the information collected from these timing logs we can reconstruct the average times a request spent in each of the components of our system, as well as the time it spent on the database or the network.

The \emph{status} namespace is being filled with periodic updates about the status of our system. These include all hardware stats, like CPU utilization or memory usage, as well as database stats, like how many messages there are currently in the system and also thinks like how many pending tasks are currently in the queues on the middleware.

The \emph{trace} namespace captures all entries that are generated by our client nodes whenever they send/receive a message. They will log the message id together with the counter, which shows how many times this message has been passed around so far.
By using these logs, we can reconstruct how messages were passed in our system and we can ensure that no message has been left sitting in our system because, for example, its intended receiver crashed.

Finally, the \emph{error} namespace captures all errors generated by our system, which, luckily, we did not have too many during the course of this project.

\subsection{Evaluation}

We have staged the evaluation of our collected logs in two parts.
First, we have a script for analysing the individual experiments.
As the logs of these experiments can be quite immense (~16GB for the long trace), so this process takes a lot of time.
We also had to be careful when writing the script as to not use up too much memory.
Even though swapping is theoretically a nice extension of RAM, in practice, it is not desirable as it slows down computation extremely and also kills our disks.
In python, this can be done by manually deleting unused objects and calling the garbage collector manually, as well as going through the files multiple times for collecting different sets of data.
Thus, we had to engineer the statistics we wanted to calculate in such a way, that they can be divided into independent parts, each of which is then dealt with a separate run through the files.
All together, analysing a single experiment of 400 seconds takes a couple of minutes, which is why we have scripts that watch directories for new logs that are being collected from experiments and then directly start analysing them.

In the second step, we have another script that uses the statistics from the single experiments to compare these with each other. Here, we can calculate a multitude of plots and statistics, including the necessary calculations for the $2^k$ factorial analysis or the dependencies of throughput and response time with regard to a varied parameter.

\subsection{Tested parameters}

We have conducted a number of tests to test the influence of different parameters and their dependencies on the system. For this, we have on the one hand designed experiments where we varied a single parameter at a time in order to see how the system responds to different configurations and on the other hand we have conducted experiments where we varied two parameters in a classical $2^k$ (here $k=2$) setting in order to determine the dependencies between parameters.
Specific parameters we were looking at included the number of threads on the middleware, the number of database connections for each middleware, the number of different types of clients, the number of queues with respect to the number of clients, the think time of the clients, the message size and the ratio of consumers vs. producers in the client-server setting.

The detailed experiments as well as their result will be explained in the next section.

