\chapter{System design and implementation}

\section{Overview}

The goal of this project was to design a message queuing system as well as measuring and modelling it. For the first milestone, only the first two points have to be finished and discussed. In this chapter, we will have a look the general design as well as the implementation of the system.

The system can be roughly divided into 3 separate layers. The \emph{client tier} which basically just consists of an API to access the system over the network, a middleware (we also call this \emph{message tier}), which handles requests from several clients and sends them to a single database via a provided interface as well as sending back the responses to the clients, and lastly a data tier, which provides that interface to the database and handles the connections to it. There is also the database itself, which provides consistent data storage as well as stored procedures which are the heart of the queuing system logic.

An overview of the system can be seen in figure \ref{fig:design}.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/design.png}
\caption{An overview of our system with the middleware tier being highlighted}
\label{fig:design}
\end{figure}


%The environment in which we'll run our system will include a single database while there are several middlewares which handle requests from multiple clients each. The system will be able to send messages over the network between each layer. Therefore it can be distributed to several different locations and will also be able to handle requests from different computers. Thus the system provides a simple messaging system for inter process communication over the internet while assuring consistent data.

\section{General design principles}

Throughout this project, we have tried to build an object-oriented, robust and user-friendly system that could actually be used in practice, rather than a bunch of spaghetti code simply to complete the course.
This means that we have traded in some places a bit of performance for more usability, like our use of 128-Bit UUIDs instead of integers as message keys.
With this approach, we expected to save us some trouble later on when fixing bugs as well as when modeling the system.
In the following few section, we will describe how we have implemented each of the layers of our system.

\section{Client Tier}

The client tier is the top level of our system.
It provides an API as a library, through which our system can be accessed and it also handles the implementation of the networking in order to connect to the system.

\subsection{The Client API}

The \emph{ClientAPI} java interface provides a number of functionalities for using our system.
From here, a user (by user here we mean program using the library) can retrieve queues by id, query for queues where messages are waiting, query for other clients and much more.
As we have tried to build an object-oriented system, the return values of calls to this interface are again objects, rather than strings or integers.
In particular, through querying, one can retrieve one or multiple instances of \emph{ClientQueue}, which are described next.

\subsection{Queues}

The \emph{ClientQueue} interface represents a queue in the system. Through the interface, a user can peek or pop the queue or send new messages to the queue.
Whenever a queue is successfully read, either through peek or pop, a \emph{ClientMessage} object is returned.

\subsection{Messages}

The \emph{ClientMessage} interface represents a message in the system. The interface provides access to the specific fields of the message, like its text content, context or senderId.
A user can also reply to the message by calling the message's \emph{reply} method.

\section{Networking}

For the client tier to communicate with the middleware, a network connection is needed.
In this section, we will describe how we implemented the networking that makes the communication work.

\subsection{Transparent abstraction}

The goal of our networking imlementation was complete transparency to the caller.
Thus, the code for interacting with specific classes should be exactly the same whether these classes run on the local machine or on a remote machine.
We achieve this by abstracting the relevant classes to \emph{interfaces}.
When running locally, for example for testing, the implementation of the interface is simply the relevant class itself.
But when communicating over the network, the implementation of the interface is handeled by a \emph{DynamicProxy}, a class from java.lang.reflect.
DynamicProxy implements an interface without the programmer having to implement every single method of it.
Instead, only one method has to be implemented, which is called whenever any interface method is called, and which is given the called method name as well as the list of parameters as arguments.
These things are then serialized, sent over the network to the remote implementation, where the real method is called.
The response is then serialized again, sent back, deserialized and is given to the user as return value, just as if the call was made locally.
We have abstracted this principle in the \emph{RemotingClient} and \emph{RemotingServer} classes, such that it could be used for any application, not just our own.

\subsection{Serialization}

We have used java serialization to read and write our objects from and to the network.
For this, we have packed the called method name as well as a list of parameters into a \emph{RemoteRequest} class and serialized it to a byte array using java's \emph{ObjectOutputStream}.
On the receiving side, the result is deserialized, the method is executed as described above and the result or any thrown exception is packed into a \emph{RemoteResponse} class and shipped back to the caller.
On the caller side, the response is again read and if it contains an exception, the exception is throws, otherwise the contained result is returned.
This provides a simple way to deal with errors and is also a component of transparency, as the exceptions thrown on the server are simply propagated to the clients.

\subsection{The usage of NIO}

While on the client side, we use traditional blocking sockets, we have decided to use java's non-blocking I/O (which uses the underlying operating system's non-blocking sockets) for our networking implementation on the server side.
This brings the advantage, that threads will not be blocked by waiting for data from the network to pour in, but instead we have a single I/O thread, which calls \emph{select} onto all registered socket descriptors and reads from the ones where data can be read and writes to the ones where there is data available to write to them without ever being blocked.
Whenever a request has been fully read, it hands it off to a task queue, where a thread pool is available for executing the task.
Upon finishing the task, the thread from the thread pool will put the response into another queue where the I/O thread can now write the response back onto the network.

As good as the usage of NIO sounds, it has some traps one has to pay attention to.
First, there is never any guarantee as to how many bytes can be read/written from or to a socket.
This means two things: One, that we can never be sure when a request has been fully read and two, that we somehow have to buffer the incoming requests until they have been received completely in order to deserialize them.
We solved the first problem in a relatively easy way: We simply dedicated four bytes at the beginning of each request to denote the size of the following request.
This way, a receiver can be sure when the whole request has been read.
The second problem requires a bit more engineering, as there is a second problem of NIO attached to it:
It does not play well with java serialization.
\emph{ObjectInputStream} and \emph{ObjectOutputStream} require underlying blocking streams, which is in obvious conflict with our use of NIO.
Thus, we could not simply attach the ObjectStreams to our sockets, but we had to attach them to a simple \emph{ByteArrayOutputStream}, which simply writes the data to a byte array.
Because of the inefficient implementation of ByteArrayOutputStream - it copies the buffer each time one tries to access it - we subclassed it to yield a faster version with direct access to the buffer.
In this version, it was also possible for us to fill the buffer not from the beginning, such as to reserve the necessary space for our length header without ever having to copy a buffer.
These byte arrays were then wrapped in NIO's \emph{ByteBuffer} classes, which are a very low-level construct and allow precise manipulation of the read and write operations.
Using these ByteBuffers, we were able to bridge java serialization with NIO and thus achieve our goals.
Through all that, we tried to closely watch performance, as possible reuse the bytebuffers instead of allocating them anew and doing as little copying as possible and in the end, it worked surprisingly well.

\section{Middleware Tier}

The middleware tiers are central points where clients can connect to.
There is not much business logic being done here, the code is dominated by networking, handling of the threads and handling of the database connections.
Given a request, the middleware will simply propagate it to the database and then serve the response back to the requesting client.
Since there can be many clients per middleware, an effort has to be made to efficiently schedule the incoming requests.
For this, we used a thread pool toghether with a task queue.
Incoming requests are simply put onto the task queue and as soon as a thread from the pool is available, it will try to fulfil the request, most of the time by grabbing a database connection from the database connection pool and running some stored procedure.
Concretely, we used an \emph{Executor} from the jdk, which already implements all of this fairly nicely.

\section{Data Tier}

The data tier is where all necessary of the data about the clients, queues and messages stored. It consists of a Postgresql database which uses stored procedures, a Java interface as well as an according Java wrapper which implements the interface and connects to the database via connection pools. Since we have two different database schemas, which are determined by two separate ways a message, which gets sent to multiple queues, should get handled, we also provide two different Java wrappers to the middleware.

\subsection{ER-Schemas}

%2 version of the schema. one with the 1 queueid per message and one with n queueids per message
In the planning phase of the project, one of the crucial points was the schema of the database. While most of the entities, relations and attributes were straight forward, given the project description, there was one point which left quite a bit of interpretable space. 

It was not specified whether a single message, which gets sent to multiple queues, has to be popped from each queue individually to be completely removed from the database or from just one queue. We decided to implement both versions of the database because it has quite a different semantic but we still planned to compare both implementations in our measurements. As seen in figure \ref{fig:ER} the ER-diagram looks alike in both version except for the cardinality of the relation between message and queue. This however has major consequences on the implementation of the database-relations as well as the stored procedures. We will discuss the pros and cons in the following chapters in more detail.

\begin{figure}[p]
\centering
\includegraphics[width=\textwidth]{imgs/ER.png}
\caption{ER diagram of both our database designs, the only difference is the part in red/blue}
\label{fig:ER}
\end{figure}

\subsection{Database implementation}

We have two different database schemas for the two different semantics of a single message which gets sent to multiple queues. As shown in figure \ref{fig:schema} the general layout is the same except for the fact that the entry queueId in one of the schemas got replaced by a relation queueId messageId which is stored in a separate table. This allows us to have the same message put into several queues without copying the same message over and over again.

\begin{figure}[p]
\centering
\includegraphics[width=\textwidth]{imgs/schema.png}
\caption{Functionality provided by the data tier, version 1 on top, version 2 on bottom}
\label{fig:schema}
\end{figure}

Another rather unique decision is the fact, that we use 128-Bit unique universal identifiers (UUIDs) as primary keys because it removes the necessity to check for key collisions and we are able to calculate them already in the middleware or client layer. In fact, is an actually necessary to calculate them ahead of time since Postgres supports UUID as a datatype but it cannot generate any in the database itself without the usage of additional programs. As it turned out though, this does not have any major impact on the time needed to send packages over the network. This can be seen in our experiments regarding the message length where we see that increasing the message length by a small amount will not have a large impact on network performance and since a UUID is only 12 bytes more than an integer, we don't expect it to have a large impact. We also don't expect it to have a large impact on database performance, but we have not done any experiments to see how an indexed UUID key is different in performance from an indexed integer key.

The implementation in the database was more or less straight forward. We used schemas to be able to have both version of the database ready at any given time and used b-tree indices to speed up access times on the different relations. To implement the stored procedures, we used PL/pgSQL. The stored procedures are quite slick because they either check for some conditions and throw exceptions if they are not met or just run some standard SQL queries like select or insert.

There are a few differences in the stored procedures depending on the chosen schema. In the first case (as shown in figure \ref{fig:schema}) the sql statements are quite easy since there are no joins required at all whereas for the second schema (Figure \ref{fig:schema}) we have quite a few joins, mostly between the \emph{message} and the \emph{isIn} relation. On the other hand, the \emph{sendMessage} in the first schema requires n UUIDs for the n entries in message as soon as a message gets inserted into n queues whereas in the second schema, only one UUID for the message id is required since its the same for all message-queue pairs.


\subsection{Data Tier Wrappers}

The data tier interface provides a simple API to the middleware layer. There are functions to manipulate as well as analyse data in the database. The interface gets implemented with simple wrapper classes which handle the connection pool to the database as well as the translation from simple Java functions to the stored procedures.

We have implemented Java wrappers for the data tier, one for each database schema. They are the same except for the \emph{sendMessage} API functions since the respective stored procedures have different headers.

The wrapper creates a connection pool to the database as soon as it gets instantiated by an according middleware implementation. It is based on the class \emph{PGPoolingDataSource} which is provided by the Postgres JDBC driver. The maximum number of simultaneous connections as well as general login credentials and the location of the database itself can be manipulated via a simple configuration class. Since the close operation on a connection which is grabbed from a pool is overwritten, it doesn't close the actual connection to the database and thus there is no need to reinitialise the connection to the database again. The execution plan for a stored procedure in the database also gets calculated as soon as a connection to the database gets opened and since we don't reopen the connections as soon as everything is up and running, we can use the advantages of stored procedures and gain performance.

To access the stored procedures on the database, we use prepared statements. As these prepared statements are cached, the JDBC driver has less statement execution planning to do as soon as all the statements got initialised and can send the data more efficiently to the database after some warm-up time.

\subsection{Behaviour of the second database schema}

We did not end up including the second database schema, which has the additional relation \emph{isIn}, into our experiments, because it performed really bad compared to the first schema in our initial tests. If we had more time we would have improved and tested it as well but running the tests for one database schema is already quite some work.
