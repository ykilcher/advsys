\chapter{Results and Discussion}

\section{Overview}
In this chapter we will discuss the results of our experiments. First, we will talk about the trace, because it shows that the system runs stable. If that was not the case, the other experiments would not return any meaningful data. Afterwards we will highlight specific factor combinations and discuss all the experiments related to them.

\section{Two hour trace}

First off, we wanted to make sure that our system runs stable and thus we ran two hour traces to check for abnormal behaviour, such as increasing response time and decreasing throughput. Furthermore we checked if there are any cyclic spikes in either of the results which can be related to garbage collectors and similar processes.

The trace was run using 20 pairs of request-response clients, 50 one-way clients, 2 services with 10 consumers and 5 providers each, 10 queues per type of client, 8 threads per pool, 8 database connections per middleware, a think time of 0.01 seconds and a message size of 5 bytes (the word "hello"). The test ran successfully for little over two hours and figure \ref{fig:traceresptimes} shows the response time on the clients over time. Note that we had to do this graph by hand, as the logs were so huge, our scripts, even though optimized for this, crashed.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/traceresptimes.png}
\caption{Client response times of the trace, mean is around 60ms}
\label{fig:traceresptimes}
\end{figure}

Our results (as seen in \ref{fig:traceresptimes}) show constant behaviour over the full two hours excluding the warm up and cool down time.
Figure \ref{fig:tracenmsgs} shows that the number of messages in our system stays at the same level and a consultation at the tracing logs also shows that only very few messages got stuck in the system, making us confident in our implementation.
Figure \ref{fig:tracerparts} shows, from bottom to top, how these 60ms are spent as the request goes through our system.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/tracenmsgs.png}
\caption{Number of messages in our system during the trace}
\label{fig:tracenmsgs}
\end{figure}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/tracerparts.png}
\caption{A request goes through our system from bottom to top}
\label{fig:tracerparts}
\end{figure}

As we looked closer there was indeed a periodically appearing spike in response time, which we could reproduce in shorter experiments. As it turned out, the main reason for these spikes are the postgres database which runs autovacuums and autocheckpoints every so often.
This is further verified by the fact that we see spikes in the WAIT section of the data tier's CPU statistics, as seen in figure \ref{fig:dtcpu}, but at the same time we were observing peaks in IDLE on the client machines, as seen in figure \ref{fig:ctcpu}.
This means that something is going on on the database, which gives a spike in system performance periodically.
This was very interesting to us, but it indicates normal behaviour, so there is nothing we can do about it.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/dtcpu.png}
\caption{Sample CPU usage on the data tier. Note the blue spikes.}
\label{fig:dtcpu}
\end{figure}
\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/ctcpu.png}
\caption{Sample CPU usage on the client tier. Note the red spikes.}
\label{fig:ctcpu}
\end{figure}

With these results and the knowledge that our system runs smoothly, we will now present selected experiments that show something about the behaviour of our system.

\section{Standard parameters}
\label{standardParams}
For all following experiments, we varied one or two parameters.
Whenever nothing specified, the other ones were as follows:
We worked with 8 pairs of request-response clients, 16 one-way clients, 1 service with 10 consumers and 5 producers, 0.02 seconds of think time, 10 queues per type of client, a message size of 5 bytes, 8 threads per pool and 8 database connections per middleware. We ran for 400 seconds each test and threw away a conservative 240 seconds at the beginning and 5 seconds at the end as warmup and cooldown.

\section{Data tier connections and thread pool size}

The number of data tier connections and the number of threads in the thread pool on the middlewares are heavily correlated. This can be seen in the $2^{k}$ experiments shown in Figure \ref{fig:2kctt} and the corresponding Table \ref{tab:2kctt} which shows the throughput of the whole system. This makes sense, since every worker thread uses up to one connection from the connection pool at a time and the connections only get used by the worker threads. Thus in general, the minimum number of either of the two is the bottleneck. This however only holds up to a certain amount of threads since they all run on the same computer and at some point the overhead between thread swapping as well as the maximum amount of threads which can be run simultaneously will have a bigger impact on the maximum throughput of a single middleware.

\begin{table}
\centering
\subsubsection*{Sign Table (Outgoing Throughput on client)}
\begin{tabular}[c]{ccccl}
I & A & B & AB & y \\
\hline
1 & -1 & -1 & 1 & 295.0 \\
1 & 1 & -1 & -1 & 312.0 \\
1 & -1 & 1 & -1 & 295.0 \\
1 & 1 & 1 & 1 & 1080.0 \\
\hline
1980.0 & 801.0 & 766.0 & 768.0 & Total \\
495.0 & 200.0 & 191.0 & 192.0 & Total/4 \\
\end{tabular}
\subsubsection*{Allocation of Variance}
\begin{tabular}[p]{rrr}
Total Variation: & 454000.0 & 100\% \\
Thread pool size: & 160000.0 & 0.353\% \\
Data tier connection pool size: & 147000.0 & 0.323\% \\
Both: & 147000.0 & 0.324\% \\
\end{tabular}
\caption{$2^k$ factorial analysis,
A = Thread pool size, B = Data tier connection pool size}
\label{tab:2kctt}
\end{table}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/2k_connections_threads_throughput.png}
\caption{$2^k$ factorial experiment with varying numbers of threads in the threadpool as well as varying numbers of connections to the database for each middleware.}
\label{fig:2kctt}
\end{figure}

The fact that the decisive amount is the minimum of either is underlined by the single factor experiments we run for either of the two factors. 

In the single factor experiment for the number of connections to the database, we have a set amount of 8 threads in the thread pool. As shown in Figure \ref{fig:sct}, the throughput of the experiment did increase by a factor of 3.5 and the response time decreased by a factor of 5 as we increased the number of connections to the database from 1 to 8. For a higher number of connections to the database, the throughput and response time (shown in Figure \ref{fig:scr}) stagnated thus first the number of connections in the pool was the bottleneck while afterwards the number of threads became the bottleneck. This also gets supported by the average times in each part of the system as can be seen in \ref{fig:scb}. For the first experiment, the queue time is huge because of the conjunction at the connection pool which means that the shown database time is dramatically higher compared to the other experiments.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/single_connections_responseTime.png}
\caption{The average response times for requests measured on the clients while varying the number of connections to the database in the database connection pool. Constant amount of 8 threads are running in the thread pool.}
\label{fig:scr}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/single_connections_bars.png}
\caption{The average response times splitted into the different section of the lifetime of a request.}
\label{fig:scb}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/single_connections_throughput.png}
\caption{The average throughput for requests measured on the clients while varying the number of connections to the database in the database connection pool. Constant amount of 8 threads are running in the thread pool.}
\label{fig:sct}
\end{figure}

The exact same results can be attained with the opposite setup. In the experiments shown in Figures \ref{fig:str} and \ref{fig:stt} we have 8 database connections in the connection pool while we vary the number of threads in the thread pool. As can be seen, both the response time and the throughput gets drastically better as soon as the number of threads get increased up to 6 and then stagnate again.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/single_threads_responseTime.png}
\caption{The average response times for requests measured on the clients while varying the number of threads in the thread pool. Constant amount of 8 connections are in the connection pool.}
\label{fig:str}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/single_threads_throughput.png}
\caption{The average throughput for requests measured on the clients while varying the number of threads in the thread pool. Constant amount of 8 connections are in the connection pool.}
\label{fig:stt}
\end{figure}

In conclusion, the minimum amount of either number of threads in the thread pool and the size of the connection pool is the limiting amount up to around 8 each, afterwards the overhead of handling multiple threads and connections to the database got too big.

\section{Message length}

Another important factor is the message length. We ran single factor experiments with different message lengths as can be seen in Figure \ref{fig:smr} and the response time of the whole system increased as the message size got bigger. The results show a more or less linear behaviour. On the other hand, an increase of just a few bytes did not have a significant influence in the response time.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/single_messagelen_responseTime.png}
\caption{The average response times for requests depending on the message size.}
\label{fig:smr}
\end{figure}

The reason why the response time increases is the network traffic. This is shown in Figure \ref{fig:smb}. The time needed to handle a request between receiving a message on the middleware up to sending the response back to the client stays around the same while the time needed between the client and the middleware steadily increases.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/single_messagelen_bars.png}
\caption{The average response times for requests depending on the message size split up into the different sections in the lifetime of a request.}
\label{fig:smb}
\end{figure}

\section{Number of used queues in the system}

To see if the number of queues actually affects the throughput of the system, we ran several combinations with different types of clients. 

One simple setup was the number of queues in the system versus the number of one way clients whose behaviour is described in Section \ref{ClientTypes}. Figure  \ref{fig:2kqot} as well as the data in the corresponding Table \ref{tab:2kqot} shows, that the number of queues did not affect the actual throughput for all clients, no matter how many clients sent and read messages. 

\begin{table}
\centering
\subsubsection*{Sign Table (Outgoing Throughput on client)}
\begin{tabular}[c]{ccccl}
I & A & B & AB & y \\
\hline
1 & -1 & -1 & 1 & 240.0 \\
1 & 1 & -1 & -1 & 1080.0 \\
1 & -1 & 1 & -1 & 245.0 \\
1 & 1 & 1 & 1 & 1120.0 \\
\hline
2690.0 & 1720.0 & 41.4 & 33.2 & Total \\
673.0 & 430.0 & 10.3 & 8.31 & Total/4 \\
\end{tabular}
\subsubsection*{Allocation of Variance}
\begin{tabular}[p]{rrr}
Total Variation: & 741000.0 & 100\% \\
nOneWays: & 740000.0 & 0.999\% \\
nOneWaysQueues: & 428.0 & 0.000578\% \\
Both: & 276.0 & 0.000373\% \\
\end{tabular}
\caption{$2^k$ factorial analysis,
A = nOneWays, B = nOneWaysQueues}
\label{tab:2kqot}
\end{table}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/2k_oneways_onewaysqueues_throughput.png}
\caption{The average throughput for requests measured on the clients while varying the amount of queues in the database in relation to the number of oneway clients using them.}
\label{fig:2kqot}
\end{figure}

Another experiment series was the number of queues in relation to the number of request responds pairs. As can be seen in Figure \ref{fig:2kqpt} and in the corresponding Table \ref{tab:2kqpt} we have similar results as in the experiments above. As can be seen, the time in the data tier got reduced slightly which results in a slight change of total response time but again, it is outside of the statistical significance.

\begin{table}
\centering
\subsubsection*{Sign Table (Outgoing Throughput on client)}
\begin{tabular}[c]{ccccl}
I & A & B & AB & y \\
\hline
1 & -1 & -1 & 1 & 228.0 \\
1 & 1 & -1 & -1 & 1100.0 \\
1 & -1 & 1 & -1 & 230.0 \\
1 & 1 & 1 & 1 & 1120.0 \\
\hline
2680.0 & 1760.0 & 26.4 & 22.9 & Total \\
670.0 & 441.0 & 6.6 & 5.74 & Total/4 \\
\end{tabular}
\subsubsection*{Allocation of Variance}
\begin{tabular}[p]{rrr}
Total Variation: & 777000.0 & 100\% \\
Number of Request-Response Pairs: & 777000.0 & 1.0\% \\
Number of queues: & 174.0 & 0.000225\% \\
Both: & 132.0 & 0.000169\% \\
\end{tabular}
\caption{$2^k$ factorial analysis,
A = Number of Request-Response Pairs, B = Number of queues}
\label{tab:2kqpt}
\end{table}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/2k_oneways_onewaysqueues_throughput.png}
\caption{The average throughput for requests measured on the clients while varying amount of queues in the database in relation to the number of request response pairs using them.}
\label{fig:2kqpt}
\end{figure}



\section{Number of clients}

After having shown, that the number of clients is the main factor regarding the system load, the next goal was to find out how far our system can go until it gets saturated.

In Figure \ref{fig:spr} we can see that with low loads, the response time only slightly increases while the load increases significantly. For 128 clients though, the response times is more than double the response time for 48 clients. Therefore something must be clogging the system.
Looking at the average run times of the different tiers (see Figure \ref{fig:spb}), the database time stays the same on average which also corresponds to the actual data. This indicates, that the database itself is not saturated yet. Since there just 8 threads in the thread pool as well as 8 connections to the database open, they can only handle some amount of requests per time. The data suggests that it starts clogging around 48 clients. Another interesting fact is, that the time between the client and the middleware is increasing more or less linearly. This indicates that the IO thread of the middleware gets saturated itself because the point where the middleware is measuring its time is the point as soon as the IO Thread actually sees the incoming data and not the time where it arrives in the first place.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/single_pairs_responseTime.png}
\caption{The average response times for requests depending on number or request response pairs.}
\label{fig:spr}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/single_pairs_bars.png}
\caption{The average response times for requests depending on number or request response pairs split up into the different sections of the lifetime of a request.}
\label{fig:spb}
\end{figure}

Therefore one middleware can handle slightly less than 48 clients before it starts clogging, with our standard settings. Some queueing time can be tolerated because there is always the possibility that all the clients send messages at the same time thus the response time would spike because of the huge queue in the middleware. Another reason for queueing time spikes are the auto vacuums and checkpoints of the database which slow down or even stop transactions from running for short amount of times which obviously clogs the middleware during those period of times.

\section{Number of middlewares}

The next two experiments are central for measuring the scalability of a system. The speed up measures the change in response time based on constant load but varying resources in the system whereas the scale up measures the capability of coping with increasing load while the resources increase proportionally. For both test we use the same composition of clients as defined in Section \ref{standardParams}. There are 47 clients in total with a great mixture of read and write requests. This is around the amount of clients which start clogging a single middleware to ensure a maximum workload for each middleware and thus maximising the workload per middleware on the database as well as measurable differences in queue times.

\subsection{Speed up}

The speed up experiment is quite important since it shows how the response time scales with constant load while changing the available resources, in our case the number of middlewares. 

As can be seen in Figure \ref{fig:speedup}, our system gets faster, but definitely not linearly which a perfect system would. The reason for that is actually quite simple. As shown in Figure \ref{fig:speedupBars}, the time it takes to send a message over the network stays more or less the same in average. It just slightly goes back because the IO thread on the middleware will read the sockets more often due to less work and therefore the time will get measured earlier. The time in the data tier stays more or less constant as well. The small variations can be side effects of testing errors as well as the additional concurrent workload on the database which sometimes might happen. The real difference though is the queueing time which behaves more or less inverse proportional. Since there are more threats and connections in the middle layer, the messages can be processed more efficiently thus decreasing the queueing time on each middleware independently. 

In short, the main improvement on the response time is the decreasing queueing time and since this is only a small part of the whole response time, the response time does not scale inverse proportional to the number of resources and thus the speed up is not linear.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/speedup.png}
\caption{The speed up experiment for our system. The actual speed up values are not depicted in this graph but instead the response times which correspond to the speed up are shown as representatives.}
\label{fig:speedup}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/speedup_bars.png}
\caption{The speed up experiment for our system. Explaining the life cycle of an average request.}
\label{fig:speedupBars}
\end{figure}

\subsection{Scale up}

The scale up measures the throughput in relation to increasing load while increasing the available resource, in our case the middlewares, proportionally. Therefore it is really useful to show the scalability of the system as a whole.

Figure \ref{fig:scaleup} shows how the scale up for our system behaves. First it increases by a factor of around 1.7 while we doubled the load which means it is scaling not too bad but it is definitely not proportional. With 3 middle wares it is still increasing a bit but the system already starts thrashing and with 4 middlewares the throughput fell again because of the thrashing.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/scale_up.png}
\caption{The scale up experiment for our system.}
\label{fig:scaleup}
\end{figure}

By looking at Figure \ref{fig:scaleup_dt}, you can clearly see that the data tier gets slower and slower. Since the load put onto each middleware stays the same during all the experiments, this can only result from the database becoming slower and slower and therefore it being the bottleneck. Because of the slower response time of the data tier, the worker threads need more time to handle requests and therefore the queueing time increases. All of those factors vastly increase the response time and lower the throughput.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{imgs/scale_up_dt.png}
\caption{The scale up experiment for our system.}
\label{fig:scaleup_dt}
\end{figure} 

Conclusively, our system can take 2 middlewares with 47 clients each while still being able to handle the requests more or less efficiently. As soon as it gets increased even further though, it starts thrashing.

\section{Summary}

Our system runs stable over at least 2 hours and can handle several middlewares and clients at the same time without thrashing. If the middlewares are running with a full load, the database cannot handle more than 2 middlewares at a time but if the middlewares have smaller workloads it can handle several of them before the performance gain is getting small.

One of the main internal factors of the efficiency of our system are the number of connections in the database connection pool as well as the number of threads in the threadpool in each middleware.

Regarding the factors which are influenced by the environment, the number of queues used in the system as well as the composition of the clients did not have as huge as an impact onto the performance as expected. Even the message length did not change too much of the performance of the system itself, it just had a longer response time due to the additional network traffic.

There are still a few details which are not perfect yet, for example the auto vacuum and checkpoint of the database which clogs up the system regularly or the inefficiently running second database schema but all in all we are happy with our results.
